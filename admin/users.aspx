﻿<%@ Page Title="User Accounts &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="users.aspx.cs" Inherits="admin_users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
        .style5
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
<h2>User Accounts</h2>
			<p>This page allows you to view add user accounts.<asp:AccessDataSource 
                    ID="MembersData" runat="server" DataFile="~/App_Data/MathClub.mdb" 
                    SelectCommand="SELECT M.[Name], M.[Email] FROM [Members] M
                    WHERE (SELECT COUNT(*) FROM [Users] U WHERE U.[Username] = M.[Name]) = 0 ORDER BY M.[Name]">
                </asp:AccessDataSource>
            </p>
            <table class="style5">
                <tr>
                    <td valign="top">
                        <asp:CreateUserWizard 
                    ID="CreateUserWizard1" runat="server" LoginCreatedUser="false"
                    OnCreatedUser="CreateUserWizard1_CreatedUser" MembershipProvider="MembershipProvider" ActiveStepIndex="0"
                    ContinueDestinationPageUrl="~/admin/users.aspx" ContinueButtonText="New User" CompleteSuccessText="User created successfully."
                    InstructionText=" &nbsp;" >
                <WizardSteps>
                    <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server" Title="Create a New User" />
                    <asp:WizardStep ID="AddRolesStep" Runat="server" AllowReturn="False" Title="Assign User To Roles"
                        OnActivate="AssignUserToRoles_Activate" OnDeactivate="AssignUserToRoles_Deactivate">
                        <asp:CheckBoxList ID="AvailableRoles" runat="server" />
                        </asp:WizardStep>
                    <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server" OnActivate="AssignUserToRoles_Deactivate" />
                </WizardSteps>
                </asp:CreateUserWizard>
                    </td>
                    <td valign="top">
                        <h4>
                            Members without Accounts</h4>
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                            DataKeyNames="Name" DataSourceID="MembersData" ForeColor="#333333" 
                            GridLines="None">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <Columns>
                                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" 
                                    SortExpression="Name" />
                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                            </Columns>
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
</asp:Content>

