﻿<%@ Page Title="Update Classes &amp; Teachers &ndash; Newport Math Club" Language="C#"
    MasterPageFile="~/Site.master" %>

<script runat="server">

</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style5
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Update Classes &amp; Teachers</h2>
    <p>
        This page allows you to change the list of math classes and math teachers at NHS.</p>
    <asp:AccessDataSource ID="ClassesDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb"
        DeleteCommand="DELETE FROM [Classes] WHERE [Class Name] = ?" InsertCommand="INSERT INTO [Classes] ([Class Name]) VALUES (?)"
        SelectCommand="SELECT [Class Name] AS Class_Name FROM [Classes] ORDER BY [Class Name]">
        <DeleteParameters>
            <asp:Parameter Name="original_Class_Name" Type="String" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Class_Name" Type="String" />
        </InsertParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="TeachersDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb"
        DeleteCommand="DELETE FROM [Teachers] WHERE [Teacher Name] = ?" InsertCommand="INSERT INTO [Teachers] ([Teacher Name]) VALUES (?)"
        SelectCommand="SELECT [Teacher Name] AS Teacher_Name FROM [Teachers] ORDER BY [Teacher Name]">
        <DeleteParameters>
            <asp:Parameter Name="original_Teacher_Name" Type="String" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Teacher_Name" Type="String" />
        </InsertParameters>
    </asp:AccessDataSource>
    <table class="style5">
        <tr>
            <td valign="top">
                <asp:FormView ID="FormView1" runat="server" CellPadding="4" DataKeyNames="Class_Name"
                    DataSourceID="ClassesDataSource" DefaultMode="Insert" ForeColor="#333333">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <InsertItemTemplate>
                        Class Name:
                        <asp:TextBox ID="Class_NameTextBox" runat="server" Text='<%# Bind("Class_Name") %>' /><br />
                        <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                            Text="Add" />
                    </InsertItemTemplate>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                </asp:FormView>
                <br />
                <asp:GridView ID="ClassesGrid" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Class_Name" DataSourceID="ClassesDataSource"
                    ForeColor="#333333" GridLines="None">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" />
                        <asp:BoundField DataField="Class_Name" HeaderText="Class Name" ReadOnly="True" SortExpression="Class_Name" />
                    </Columns>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:FormView ID="FormView2" runat="server" CellPadding="4" DataKeyNames="Teacher_Name"
                    DataSourceID="TeachersDataSource" DefaultMode="Insert" ForeColor="#333333">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <InsertItemTemplate>
                        Teacher Name:
                        <asp:TextBox ID="Teacher_NameTextBox" runat="server" Text='<%# Bind("Teacher_Name") %>' /><br />
                        <asp:LinkButton ID="InsertButton0" runat="server" CausesValidation="True" CommandName="Insert"
                            Text="Add" />
                    </InsertItemTemplate>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                </asp:FormView>
                <br />
                <asp:GridView ID="TeachersGrid" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Teacher_Name" DataSourceID="TeachersDataSource"
                    ForeColor="#333333" GridLines="None">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" />
                        <asp:BoundField DataField="Teacher_Name" HeaderText="Teacher Name" ReadOnly="True"
                            SortExpression="Teacher_Name" />
                    </Columns>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
