﻿<%@ Page Title="Newport Math Club &ndash; Database Test" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="dbtest.aspx.cs" Inherits="admin_dbtest" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Database Test</h2>
    <p>
        This page tests the connection to the SQL Server 2008 instance used on this website.</p>
    <p>
        Server IP Address: 204.93.172.185</p>
    <asp:SqlDataSource ID="SqlDataSource" ConnectionString="<%$ ConnectionStrings:MathClubSqlServer %>"
    SelectCommand="SELECT * FROM kpmt_grades"
        runat="server"></asp:SqlDataSource>

        <h3>Test Data: Grades for KPMT</h3>
<asp:GridView ID="TestGridView" DataSourceID="SqlDataSource" AutoGenerateColumns="true" runat="server" />
</asp:Content>
