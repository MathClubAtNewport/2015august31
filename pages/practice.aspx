﻿<%@ Page Title="Newport Math Club &ndash; Practice" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style6
        {
            margin-top: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Practice</h2>
    <p>
        Practice is the best way to improve your performance at math contests and even in
        math class. Dedication and consistent practice are necessary for maximum growth.
        Incorporate math practice into your daily or weekly schedule and you&#39;ll be more
        prepared and confident at competitions.</p>
    <h3>
        Strategy for Practice</h3>
    <p>
        There are three areas that you should aim to improve when you practice competition
        mathematics. They are listed below along with some helpful pointers.</p>
    <ul>
        <li><strong>Knowledge</strong> - Read the entire problem. Think about possible approaches
            to solving it. Then try to solve the problem and check your answer. Do another similar
            problem and try to reinforce the concept. If you get stuck, use the internet, a
            teacher, or a friend to help you work through the problem. Memorize any necessary
            formulas or steps. Find a problem-solving method that makes sense to you.</li>
        <li class="style6"><strong>Accuracy</strong> - Once you understand the related math
            concept, find a series of problems that are similar to the one you learned (or make
            them up if possible). Then, do a short number of problems and check your answers.
            Go over those problems and see what you did wrong. Finally, finish the rest of the
            problems and focus on being as accurate as possible. Avoid rushing at this point.</li>
        <li class="style6"><strong>Speed</strong> - Now that you are confident that you can
            solve the problem, find or have a friend construct a practice test. Set a fast-paced,
            but reasonable time limit, create an answer grid, and go! On scratch paper, write
            as much as you need to as you work, but try not to write so much that it slows you
            down. Once the time is up, review each problem and how you solved it, and search
            for faster methods. Take the test again, and see if you can complete the same problems
            at a faster rate. Over time, you will start to recognize similar problems on related
            tests and you will begin to develop faster solution methods.</li>
    </ul>
    <h3>
        Past Competition Tests</h3>
    <p>
        Here is a list of practice materials for various tests, which includes but is not
        limited to past tests and sample tests from a variety of sources. It is recommended
        that members practice and familiarize themselves with test formats from competitions
        that they are attending prior to the competition date.</p>
    <p>
        Select a competition from the list below and click Go. A new window will take you
        to a page that lists past tests for the selected competition. JavaScript will need
        to be enabled on your browser in order to use this feature.</p>
    <fieldset name="group" style="height: 45px">
        <legend>Select a Competition: </legend>
        <table>
            <tr>
                <td>
                    <select name="input" id="input" style="width: 532px;">
                        <option selected="selected">(Select a competition...)</option>
                        <option value="http://wamath.net/hs/contests/sigma/samples.html">Skyview Math Contest
                            (SIGMA)</option>
                        <option value="http://www.wamath.net/contests/MathisCool/samples/high.html">Math Is
                            Cool (MIC)</option>
                        <option value="http://www.wamath.net/contests/MathisCool/samples/high.html">Math Is
                            Cool (MIC) Masters</option>
                        <option value="http://wamath.net/hs/contests/fallclassic/samples.html">Mu Alpha Theta
                            Fall Classic</option>
                        <option value="http://www.blaine.wednet.edu/Math%20Championship/highschool/hsresultstestsanswers.htm">
                            Northwest Math Championship (Blaine)</option>
                        <option value="http://www.artofproblemsolving.com/Forum/resources.php?c=182">American
                            Mathematics Competition (AMC)</option>
                        <option value="http://www.artofproblemsolving.com/Forum/resources.php?c=182">American
                            Invitational Mathematics Examination (AIME)</option>
                        <option value="http://wamath.net/hs/contests/wastate/state08/index.html">Mu Alpha Theta
                            State</option>
                        <option value="http://wsmc.net/contests/">Washington State Mathematics Council Mathematics
                            Contest (WSMC)</option>
                        <option value="http://www.artofproblemsolving.com/Forum/resources.php?c=182">United
                            States of America Mathematical Olympiad (USAMO)</option>
                    </select>
                </td>
                <td>
                    <input name="go" type="button" value="Go!" onclick="openLink()" style="width: 52px" />
                </td>
            </tr>
        </table>
    </fieldset>
    <script type="text/javascript">
        function openLink() {
            var value = theForm.input.options[theForm.input.selectedIndex].value;
            if (value != "") {
                window.open(value, 'newwin');
                return false;
            }
            else {
                if (value == "") value = "no selection";
                alert("Your choice, " + value + ", is not valid. \nIf this is a site error, please contact the webmaster. ");
            }
        }
    </script>
    <!--
    <p>
        <strong>A Newport Math Club Video: </strong>(not viewable in all browsers)</p>
    <p>
        <iframe id="I1" frameborder="0" marginheight="0" marginwidth="0" name="I1" scrolling="no"
            src="../video/Default.html" style="width: 320px; height: 240px">
            <p>
                Your browser does not support inline frames or is currently configured not to display
                inline frames.</p>
            <p>
                <a href="../video/Default.html">View the content of this inline frame</a> within
                your browser.</p>
        </iframe>
    </p>
    -->
</asp:Content>
