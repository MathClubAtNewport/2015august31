﻿<%@ Page Title="Newport Math Club &ndash; WSMA" Language="C#" MasterPageFile="~/Site.master" %>

<script runat="server">

</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <h1>
        <img src="../images/wsma_logo.png" /></h1>
    <p>
        <a href="www.wastudentmath.org">Washington Student Math Association</a>, or WSMA,
        is a student-run, nonprofit organization founded in 2009 whose <a href="http://www.wastudentmath.org/pages/about/mission.aspx">
            mission</a> is to promote math education throughout the state. WSMA aims to
        expand math-related programs across the state while creating a supportive community
        for teachers, coaches, parents and students and compiling resources for math clubs
        statewide.
    </p>
    <p>
        The Electronic Data Initiative, or EDI, is a project aimed at collecting practice
        tests, competition dates and other math club planning resources into a single, convenient
        location online. In addition to the EDI, WSMA puts on elementary school math seminars
        and a high school Knowledge Bowl tournament to encourage students' love of math
        at all ages.</p>
    <p>
        Newport Math Club is an active member of WSMA, with two members on the leadership
        board and many other students engaged in other crucial roles. Newport Math Club
        members helped found WSMA, and the club continues to contribute to the organization's
        resources and efforts.</p>
    <p>
        <a href="http://www.wastudentmath.org/pages/getinvolved/register.aspx">Join WSMA now!</a></p>
</asp:Content>
