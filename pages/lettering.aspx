﻿<%@ Page Title="Newport Math Club &ndash; Lettering" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="lettering.aspx.cs" Inherits="pages_lettering" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 684px;
            height: 310px;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <img alt="2009-10 Varsity letter awards" align="middle" class=".style1" src="\images/varsity-awards.png" />
    <h2>
        Lettering Criteria</h2>
    <h2>
        <asp:AccessDataSource ID="PointValuesData" runat="server" DataFile="~/App_Data/MathClub.mdb"
            SelectCommand="SELECT [Item], [Points] FROM [Point_Values] ORDER BY [Points] DESC">
        </asp:AccessDataSource>
        <asp:AccessDataSource ID="PointsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
            SelectCommand="SELECT P.[ID], P.[Date], V.[Item], V.[Points] FROM [Points] P, [Point_Values] V WHERE V.[Code]=P.[Item] AND P.[Name]=? ORDER BY P.[Date] DESC">
            <SelectParameters>
                <asp:ControlParameter ControlID="nameLabel" PropertyName="Text" Type="String" />
            </SelectParameters>
        </asp:AccessDataSource>
    </h2>
    <p>
        <asp:Label runat="server" ID="nameLabel" Visible="false" />
        Students may be eligible to receive a varsity letter in Math Club by demonstrating
        substantial achievement in the majority of the following areas, as determined by
        the club officers and advisor:</p>
    <ul>
        <li>Member attends and actively participates in club meetings </li>
        <li>Member attends and competes in math competitions </li>
        <li>Member represents Newport in a positive manner while attending contests </li>
        <li>Member shows consistent dedication to the club </li>
        <li>Member exhibits strong teamwork skills, demonstrating the ability to succeed both
            as an individual and as a member of his/her team </li>
        <li>Member spends time outside of club meetings practicing and reviewing what he/she
            has learned </li>
        <li>Member shows improvement throughout the year </li>
        <li>Member demonstrates an interest and enthusiasm for learning and/or teaching math
        </li>
        <li>Member is a senior who has actively participated in Math Club for all four years
            of high school </li>
        <li>Member earns a high number of “pi points,” as determined by the club officers near
            the end of the year </li>
    </ul>
    <p>
        Club members will not be able to earn a letter without hard work; only the most
        devoted members will be eligible to letter.
    </p>
    <p>
        Pi Points serve as a guide for lettering, though they are not the exclusive way
        to earn a letter. The point system is structured to include participation, dedication,
        and learning, not simply winning.
    </p>
    <p>
        <asp:LoginView ID="LoginViewPoints" runat="server">
            <LoggedInTemplate>
                You currently have&nbsp;<asp:Label ID="TotalPointsLabel" runat="server"></asp:Label>
                Pi Points. View them <a href="../members/mypoints.aspx">here</a>.
            </LoggedInTemplate>
        </asp:LoginView>
        &nbsp;The points can be earned using the following point values:
    </p>
    <p>
        <asp:GridView ID="PointsGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            CellPadding="4" DataSourceID="PointValuesData" ForeColor="#333333" GridLines="None">
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:BoundField DataField="Item" HeaderText="Item" SortExpression="Item" />
                <asp:BoundField DataField="Points" HeaderText="Points" SortExpression="Points" />
            </Columns>
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </p>
    <p>
        Note: Newport Math Club officers reserve the right to change these point values
        at any time.</p>
    <asp:GridView ID="MyPointsGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
        CellPadding="4" DataSourceID="PointsData" ForeColor="#333333" GridLines="None"
        Visible="False">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" SortExpression="ID"
                Visible="False" />
            <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" DataFormatString="{0:ddd. M/d/yy}" />
            <asp:BoundField DataField="Item" HeaderText="Item" SortExpression="Item" />
            <asp:BoundField DataField="Points" HeaderText="Points" SortExpression="Points" />
        </Columns>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>
