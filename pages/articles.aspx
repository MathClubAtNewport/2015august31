<%@ Page Title="Newport Math Club &ndash; Articles" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .pages_articles_th
        {
            font-weight: bold;
            background-color: #FFFFCC;
        }
        .style1
        {
            font-weight: bold;
            background-color: #FFFFCC;
            width: 356px;
        }
        .style2
        {
            width: 356px;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Articles</h2>
    <p>
        This page will contain periodically updated articles and lessons on various math
        topics, spanning a wide range of difficulty levels. Content will be added as we
        cover more material in our club meetings.</p>
    <br>
  
    <h4>
        Presentations</h4>
    <p>
        If you missed seeing any of these presentations, or are otherwise interested in
        seeing them, please feel free to have a look. Sample problems are often included
        in the presentations. Happy learning!</p>
    <table style="width: 100%;">
        <tr>
            <td class="style1">
                <p>
                    Topic</p>
            </td>
            <td class="pages_articles_th">
                <p>
                    Date</p>
            </td>
            <td class="pages_articles_th">
                <p>
                    Download</p>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Number Theory (part 1)
            </td>
            <td>
                11/21/2008
            </td>
            <td>
                <a href="../articles/numbertheory1.ppt">.ppt</a> | <a href="../articles/numbertheory1.pptx">
                    .pptx</a> | <a href="../articles/numbertheory1.pdf">.pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Circles
            </td>
            <td>
                12/5/2008
            </td>
            <td>
                <a href="../articles/circles.ppt">.ppt</a> | <a href="../articles/circles.pptx">.pptx</a>
                | <a href="../articles/circles.pdf">.pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Number Theory (part 2)
            </td>
            <td>
                12/12/2008
            </td>
            <td>
                <a href="../articles/numbertheory2.ppt">.ppt</a> | <a href="../articles/numbertheory2.pptx">
                    .pptx</a> | <a href="../articles/numbertheory2.pdf">.pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Bases
            </td>
            <td>
                4/24/2009
            </td>
            <td>
                <a href="../articles/bases.ppt">.ppt</a> | <a href="../articles/bases.pptx">.pptx</a>
                | <a href="../articles/bases.pdf">.pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Polynomials (by Brian Liou)
            </td>
            <td>
                3/25/2011
            </td>
            <td>
                <a href="../articles/Polynomials.ppt">.ppt</a> | <a href="../articles/Polynomials.pptx">
                    .pptx</a> | <a href="../articles/Polynomials.pdf">.pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Complex Numbers (by Brian Liou)
            </td>
            <td>
                4/1/2011
            </td>
            <td>
                <a href="../articles/Polynomials Review Complex Numbers.ppt">.ppt</a> | <a href="../articles/Polynomials Review Complex Numbers.pptx">
                    .pptx</a> | <a href="../articles/Polynomials Review Complex Numbers.pdf">.pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Inequalities (by Brian Liou)
            </td>
            <td>
                4/15/2011
            </td>
            <td>
                <a href="../articles/Inequalities.ppt">.ppt</a> | <a href="../articles/Inequalities.pptx">
                    .pptx</a> | <a href="../articles/Inequalities.pdf">.pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Ratios (by Emily Zhang)</td>
            <td>
                9/23/2011</td>
            <td>
                <a href="../articles/Ratios.pptx">.pptx</a> | <a href="../articles/Ratios.pdf">.pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Sequences and Series (by Emily Zhang)</td>
            <td>
                10/7/2011</td>
            <td>
               <a href="../articles/Sequences%20and%20Series.pptx">.pptx</a> | <a href="../articles/Sequences%20and%20Series.pdf">.pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Counting and Probability (by Emily Zhang)</td>
            <td>
                10/14/2011</td>
            <td>
                <a href="../articles/Counting%20and%20Probability.pptx">.pptx</a> | <a href="../articles/Counting%20and%20Probability.pdf">.pdf</a>
            </td>
        </tr>
    </table>
    <h4>
        Practice Worksheets</h4>
    <p>
        Feel free to use these worksheets to practice what you learned in some of the above
        presentations!</p>
    <table style="width: 100%;">
        <tr>
            <td class="pages_articles_th">
                <p>
                    Topic</p>
            </td>
            <td class="pages_articles_th">
                <p>
                    Date</p>
            </td>
            <td class="pages_articles_th">
                <p>
                    Download</p>
            </td>
        </tr>
        <tr>
            <td>
                Polynomials & Complex Numbers (by Brian Liou)
            </td>
            <td>
                3/25/2011
            </td>
            <td>
                <a href="../articles/Polynomials Complex Numbers Worksheet.doc">.doc</a> | <a href="../articles/Polynomials Complex Numbers Worksheet.docx">
                    .docx</a> | <a href="../articles/Polynomials Complex Numbers Worksheet.pdf">.pdf</a>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;Select Answers
            </td>
            <td>
                3/25/2011
            </td>
            <td>
                <a href="../articles/Polynomials Complex Numbers Worksheet Select Answers.doc">.doc</a>
                | <a href="../articles/Polynomials Complex Numbers Worksheet Select Answers.docx">.docx</a>
                | <a href="../articles/Polynomials Complex Numbers Worksheet Select Answers.pdf">.pdf</a>
            </td>
        </tr>
        <tr>
            <td>
                Inequalities (by Brian Liou)
            </td>
            <td>
                4/15/2011
            </td>
            <td>
                <a href="../articles/Inequalities Worksheet.doc">.doc</a> | <a href="../articles/Inequalities Worksheet.docx">
                    .docx</a> | <a href="../articles/Inequalities Worksheet.pdf">.pdf</a>
            </td>
        </tr>
    </table>
</asp:Content>
