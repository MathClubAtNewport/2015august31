﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_achievements : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!User.Identity.IsAuthenticated)
        {
            TeamGrid.Columns[0].Visible = false; // Hide viewing team members
            TeamGrid.Columns[4].Visible = false; // Hide viewing team name
            // 0 ID, 1 Competition, 2 Name
            IndividualGrid.Columns[2].Visible = false; // Hide name from users not logged in
        }
    }
}