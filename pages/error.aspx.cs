﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_error : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string error = (string)Application["LastError"];
        if (String.IsNullOrEmpty(error))
            lblErrorDetails.Text = "No error occured or no information about the error is available.";
        else
            lblErrorDetails.Text = error;
    }
}