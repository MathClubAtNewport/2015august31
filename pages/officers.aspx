<%@ Page Title="Newport Math Club &ndash; Officers" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 145px;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Officers</h2>
    <p>
        Our officers for the current school year were elected at the end of the previous
        school year and inducted at the end-of-the-year banquet last May. The officers
        work hard to prepare for team meetings, plan and register for competitions, and
        keep track of team finances and records. Since they were elected to serve the club
        and to maintain a positive learning environment, they would appreciate your suggestions
        and feedback in the interest of improving the club.</p>
    <h3>
        Mailing Address</h3>
    <p>
        Newport Math Club<br />
        Newport High School<br />
        4333 Factoria Blvd. SE
        <br />
        Bellevue, WA 98006</p>
    <h3>
        Officer Contact Information</h3>
    <p>
        Use this address to send an e-mail to all five student officers: <a href="mailto:officers@newportmathclub.org">
            officers@newportmathclub.org</a><br />
        Use this address to send an e-mail to all five student officers and all faculty
        advisors: <a href="mailto:leadership@newportmathclub.org">leadership@newportmathclub.org</a>
    </p>
    <p>
        &nbsp;Contact information for the officers and advisors is given below:</p>
    <table class="officersTable" border="1" rules="all" frame="box">
        <tr>
            <td class="style1" valign="middle">
                <strong>Name</strong>
            </td>
            <td style="width: 142px;" class="style14" valign="middle">
                <strong>Position</strong>
            </td>
            <td style="width: 406px;" class="style14" valign="middle">
                <strong>Details</strong>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <strong>Phillip Wang</strong>
            </td>
            <td style="width: 142px" class="style19">
                <strong>President</strong>
            </td>
            <td style="width: 406px" class="style19">
                <a href="mailto:president@newportmathclub.org">Send E-mail</a>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <strong>Sean Yu</strong></td>
            <td style="width: 142px" class="style19">
                <strong>Vice President</strong>
            </td>
            <td style="width: 406px" class="style19">
                <a href="mailto:vicepresident@newportmathclub.org">Send E-mail</a>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <strong>Andy Kim</strong></td>
            <td style="width: 142px" class="style19">
                <strong>Treasurer</strong>
            </td>
            <td style="width: 406px" class="style19">
                <a href="mailto:treasurer@newportmathclub.org">Send E-mail</a>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <strong>Laura Hu</strong></td>
            <td style="width: 142px" class="style19">
                <strong>Secretary</strong>
            </td>
            <td style="width: 406px" class="style19">
                <a href="mailto:secretary@newportmathclub.org">Send E-mail</a>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <strong>Michael Huang</strong></td>
            <td style="width: 142px" class="style19">
                <strong>Webmaster</strong>
            </td>
            <td style="width: 406px" class="style19">
                <a href="mailto:webmaster@newportmathclub.org">Send E-mail</a>
            </td>
        </tr>
        

<!--	I don't believe we need Mrs. Jones' stuff anymore 
	<tr>
           <td class="style1">
               <strong>Mrs. Jones</strong>
           </td>
           <td style="width: 142px" class="style19">
            <strong>Club Advisor</strong>
            </td>
           <td style="width: 406px" class="style19">
             <a href="mailto:jonese@newportmathclub.org">Send E-mail</a> � <a href="http://curriculum.bsd405.org/MySite/Public.aspx?accountname=BSD%5cJonese"
              onclick="window.open(this.href,'newwin'); return false;">View Website</a>
            </td>
        </tr>
-->
        <tr>        
	<td class="style1">
		<b>Ms. Northey</b>
	</td>
	<td style="width: 142px" class="style19">
		<b>Incoming Club Advisor</b>
	</td>
	<td style="width:406px" class="style19">
		<a href="mailto:northeyt@newportmathclub.org">Send E-mail</a> ~ <b> Site Currently Unavailable </b>
	</td>
	</tr>
    </table>
    <h3>
        Website Feedback</h3>
    <p>
        We appreciate feedback about our website. If you notice any errors or have any suggestions,
        please send a message to the <a href="mailto:webmaster@newportmathclub.org">webmaster</a>.</p>
    <br />
</asp:Content>
