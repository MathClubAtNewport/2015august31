﻿<%@ Page Title="Newport Math Club &ndash; Page Not Found" Language="C#" MasterPageFile="~/Site.master" %>

<script runat="server">

</script>
<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
    <%
        Response.Status = "404 Not Found";
    %>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Page Not Found</h2>
    <p>
        You tried to access a page that did not exist or was unavailable. If you believe
        this is an error, please <a href="mailto:webmaster@newportmathclub.org">contact the
            webmaster</a>.
    </p>
    <p>
        <a href="<%= ResolveUrl("~/default.aspx") %>">Return to the home page &raquo;</a></p>
</asp:Content>
