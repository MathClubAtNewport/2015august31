﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_meetings : System.Web.UI.Page
{
    public class GridViewBoundFieldHelper
    {
        public static int GetIndex(GridView grd, string fieldName)
        {
            for (int i = 0; i < grd.Columns.Count; i++)
            {
                DataControlField field = grd.Columns[i];

                HyperLinkField bfield = field as HyperLinkField;

                //Assuming accessing happens at data level, e.g with data field's name
                if (bfield != null && bfield.HeaderText == fieldName)
                    return i;
            }
            return -1;
        }

        public static HyperLinkField GetField(GridView grd, string fieldName)
        {
            int index = GetIndex(grd, fieldName);
            return (index == -1) ? null : grd.Columns[index] as HyperLinkField;
        }

        public static string GetText(GridViewRow row, string fieldName)
        {
            GridView grd = row.NamingContainer as GridView;
            if (grd != null)
            {
                int index = GetIndex(grd, fieldName);
                if (index != -1)
                    return row.Cells[index].Text;
            }
            return "";
        }
    }

    void MeetingsGrid_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < MeetingsGrid.Rows.Count; i++)
        {
            HyperLinkField link = GridViewBoundFieldHelper.GetField(MeetingsGrid, "Minutes");
            if (link.Target == "")
                link.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        MeetingsGrid.Columns[5].Visible = false; // Hide Minutes
        if (!User.Identity.IsAuthenticated)
        {
            MeetingsGrid.Columns[3].Visible = false; // Hide Present
            //MeetingsGrid.Columns[0].Visible = false; // Hide ID
            PresentExplainPanel.Visible = false;
            return;
        }
        else
        {
            if (User.IsInRole("Officers"))
                MeetingsGrid.DataSourceID = MeetingsDataSource_Officers.ID;
        }

        ShowCheckBoxes();

        /*AttendanceDataSource.DataSourceMode = SqlDataSourceMode.DataSet;
        System.Data.DataView view = AttendanceDataSource.Select(DataSourceSelectArguments.Empty) as System.Data.DataView;

        ArrayList meetings = new ArrayList();
        foreach (System.Data.DataRow row in view.Table.Rows) // 0 ID, 1 Name, 2 Meeting
        {
            if ((string)(row.ItemArray.GetValue(1)) == User.Identity.Name)
                meetings.Add((int)(row.ItemArray.GetValue(2)));
        }*/

        /*foreach (GridViewRow row in MeetingsGrid.Rows)
        {
            string text = row.Cells[0].Text;
            int meetingNumber = Int32.Parse(text);
            if (meetings.Contains(meetingNumber))
                (row.Cells[3].Controls[1] as CheckBox).Checked = true;
            else
                (row.Cells[3].Controls[1] as CheckBox).Checked = false;
        }*/
    }

    protected void MeetingsDataSource_Members_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters[0].Value = User.Identity.Name;
    }

    protected void MeetingsDataSource_Officers_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters[0].Value = User.Identity.Name;
    }

    protected void MeetingsGrid_PageIndexChanging(object sender, EventArgs e)
    {
        ShowCheckBoxes();
    }

    protected void ShowCheckBoxes()
    {
        foreach (GridViewRow row in MeetingsGrid.Rows)
        {
            string text = row.Cells[3].Text; // present: -1, not present: 0
            if (text.Equals("-1"))
                row.Cells[3].Text = "Yes";
            else if (text.Equals("0"))
                row.Cells[3].Text = "No";
            /*CheckBox c = new CheckBox();
            c.Checked = (present < 0);
            c.Enabled = false;
            row.Cells.Add(new TableCell());
            row.Cells[row.Cells.Count - 1].Controls.Add(c);
            //row.Cells[3].Controls.Add(c);
            //if (present < 0)
                //row.Cells[3].Text = "Yes";*/
        }
    }
}
