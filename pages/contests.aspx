﻿<%@ Page Title="Newport Math Club &ndash; Contests" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Contests</h2>
    <p>
        Newport Math Club hosts a variety of contests to reach out into the community and
        provide more opportunities for students of all grade levels to succeed in mathematics.</p>
    <h4>
        <a href="../kpmt/default.aspx">Knights of Pi Math Tournament</a></h4>
    <p>
        The <a href="../kpmt/default.aspx">Knights of Pi Math Tournament</a> is a math contest
        hosted at Newport High School where we invite math teams in grades 5 through 8 to
        come and compete. We take pride in being able to host a competition and share our
        love of math with younger students in our community. The tournament consists of
        many rounds that reinforce and challenge students&#39; math abilities on a variety
        of topics in a team of four as well as in an individual setting. In 2009, 164 students
        from 14 schools competed in the tournament on December 12, 2009. The 2010 Tournament
        was held on <b>Saturday, December 4, 2010,</b> hosting 222 students from 18 schools
        for a fun day of math. More information about the Knights of Pi Tournament is available on
        the tournament&#39;s <a href="../kpmt/default.aspx">contest page</a>.</p>
    <h4>
        American Mathematics Contest (AMC) 10/12</h4>
    <p>
        The American Mathematics Competition, hosted by The Mathematical Association of
        America (MAA), is a 25-question multiple-choice test available to all Newport High
        School students. Sign-ups were held in January in math classrooms, and the contest
        was held on February 8 and 23, 2010. We are pleased to report that <b>over 150 students
            participated in the AMC this year! </b>A big thanks to all our proctors and
        all the students who participated.</p>
    <p>
        There are two AMC test levels: 10th grade and 12th grade. The AMC 10 is open to
        grades 9-10, while the AMC 12 is open to all grade levels. 9th and 10th grade students
        may choose which exam they wish to take. By performing well on the exams, students
        can win awards and qualify for further invitational contests, such as the AIME.
        In fact, several Newport students already qualified for the AIME based on their
        AMC 10/12 A performace. The AMC is administered in the NHS math wing after school.
        Each year, Newport participates on both test dates (A and B). For more information
        about the AMC exams or the MAA, please visit <a href="http://www.unl.edu/amc/">http://www.unl.edu/amc/</a>.</p>
    <h4>
        American Invitational Mathematics Examination (AIME)</h4>
    <p>
        The AIME is a 15-question short-answer test offered to all students who qualify
        based on their AMC 10/12 scores. The test lasts 3 hours, and the questions are very
        challenging. The 2010 AIME was administered at Newport High School to 9 students
        in grades 8 through 12 on Tuesday, March 31, 2010. The 2011 AIME will be administered at Newport on Tuesday, March 30, 2011.</p>
    <h4>
        National Assessment & Testing Contests</h4>
    <p>
        Newport Math Club hosts NA&T contests at our school each year. The contests
        range from individual to team work and are open to all Newport students. See more
        information about the contests <a href="~/contests/nat.aspx" runat="server">here</a>.</p>
    <h4>
        <a href="../contests/challenge/challenge.aspx" target="_contest">Newport Math Challenge</a></h4>
    <p>
        <a href="../contests/challenge/challenge.aspx" target="_contest">
            <img id="ChallengeImage" src="../contests/challenge/Challenge.png" alt="Newport Math Challenge"
                width="206px" style="float: right" />
        </a>This 25-question contest was held over a five-week period in January-February
        2009 and is open to <b><i>all</i></b> Newport High School students. It was designed
        to help sharpen students&#39; math skills, prepare them for the AMC, and get the
        entire student body excited about math! Questions were designed to be challenging
        and stimulating, but students who attempted the challenge found themselves better
        problem solvers at the end regardless of their scores. Students had one week to
        solve each of five sets of five problems. The most rewarding part of the challenge
        was that at the end of each week, solutions were posted so that students could have
        an opportunity to learn about math concepts that they did not know.</p>
    <p>
        <b>The Newport Math Challenge is not currently open.</b> Click <a href="../contests/challenge/challenge.aspx"
            target="_contest">here</a> to go to the contest page.</p>
</asp:Content>
