﻿<%@ Page Title="Newport Math Club &ndash; Error" Language="C#"
    MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="error.aspx.cs"
    Inherits="pages_error" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Error</h2>
    <p>
        Oops! An error occured on the server. Please try the operation again, or contact
        the <a href="mailto:webmaster@newportmathclub.org">webmaster</a> if the problem persists.</p>
    <h4>
        Technical Details</h4>
    <asp:Label ID="lblErrorDetails" runat="server" Style="color: Red" 
        Text="Error Details" />
</asp:Content>
