﻿<%@ Page Title="Newport Math Club &ndash; Links" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Links</h2>
    <ul>
        <li>
            <p>
                <a href="http://www.wastudentmath.org/" target="_blank">
                    <img alt="WSMA" height="60" src="../images/wsma_logo.png" style="float: right" width="240" /></a>
                <a href="http://www.wastudentmath.org" onclick="window.open(this.href,'newwin'); return false;">
                    <strong>www.wastudentmath.org</strong></a> - Washington Student Math Association
                (WSMA), a student-led non-profit organization designed to expand and support math
                club programs in Washington. Their website has lots of great resources, including
                information about competitions, past tests, and summer programs.
            </p>
        </li>
        <li>
            <p>
                <a href="http://www.mathace.net" onclick="window.open(this.href,'newwin'); return false;">
                    www.mathace.net</a> - General math articles written by Newport Math Club members</p>
        </li>
        <li>
            <p>
                <a href="http://www.bsd405.org" onclick="window.open(this.href,'newwin'); return false;">
                    www.bsd405.org</a> - Bellevue School District website</p>
        </li>
        <li>
            <p>
                <a href="http://schools.bsd405.org/nhs/homepage/" onclick="window.open(this.href,'newwin'); return false;">
                    schools.bsd405.org/nhs/homepage</a> - Newport High School website</p>
        </li>
        <li>
            <p>
                <a href="http://www.artofproblemsolving.com" onclick="window.open(this.href,'newwin'); return false;">
                    www.artofproblemsolving.com</a> - Problem-solving mathematics for strong math
                students, with forums and lots of excellent resources.</p>
        </li>
        <li>
            <p>
                <a href="http://www.wamath.net/hs/contests/index.html" onclick="window.open(this.href,'newwin'); return false;">
                    www.wamath.net/hs/contests/index.html</a> - List of Washington math competitions
                with some past tests and results (not always current).</p>
        </li>
    </ul>
</asp:Content>
