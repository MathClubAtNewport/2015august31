﻿<%@ Page Title="Newport Math Club  &ndash; Search" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Search Results</h2>
    <div id="cse-search-results">
    </div>
    <script type="text/javascript">
        var googleSearchIframeName = "cse-search-results";
        var googleSearchFormName = "cse-search-box";
        var googleSearchFrameWidth = 795;
        var googleSearchDomain = "www.google.com";
        var googleSearchPath = "/cse";
    </script>
    <script type="text/javascript" src="http://www.google.com/afsonline/show_afs_search.js"></script>
</asp:Content>
