﻿<%@ Page Title="Newport Math Club &ndash; Achievements" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="achievements.aspx.cs" Inherits="pages_achievements" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <asp:AccessDataSource ID="TeamData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT A.[ID], C.[Name], T.[ID] as TeamID, T.[Team Description], A.[Place], A.[Division], A.[Award Description] as description
                FROM [Team_Awards] A, [Teams] T, [Competitions] C
                WHERE T.[ID]=A.[Team] AND C.[ID]=T.[Competition]
                ORDER BY C.[Date] DESC"></asp:AccessDataSource>
    <asp:AccessDataSource ID="TeamMembersData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT P.[Member] FROM [Competitors] P, [Team_Members] M WHERE M.[Team]=? AND P.[ID]=M.[Competitor] ORDER BY P.[Member]">
        <SelectParameters>
            <asp:ControlParameter ControlID="TeamGrid" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="IndividualData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT A.[ID], C.[Name], P.[Member], A.[Place], A.[Division], A.[Award Description] as description
                FROM [Individual_Awards] A, [Competitors] P, [Competitions] C
                WHERE P.[ID]=A.[Competitor] AND C.[ID]=P.[Competition]
                ORDER BY C.[Date] DESC"></asp:AccessDataSource>
    <h2>
        Club Achievements</h2>
    <hr />
    <h4>
        Team Awards</h4>
    <br />
    <table style="float: none;" class="style1">
        <tr>
            <td align="center">
                <img src="../images/awards/MIC_08_1st.jpg" alt="Math is Cool Championships" height="200px" />
            </td>
            <td align="center">
                <img src="../images/awards/Fall_Classic_08_5th.jpg" alt="Fall Classic" height="200px" />
            </td>
            <td align="center">
                <img src="../images/awards/MIC_Masters_09_3rd.jpg" alt="Math is Cool Masters" width="200px" />
            </td>
        </tr>
        <tr>
            <td align="center">
                Math is Cool Championships 2008<br />
                1st Place 9/10 Team 1B
            </td>
            <td align="center">
                Fall Classic 2008<br />
                5th Place 9/10 Team
            </td>
            <td align="center">
                Math is Cool Masters 2009<br />
                3rd Place 9/10 Team
            </td>
        </tr>
    </table>
    <br />
    <asp:GridView ID="TeamGrid" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataSourceID="TeamData" ForeColor="#333333"
        GridLines="None" DataKeyNames="TeamID">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:CommandField ShowSelectButton="True" SelectText="View Team Members" />
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" SortExpression="ID"
                Visible="False" />
            <asp:BoundField DataField="Name" HeaderText="Competition" SortExpression="Name" />
            <asp:BoundField DataField="TeamID" Visible="false" />
            <asp:BoundField DataField="Team Description" HeaderText="Team" SortExpression="Team Description" />
            <asp:BoundField DataField="Place" HeaderText="Place" SortExpression="Place" />
            <asp:BoundField DataField="Division" HeaderText="Division" SortExpression="Division" />
            <asp:BoundField DataField="description" HeaderText="Award Description" SortExpression="description" />
        </Columns>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <br />
    <asp:GridView ID="TeamMembersGrid" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataSourceID="TeamMembersData" ForeColor="#333333"
        GridLines="None">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="Member" HeaderText="Team Members" SortExpression="Member" />
        </Columns>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <hr />
    <h4>
        Individual Awards</h4>
    <br />
    <asp:GridView ID="IndividualGrid" runat="server" CellPadding="4" DataSourceID="IndividualData"
        ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" AllowPaging="True"
        AllowSorting="True" PageSize="20">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" Visible="false"
                SortExpression="ID" />
            <asp:BoundField DataField="Name" HeaderText="Competition" SortExpression="Name" />
            <asp:BoundField DataField="Member" HeaderText="Name" SortExpression="Member" />
            <asp:BoundField DataField="Place" HeaderText="Place" SortExpression="Place" />
            <asp:BoundField DataField="Division" HeaderText="Division" SortExpression="Division" />
            <asp:BoundField DataField="description" HeaderText="Award Description" SortExpression="description" />
        </Columns>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>
