﻿<%@ Page Title="Newport Math Club &ndash; Upcoming Competitions" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="competitions.aspx.cs" Inherits="pages_competitions" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Upcoming Competitions</h2>
    <p>
        This page lists all known upcoming math competitions that the Newport Math Club
        may participate in, whether as an entire team or as a select group of individuals.</p>
    <p>
        To view more information about a competition, click the &quot;View Details&quot;
        link next to the corresponding competition listing.
        <asp:LoginView ID="LoginView1" runat="server">
            <AnonymousTemplate>
                <asp:LoginStatus runat="server" ID="LoginStatus"></asp:LoginStatus>
                &nbsp;to view all of the detailed information.</AnonymousTemplate>
        </asp:LoginView>
    </p>
    <asp:Label ID="now" Visible="false" runat="server" />
    <asp:AccessDataSource ID="CompetitionsDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
        SelectCommand="SELECT * FROM [Competitions] WHERE NOT [Date] < ? ORDER BY [Date]">
        <SelectParameters>
            <asp:ControlParameter ControlID="now" PropertyName="Text" Type="DateTime" />
        </SelectParameters>
        <DeleteParameters>
            <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Name="ID" />
        </DeleteParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="CompetitionDetailsDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
        SelectCommand="SELECT * FROM [Competitions] WHERE ([ID] = ?) ORDER BY [Date]">
        <SelectParameters>
            <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:GridView ID="CompetitionsGrid" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="CompetitionsDataSource"
        ForeColor="#333333" GridLines="None" PageSize="8">
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:CommandField ShowSelectButton="True" SelectText="View Details"></asp:CommandField>
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                SortExpression="ID" Visible="False"></asp:BoundField>
            <asp:BoundField DataField="Date" DataFormatString="{0:ddd. MMMM d, yyyy}" HeaderText="Date"
                SortExpression="Date"></asp:BoundField>
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
            <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location">
            </asp:BoundField>
            <asp:BoundField DataField="MeetTime" HeaderText="Meet Time" SortExpression="MeetTime"
                Visible="False"></asp:BoundField>
            <asp:BoundField DataField="StartTime" DataFormatString="{0:h:mm tt}" HeaderText="Start Time"
                SortExpression="StartTime"></asp:BoundField>
            <asp:BoundField DataField="EndTime" HeaderText="End Time" SortExpression="EndTime"
                Visible="False"></asp:BoundField>
            <asp:BoundField DataField="Fees" HeaderText="Fees" SortExpression="Fees" Visible="False">
            </asp:BoundField>
            <asp:BoundField DataField="Pay" HeaderText="Pay" SortExpression="Pay" Visible="False">
            </asp:BoundField>
            <asp:BoundField DataField="Meals" HeaderText="Meals" SortExpression="Meals" Visible="False">
            </asp:BoundField>
            <asp:BoundField DataField="Transportation" HeaderText="Transportation" SortExpression="Transportation"
                Visible="False"></asp:BoundField>
            <asp:CheckBoxField DataField="Registered" HeaderText="Registered" SortExpression="Registered"
                Visible="False"></asp:CheckBoxField>
            <asp:CheckBoxField DataField="Paid" HeaderText="Paid" SortExpression="Paid" Visible="False">
            </asp:CheckBoxField>
            <asp:BoundField DataField="RegistrationInfo" HeaderText="Registration Info" SortExpression="RegistrationInfo"
                Visible="False"></asp:BoundField>
            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description"
                Visible="False"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <br />
    <asp:LoginView runat="server" ID="LoginView5">
        <AnonymousTemplate>
            <asp:FormView ID="CompetitionDetailsForm" runat="server" CellPadding="4" DataKeyNames="ID"
                DataSourceID="CompetitionDetailsDataSource" ForeColor="#333333" HeaderText="Competition Details">
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <ItemTemplate>
                    Name:
                    <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>' />
                    <br />
                    Location:
                    <asp:Label ID="LocationLabel" runat="server" Text='<%# Bind("Location") %>' />
                    <br />
                    Date:
                    <asp:Label ID="DateLabel" runat="server" Text='<%# Bind("Date", "{0:ddd. MMMM d, yyyy}") %>' />
                    <br />
                    Start Time:
                    <asp:Label ID="Start_TimeLabel" runat="server" Text='<%# Bind("[StartTime]", "{0:h:mm tt}") %>' />
                    <br />
                    End Time:
                    <asp:Label ID="End_TimeLabel" runat="server" Text='<%# Bind("[EndTime]", "{0:h:mm tt}") %>' />
                    <br />
                </ItemTemplate>
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            </asp:FormView>
            <p>
                <asp:LoginStatus runat="server" ID="LoginStatus3"></asp:LoginStatus>
                &nbsp;to view full competition details.</p>
        </AnonymousTemplate>
        <LoggedInTemplate>
            <asp:FormView ID="CompetitionDetailsForm0" runat="server" CellPadding="4" DataKeyNames="ID"
                DataSourceID="CompetitionDetailsDataSource" ForeColor="#333333" HeaderText="Competition Details">
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <ItemTemplate>
                    Name:
                    <asp:Label ID="NameLabel0" runat="server" Text='<%# Bind("Name") %>' />
                    <br />
                    Location:
                    <asp:Label ID="LocationLabel0" runat="server" Text='<%# Bind("Location") %>' />
                    <br />
                    Date:
                    <asp:Label ID="DateLabel0" runat="server" Text='<%# Bind("Date", "{0:ddd. MMMM d, yyyy}") %>' />
                    <br />
                    Meet Time:
                    <asp:Label ID="Meet_TimeLabel" runat="server" Text='<%# Bind("[MeetTime]", "{0:h:mm tt}") %>' />
                    <br />
                    Start Time:
                    <asp:Label ID="Start_TimeLabel0" runat="server" Text='<%# Bind("[StartTime]", "{0:h:mm tt}") %>' />
                    <br />
                    End Time:
                    <asp:Label ID="End_TimeLabel0" runat="server" Text='<%# Bind("[EndTime]", "{0:h:mm tt}") %>' />
                    <br />
                    Each Member Pays:
                    <asp:Label ID="PayLabel" runat="server" Text='<%# Bind("Pay", "{0:c}") %>' />
                    <br />
                    Meals:
                    <asp:Label ID="MealsLabel" runat="server" Text='<%# Bind("Meals") %>' />
                    <br />
                    Transportation:
                    <asp:Label ID="TransportationLabel" runat="server" Text='<%# Bind("Transportation") %>' />
                    <br />
                    Description:
                    <asp:Label ID="DescriptionTextBox" runat="server" Text='<%# Bind("[Description]") %>' />
                    <br />
                    <br />
                </ItemTemplate>
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            </asp:FormView>
        </LoggedInTemplate>
        <RoleGroups>
            <asp:RoleGroup Roles="Officers, Teachers, Administrators">
                <ContentTemplate>
                    <asp:FormView ID="CompetitionDetailsForm1" runat="server" CellPadding="4" DataKeyNames="ID"
                        DataSourceID="CompetitionDetailsDataSource" ForeColor="#333333" HeaderText="Competition Details">
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <ItemTemplate>
                            Name:
                            <asp:Label ID="NameLabel1" runat="server" Text='<%# Bind("Name") %>' />
                            <br />
                            Location:
                            <asp:Label ID="LocationLabel1" runat="server" Text='<%# Bind("Location") %>' />
                            <br />
                            Date:
                            <asp:Label ID="DateLabel1" runat="server" Text='<%# Bind("Date", "{0:ddd. MMMM d, yyyy}") %>' />
                            <br />
                            Meet Time:
                            <asp:Label ID="Meet_TimeLabel0" runat="server" Text='<%# Bind("[MeetTime]", "{0:h:mm tt}") %>' />
                            <br />
                            Start Time:
                            <asp:Label ID="Start_TimeLabel1" runat="server" Text='<%# Bind("[StartTime]", "{0:h:mm tt}") %>' />
                            <br />
                            End Time:
                            <asp:Label ID="End_TimeLabel1" runat="server" Text='<%# Bind("[EndTime]", "{0:h:mm tt}") %>' />
                            <br />
                            Fees Per Person:
                            <asp:Label ID="FeesLabel" runat="server" Text='<%# Bind("Fees", "{0:c}") %>' />
                            <br />
                            Each Member Pays:
                            <asp:Label ID="PayLabel0" runat="server" Text='<%# Bind("Pay", "{0:c}") %>' />
                            <br />
                            Meals:
                            <asp:Label ID="MealsLabel0" runat="server" Text='<%# Bind("Meals") %>' />
                            <br />
                            Transportation:
                            <asp:Label ID="TransportationLabel0" runat="server" Text='<%# Bind("Transportation") %>' />
                            <br />
                            Registered:
                            <asp:CheckBox ID="RegisteredCheckBox" runat="server" Checked='<%# Bind("Registered") %>'
                                Enabled="false" />
                            <br />
                            Paid:
                            <asp:CheckBox ID="PaidCheckBox" runat="server" Checked='<%# Bind("Paid") %>' Enabled="false" />
                            <br />
                            Registration Info:
                            <asp:Label ID="Registration_InfoLabel" runat="server" Text='<%# Bind("[RegistrationInfo]") %>' />
                            <br />
                            Description:
                            <asp:Label ID="DescriptionTextBox0" runat="server" Text='<%# Bind("[Description]") %>' />
                            <br />
                            <br />
                        </ItemTemplate>
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    </asp:FormView>
                </ContentTemplate>
            </asp:RoleGroup>
        </RoleGroups>
    </asp:LoginView>
</asp:Content>
