﻿<%@ Page Title="Newport Math Club &ndash; About" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        About the Club</h2>
    <p>
        Newport Math Club is an extracurricular activity at <a href="http://bsd405.org/Default.aspx?tabid=125"
            onclick="window.open(this.href,'newwin'); return false;">Newport High School</a>
        in the Bellevue School District, Bellevue, Washington. Our club meets weekly to
        practice, and we also attend many math competitions in the greater Seattle area
        and beyond.</p>
    <p>
        The address of Newport High School is as follows: 4333 Factoria Blvd. SE Bellevue,
        WA 98006. Click <a href="officers.aspx">here</a> to learn how to contact our club
        officers or the webmaster.</p>
    <h4>
        Our Mission</h4>
    <p>
        Our goal is to provide a positive environment to develop mathematical skills while
        improving problem solving techniques and accuracy in an enjoyable manner. Through
        working in individual and group settings, we strive to prepare our members to be
        stronger math students and competitors. We want to encourage working well as a team
        while also excelling as an individual.</p>
    <h4>
        Topics</h4>
    <p>
        Our curriculum goes beyond what is covered in district math classes and is meant
        to complement and not just repeat what is learned in class. For this reason, members
        are strongly encouraged to take math classes all four years of high school and challenge
        themselves by taking Advanced Placement classes such as AP Calculus and AP Statistics.
        Members are also encouraged to meet with their teachers during tutorial after school
        to reinforce concepts they learned in class. Since we build on knowledge gained
        in high school-level math classes, it is critical that members make sure they understand
        basic math concepts that they have been taught.</p>
    <h4>
        Communication &amp; Attendance</h4>
    <p>
        Our club strongly values communication between our officers and our members. If
        you have any suggestions for the officers, please let them know. All members need
        to register with the secretary upon joining the club to ensure that they receive
        the latest information and that they can be registered for competitions that they
        sign up to attend. Attendance will also be taken at meetings, so be sure to attend
        as many meetings and competitions as possible. Members are expected to make Math
        Club one of their top priorities, so attendance at all events is strongly encouraged,
        as well as necessary to improve math skills for competitions in a group environment.
        Members are also encouraged to bring friends to the club meetings.</p>
    <h4>
        Lettering</h4>
    <p>
        During the 2008-2009 school year, our club received permission from the Student
        Senate and from the school administration to award varsity letters to members who
        show outstanding dedication and performance in Math Club. We have developed a detailed
        <a href="lettering.aspx">lettering criteria</a> for members to follow if they are
        interested in earning this award. Getting this approved was a major step in getting
        word out about our club and rewarding members for representing our school in a positive
        manner in the community. Varsity letters are awarded each year at the end-of-the-year
        banquet.</p>
    <h4>
        Bylaws</h4>
    <p>
        Our bylaws govern the club activities and provide basic structure to its operations.
        Our <a href="Bylaws.pdf">current bylaws</a> were written and last ratified by the club
        officers and advisor on March 23, 2012.</p>
</asp:Content>
