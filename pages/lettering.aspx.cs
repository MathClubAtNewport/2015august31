﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_lettering : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        nameLabel.Text = User.Identity.Name;
        // 0 ID, 1 Date, 2 Item, 3 Points
        int points = 0;
        for (int i = 0; i < MyPointsGrid.Rows.Count; i++)
        {
            string s = MyPointsGrid.Rows[i].Cells[3].Text;
            if (s == null || s == "")
                break;
            int dPoints = Int32.Parse(s);
            points += dPoints;
        }
        //TotalPointsLabel.Text = points.ToString(); // not accessible
        Control c = LoginViewPoints.FindControl("TotalPointsLabel");
        if (c != null)
        {
            Label l = (Label)c;
            l.Text = points.ToString();
        }
    }
}