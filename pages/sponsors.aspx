<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" %>

<script runat="server">

</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Newport Math Club &ndash; Sponsors</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Sponsors</h2>
    
        <!--<p><strong>Thank you to all of our sponsors! Our club could not function without
            you.</p></strong>-->
    
    
        <p><strong>The NEW Sponsorship Information Packet for the next school year is now
            available!</p></strong>
    
    <h4>
        Become a Sponsor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
            class="style6"> </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="style5" href="SponsorshipInfoPacket11-12.pdf"><span class="style6">View our
            2011-2012 Sponsorship Information Packet &raquo;</span></a></h4>
    <p>
        Because of travel costs and registration fees, our club has a very tight budget.
        In addition, clubs do not get as much funding as sports, and the funds we receive
        from our school ASB are small.&nbsp; As a result, our club relies primarily on the
        generous support of local businesses and families.</p>
    <p>
        Competition fees are generally around $10 per person each contest, but they can
        be more. In order to encourage more members to attend these contests, we try to
        subsidize part of the costs, requiring money on top of what we receive from ASB.
    </p>
    <p>
        Our club is looking for sponsors to help provide additional funds that will offset
        club costs, including:
    </p>
    <ul>
        <li><strong>Contest registration fees</strong></li>
        <li><b>Fee for hosting the <a href="contests.aspx">AMC</a> for 200+ Newport High School
            students</b></li>
        <li>Transportation to and from contests</li>
        <li>Test materials for our <a href="../kpmt/default.aspx">KPMT community competition
        </a></li>
        <li>Awards for our <a href="../kpmt/default.aspx">KPMT community competition</a>
        </li>
        <li>Bus to Blaine, WA for state championships</li>
        <li>Materials (paper, pencils, calculators, rulers, etc.) </li>
        <li>Website hosting fees </li>
    </ul>
    <p>
        We have created a system of rewards for our corporate sponsors, including advertising
        on this website and on our team apparel. If you are interested, please take a look
        at our <a href="SponsorshipInfoPacket10-11.pdf">Sponsorship Information Packet</a>,
        which contains much more information about the benefits of being a sponsor and how
        to contact us.</p>
    <p>
        If you are interested in sponsoring our club, we would love to talk with you! Please
        feel free to contact one of our <a href="officers.aspx">officers</a>. Any help is
        greatly appreciated! Thank you for your support of Newport Math Club.</p>
    <!-- #EndEditable -->
</asp:Content>
