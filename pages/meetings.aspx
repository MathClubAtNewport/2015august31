﻿<%@ Page Title="Newport Math Club &ndash; Meetings" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="meetings.aspx.cs" Inherits="pages_meetings" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Meetings</h2>
    <p>
        Below is a list of team meetings and the associated details. Meeting minutes and
        other meeting resources can be downloaded when available if you are logged in. Upcoming
        meetings are shown at the top of the list by default.</p>
    <asp:Panel ID="PresentExplainPanel" runat="server">
        The &quot;Present&quot; column shows at which meetings you were marked as being
        in attendance. Click <a href="../members/mymeetings.aspx">here</a> to see this information
        for only the meetings which you attended.
    </asp:Panel>
    <asp:AccessDataSource runat="server" ID="MeetingsDataSource_Members" DataFile="../App_Data/MathClub.mdb"
        SelectCommand="SELECT [ID], [Date], [Type], (SELECT COUNT(*) FROM [Attendance] WHERE [Meeting] = [Meetings].[ID]) AS [Members], ((SELECT COUNT(*) FROM [Attendance] WHERE [Name] = ? AND [Meeting]=[Meetings].[ID]) = 1) AS [Present], [Minutes], [Notes] FROM [Meetings] WHERE [Type] <> ? AND [Type] <> ? ORDER BY [Date] DESC"
        OnSelecting="MeetingsDataSource_Members_Selecting">
        <SelectParameters>
            <asp:Parameter Name="MemberName" DefaultValue="" Type="String" />
            <asp:Parameter DefaultValue="Officer Meeting" Type="String" />
            <asp:Parameter DefaultValue="Competition" Type="String" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource runat="server" ID="MeetingsDataSource_Officers" DataFile="../App_Data/MathClub.mdb"
        SelectCommand="SELECT [ID], [Date], [Type], (SELECT COUNT(*) FROM [Attendance] WHERE [Meeting] = [Meetings].[ID]) AS [Members], ((SELECT COUNT(*) FROM [Attendance] WHERE [Name] = ? AND [Meeting]=[Meetings].[ID]) = 1) AS [Present], [Minutes], [Notes] FROM [Meetings] ORDER BY [Date] DESC"
        OnSelecting="MeetingsDataSource_Officers_Selecting">
        <SelectParameters>
            <asp:Parameter Name="MemberName" DefaultValue="" Type="String" />
        </SelectParameters>
    </asp:AccessDataSource>
    <!--<asp:AccessDataSource runat="server" id="AttendanceDataSource" DataFile="../App_Data/MathClub.mdb"
				SelectCommand="SELECT [ID], [Name], [Meeting] FROM [Attendance]">
				</asp:AccessDataSource>-->
    <br />
    <asp:GridView ID="MeetingsGrid" runat="server" Width="100%" AllowPaging="False" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="MeetingsDataSource_Members"
        ForeColor="#333333" GridLines="None" PageSize="20" OnPageIndexChanging="MeetingsGrid_PageIndexChanging">
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="" InsertVisible="False" ReadOnly="True"
                SortExpression="ID" Visible="False" />
            <asp:BoundField DataField="Date" DataFormatString="{0:M/d/yy h:mm tt}" HeaderText="Date"
                SortExpression="Date" ApplyFormatInEditMode="True" />
            <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
            <asp:BoundField DataField="Present" HeaderText="Present" SortExpression="Present" />
            <asp:BoundField DataField="Members" HeaderText="Members" SortExpression="Members" />
            <asp:HyperLinkField DataNavigateUrlFields="Minutes" Text="Download" HeaderText="Minutes"
                SortExpression="Minutes" />
            <asp:BoundField DataField="Notes" HeaderText="Notes" SortExpression="Notes" />
        </Columns>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
        <AlternatingRowStyle BackColor="White" />
        <RowStyle HorizontalAlign="Center" />
    </asp:GridView>
    <!-- <asp:TemplateField HeaderText="Present" >
			                <ItemTemplate>
			                    <asp:CheckBox ID="PresentCheckBox" runat="server" Checked="false" Enabled="false"/>
			                </ItemTemplate>
			            </asp:TemplateField>-->
    <!--<table class="style4">
			<tr>
			<td class="style11" style="width: 69px">
			<p class="style8"><strong>Date</strong></p>
				</td>
			<td style="width: 101px" class="style19">
			<p class="style10"><strong>Meeting Type</strong></p>
					</td>
			<td class="style17" style="width: 110px">
			<p class="style8"><strong>Members Attending</strong></p>
					</td>
			<td style="width: 88px" class="style19">
			<p class="style10"><strong>Minutes</strong></p>
					</td>
			<td style="width: 309px" class="style18">
			<p class="style10"><strong>Notes</strong></p>
					</td>
			</tr>
			<tr>
			<td style="width: 69px" class="style12">9/8/08&nbsp;</td>
			<td style="width: 101px" class="style16">TBA</td>
			<td style="width: 110px" class="style16">-&nbsp;</td>
			<td style="width: 88px" class="style16">-</td>
			<td style="width: 309px" class="style13">Meet in Mr. Nonis&#39; room at 
			3:00 PM.</td>
			</tr>
			<tr>
			<td style="width: 69px" class="style12">6/9/08&nbsp;</td>
			<td style="width: 101px" class="style16">Officer Meeting</td>
			<td style="width: 110px" class="style16">7&nbsp;</td>
			<td style="width: 88px" class="style16">
			<a href="Minutes/08-06/6-9-08%20Officer%20Meeting.rtf">
			<img alt="Minutes" height="32" longdesc="Minutes/08-06/6-9-08 Officer Meeting.rtf" src="download.bmp" width="32" /></a>&nbsp;</td>
			<td style="width: 309px" class="style13">Discussed more ideas for 
			2008-2009</td>
			</tr>
			<tr>
			<td style="width: 69px" class="style12">5/19/08</td>
			<td style="width: 101px" class="style16">Officer Meeting</td>
			<td style="width: 110px" class="style16">7</td>
			<td style="width: 88px" class="style16">
			<a href="Minutes/08-05/5-19-08%20Officer%20Meeting.rtf">
			<img alt="Minutes" height="32" longdesc="Minutes/08-05/5-19-08 Officer Meeting.rtf" src="download.bmp" width="32" /></a></td>
			<td style="width: 309px" class="style13">Discussed ideas for 
			2008-2009 school year</td>
			</tr>
			</table>-->
</asp:Content>
