<%@ Page Title="Newport Math Club &ndash; Login" Language="C#" MasterPageFile="~/Site.master" %>

<%@ Import Namespace="System.Net.Mail" %>
<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Login</h2>
    <p>
        This page allows members and administrators to log in to the website in order to
        access member-only features.</p>
    <table style="width: 100%">
        <tr>
            <td>
                <asp:LoginView ID="LoginBoxLoginView" runat="server">
                    <LoggedInTemplate>
                        Welcome,&nbsp;
                        <asp:LoginName ID="LoginName" runat="server" />
                        <br />
                        <br />
                        <a href="default.aspx">Back to Home Page</a>
                        <br />
                        <asp:LoginStatus ID="LoginStatus3" runat="server" />
                        &nbsp;&nbsp;
                    </LoggedInTemplate>
                    <AnonymousTemplate>
                        <asp:Login ID="LoginBox" runat="server" BackColor="#FFFBD6" BorderColor="#FFDFAD"
                            BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana"
                            Font-Size=".9em" ForeColor="#333333" Height="200px" TextLayout="TextOnTop" Width="280px">
                            <LoginButtonStyle BackColor="White" BorderColor="#CC9966" BorderStyle="Solid" BorderWidth="1px"
                                Font-Names="Verdana" Font-Size="1em" ForeColor="#990000" />
                            <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
                            <TitleTextStyle BackColor="#990000" Font-Bold="True" Font-Size="1.1em" ForeColor="White" />
                            <TextBoxStyle Width="170px" Font-Size="1em" />
                        </asp:Login>
                    </AnonymousTemplate>
                </asp:LoginView>
            </td>
            <td>
                <asp:PasswordRecovery ID="PasswordRecoveryBox" runat="server" BackColor="#FFFBD6"
                    BorderColor="#FFDFAD" BorderPadding="4" BorderStyle="Solid" BorderWidth="1px"
                    Font-Names="Verdana" Font-Size=".9em" Height="145px">
                    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
                    <SuccessTextStyle Font-Bold="True" ForeColor="#990000" />
                    <TextBoxStyle Font-Size="1em" />
                    <TitleTextStyle BackColor="#990000" Font-Bold="True" Font-Size="1.1em" ForeColor="White" />
                    <SubmitButtonStyle BackColor="White" BorderColor="#CC9966" BorderStyle="Solid" BorderWidth="1px"
                        Font-Names="Verdana" Font-Size="1em" ForeColor="#990000" />
                </asp:PasswordRecovery>
            </td>
        </tr>
    </table>
</asp:Content>
