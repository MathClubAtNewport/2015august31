<%@ Page Title="Newport Math Club" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .kpmtAdCaption
        {
            <%--color: #A80000;--%>
            font-size: small;
            font-style: italic;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Welcome to Newport Math Club!</h2>
    <table class="style1">
        <tr>
            <td style="vertical-align: top; padding-left: 0; margin-left: 0;"> 
<p style ="color=Red"> <center>Stay tuned to the website for more information for the coming year! </center> </p>
<!--                 
<p style ="color:#00FFFF"; "font-size:20">
Check out our <b> NEW News Page </b> <a href = "pages/testnewstemplate.aspx">here</a> for the latest up-to-date information!
</p>
--!>
		<p>
                    Newport Math Club is a math team at <a href="http://bsd405.org/Default.aspx?tabid=125"
                        onclick="window.open(this.href,'newwin'); return false;">Newport High School</a>
                    in Bellevue, Washington. We meet weekly and attend local math competitions in the
                    greater Seattle area.</p>
                <p>
                    If you are new to our club, welcome to our website! Feel free to take a look around.
                    Make sure to view our <a href="pages/competitions.aspx">upcoming competitions</a>
                    and our <a href="pages/achievements.aspx">achievements</a> at past competitions.
                    Information on how to contact our officers is available on our <a href="pages/officers.aspx">
                        officers</a> page. You can also read more information <a href="pages/about.aspx">about
                            the club</a>.</p>
                <p>
                    Our club will be meeting on Friday afternoons from 3:05 to 4:10 in Ms. Northey&#39; room (2103) for the 2015-2016 school year.
                    New members are always welcome! Come ready to have fun and improve your math skills.
                    Members are strongly encouraged to attend all meetings and competitions, as these
                    are the best tools to see progress and set goals for improvement. Our most dedicated
                    members spend time outside of the club meetings sharpening their skills to improve
                    their results at upcoming competitions. Math <a href="pages/articles.aspx">articles</a>
                    and links to <a href="pages/practice.aspx">practice problems</a> are available on
                    our site.</p>
            </td>
            <td style="width: 202px; margin-left: 10px; padding: 5px; border: 2px #e6d98f solid; text-align: center">
                <a href="kpmt/default.aspx">
                    <asp:Image ID="imgLogo" runat="server" ImageUrl="~/kpmt/kpmt.png" Width="180px" ImageAlign="Top" /></a>
                <span class="kpmtAdCaption">The 6th annual Knights of Pi Math Tournament (KPMT) has ended! Click on the
                    image above to learn more about the contest and see past results.</span>
				<!--<span class="kpmtAdCaption">The 6th annual Knights of Pi Math Tournament (KPMT) for
                    students in grades 5&ndash;8 will be held on December 14, 2013. Click on the
                    image above to learn more about the contest and register online.</span>-->
            </td>
        </tr>
    </table>
    <p>
        We strive to establish a positive environment where members help each other learn
        new concepts and increase their knowledge, accuracy, and speed of problem solving.
        We also hope to share our love of math with other students in the community. If
        you are a business or individual interested in <a href="pages/sponsors.aspx">sponsoring
            our club</a>, we would love to talk with you! Just contact one of our <a href="pages/officers.aspx">
                officers</a>.</p>
    <p>
        Thank you for your support of Newport Math Club. Happy solving!</p>
    <p>
    <br>
<i> Questions? Comments? Concerns? Just contact one or all of our officers via email <a href="pages/officers.aspx"> here.</a>
    </p>
</asp:Content>
