﻿<%@ Page Title="Knights of Pi Math Tournament - Documents &amp; Materials" Language="C#"
    MasterPageFile="~/kpmt/kpmt.master" %>

<script runat="server">

</script>
<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            text-align: left;
        }
        .style15
        {
            font-size: large;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>Documents &amp; Materials</h2>
    <p>
        Use the documents below to gain valuable information about the Knights of Pi Math
        Tournament. All of the resources are available in .pdf format. If you have any further
        questions, please feel free to <a href="contact.aspx">contact us</a>.</p>
    <table>
        <tr>
            <td class="style15" style="border-width: 5px; text-align: right; padding-right: 10px;
                border-bottom-style: solid; border-bottom-color: #FF0000;">
                <h3 class="style1">
                    KPMT 2014-2015 Information &amp; Resources
                </h3>
            </td>
        </tr>
        <tr>
            <td class="style15">
                <ul>
                    <li><a href="pdf/14-15/Support/KPMT%2014-15%20Invitation%20Letter.pdf">Invitation Letter</a></li>
                    <li><a href="pdf/14-15/Support/KPMT%2014-15%20Schedule%20of%20Events.pdf">Schedule of Events</a></li>
                    <li><a href="pdf/14-15/Support/KPMT%2014-15%20FAQ.pdf">Frequently Asked Questions</a></li>
                    <li><a href="pdf/14-15/Support/KPMT%2014-15%20Registration%20Payment%20Guide.pdf">Registration and Payment Guide</a></li>
                    <li><a href="pdf/14-15/Support/KPMT%2014-15%20Tests%20and%20Scoring.pdf">Tests and Scoring</a></li>
                    <li><a href="pdf/14-15/Support/KPMT%2014-15%20Scoring%20Guidelines%20and%20Tips.pdf">Scoring Guidelines and Tips</a></li>
                    <li><a href="pdf/KPMT%20Feedback%20Form.pdf">Coaches&#39; Feedback Form</a></li>
                </ul>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="style15" style="border-width: 5px; text-align: right; padding-right: 10px;
                border-bottom-style: solid; border-bottom-color: #FF0000;">
                <h3 class="style1">
                    KPMT 2013-2014 Information &amp; Resources
                </h3>
            </td>
        </tr>
        <tr>
            <td class="style15">
                <ul>
                    <li><a href="pdf/13-14/Support/KPMT%2013-14%20Invitation%20Letter.pdf">Invitation Letter</a></li>
                    <li><a href="pdf/13-14/Support/KPMT%2013-14%20Schedule%20of%20Events.pdf">Schedule of Events</a></li>
                    <li><a href="pdf/13-14/Support/KPMT%2013-14%20FAQ.pdf">Frequently Asked Questions</a></li>
                    <li><a href="pdf/13-14/Support/KPMT%2013-14%20Registration%20Payment%20Guide.pdf">Registration and Payment Guide</a></li>
                    <li><a href="pdf/13-14/Support/KPMT%2013-14%20Tests%20and%20Scoring.pdf">Tests and Scoring</a></li>
                    <li><a href="pdf/13-14/Support/KPMT%2013-14%20Scoring%20Guidelines%20and%20Tips.pdf">Scoring Guidelines and Tips</a></li>
                    <li><a href="pdf/KPMT%20Feedback%20Form.pdf">Coaches&#39; Feedback Form</a></li>
                </ul>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="style15" style="border-width: 5px; text-align: right; padding-right: 10px;
                border-bottom-style: solid; border-bottom-color: #FF0000;">
                <h3 class="style1">
                    KPMT 2012-2013 Information &amp; Resources
                </h3>
            </td>
        </tr>
        <tr>
            <td class="style15">
                <ul>
                    <li><a href="pdf/12-13/Support/KPMT%2012-13%20Invitation%20Letter.pdf">Invitation Letter</a></li>
                    <li><a href="pdf/12-13/Support/KPMT%2012-13%20Schedule%20of%20Events.pdf">Schedule of
                        Events</a></li>
                    <li><a href="pdf/12-13/Support/KPMT%2012-13%20FAQ.pdf">Frequently Asked Questions</a></li>
                    <li><a href="pdf/12-13/Support/KPMT%2012-13%20Registration%20&%20Payment%20Guide.pdf">Registration
                        &amp; Payment Guide</a></li>
                    <li><a href="pdf/12-13/Support/KPMT%2012-13%20Tests%20&%20Scoring.pdf">Tests &amp; Scoring</a></li>
                    <li><a href="pdf/12-13/Support/KPMT%2012-13%20Scoring%20Guidelines%20&%20Tips.pdf">Scoring
                        Guidelines &amp; Tips</a></li>
                    <li><a href="pdf/12-13/Support/KPMT%2012-13%20Feedback%20Form.pdf">Coaches&#39; Feedback
                        Form</a></li>
                </ul>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="style15" style="border-width: 5px; text-align: right; padding-right: 10px;
                border-bottom-style: solid; border-bottom-color: #FF0000;">
                <h3 class="style1">
                    KPMT 2011-2012 Information &amp; Resources
                </h3>
            </td>
        </tr>
        <tr>
            <td class="style15">
                <ul>
                    <li><a href="pdf/11-12/Support/KPMT%2011-12%20Invitation%20Letter.pdf">Invitation Letter</a></li>
                    <li><a href="pdf/11-12/Support/KPMT%2011-12%20Schedule%20of%20Events.pdf">Schedule of
                        Events</a></li>
                    <li><a href="pdf/11-12/Support/KPMT%2011-12%20FAQ.pdf">Frequently Asked Questions</a></li>
                    <li><a href="pdf/11-12/Support/KPMT%2011-12%20Registration%20Payment%20Guide.pdf">Registration
                        &amp; Payment Guide</a></li>
                    <li><a href="pdf/11-12/Support/KPMT%2011-12%20Tests%20Scoring.pdf">Tests &amp; Scoring</a></li>
                    <li><a href="pdf/11-12/Support/KPMT%2011-12%20Scoring%20Guidelines%20Tips.pdf">Scoring
                        Guidelines &amp; Tips</a></li>
                    <!--<li><a href="pdf/11-12/Support/KPMT%2011-12%20Feedback%20Form.pdf">Coaches&#39; Feedback
                        Form</a></li>-->
                </ul>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="style15" style="border-width: 5px; text-align: right; padding-right: 10px;
                border-bottom-style: solid; border-bottom-color: #FF0000;">
                <h3 class="style1">
                    KPMT 2010-2011 Information &amp; Resources
                </h3>
            </td>
        </tr>
        <tr>
            <td class="style15">
                <ul>
                    <li><a href="pdf/2010/Support/KPMT%202010%20Invitation%20Letter.pdf">Invitation Letter</a></li>
                    <li><a href="pdf/2010/Support/KPMT%202010%20Schedule%20of%20Events.pdf">Schedule of
                        Events</a></li>
                    <li><a href="pdf/2010/Support/KPMT%202010%20FAQ.pdf">Frequently Asked Questions</a></li>
                    <li><a href="pdf/2010/Support/KPMT%202010%20Registration%20Payment%20Guide.pdf">Registration
                        &amp; Payment Guide</a></li>
                    <li><a href="pdf/2010/Support/KPMT%202010%20Tests%20Scoring.pdf">Tests &amp; Scoring</a></li>
                    <li><a href="pdf/2010/Support/KPMT%202010%20Scoring%20Guidelines%20Tips.pdf">Scoring
                        Guidelines &amp; Tips</a></li>
                    <li><a href="pdf/2010/Support/KPMT%202010%20Feedback%20Form.pdf">Coaches&#39; Feedback
                        Form</a></li>
                </ul>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="style15" style="border-width: 5px; text-align: right; padding-right: 10px;
                border-bottom-style: solid; border-bottom-color: #FF0000;">
                <h3 class="style1">
                    KPMT 2009-2010 Information &amp; Resources
                </h3>
            </td>
        </tr>
        <tr>
            <td class="style15">
                <ul>
                    <li><a href="pdf/09-10/Support/KPMT%2009-10%20Invitation%20Letter.pdf">Invitation Letter</a></li>
                    <li><a href="pdf/09-10/Support/KPMT%2009-10%20Schedule%20of%20Events.pdf">Schedule of
                        Events</a></li>
                    <li><a href="pdf/09-10/Support/KPMT%2009-10%20FAQ.pdf">Frequently Asked Questions</a></li>
                    <li><a href="pdf/09-10/Support/KPMT%2009-10%20Registration%20Payment%20Guide.pdf">Registration
                        &amp; Payment Guide</a></li>
                    <li><a href="pdf/09-10/Scoring/KPMT%2009-10%20Tests%20Scoring.pdf">Tests &amp; Scoring</a></li>
                    <li><a href="pdf/09-10/Scoring/KPMT%2009-10%20Scoring%20Guidelines%20Tips.pdf">Scoring
                        Guidelines &amp; Tips</a></li>
                    <li><a href="pdf/09-10/Support/KPMT%2009-10%20Feedback%20Form.pdf">Coaches&#39; Feedback
                        Form</a></li>
                </ul>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="style15" style="border-width: 5px; text-align: right; padding-right: 10px;
                border-bottom-style: solid; border-bottom-color: #FF0000;">
                <h3 class="style1">
                    KPMT 2008-2009 Information &amp; Resources
                </h3>
            </td>
        </tr>
        <tr>
            <td class="style15">
                <ul>
                    <li><a href="pdf/2009/Support/KPMT%202009%20Invitation%20Letter.pdf">Invitation Letter</a></li>
                    <li><a href="pdf/2009/Support/KPMT%202009%20Schedule%20of%20Events.pdf">Schedule of
                        Events</a></li>
                    <li><a href="pdf/2009/Support/KPMT%202009%20FAQ.pdf">Frequently Asked Questions</a></li>
                    <li><a href="pdf/2009/Support/KPMT%202009%20Registration%20Payment%20Guide.pdf">Registration
                        &amp; Payment Guide</a></li>
                    <li><a href="pdf/2009/Support/KPMT%202009%20Tests%20&%20Scoring.pdf">Tests &amp; Scoring</a></li>
                    <li><a href="pdf/2009/Support/KPMT%202009%20Scoring%20Guidelines%20&%20Tips.pdf">Scoring
                        Guidelines &amp; Tips</a> </li>
                </ul>
            </td>
        </tr>
    </table>
</asp:Content>
