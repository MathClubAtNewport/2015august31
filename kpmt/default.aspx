﻿<%@ Page Title="Knights of Pi Math Tournament (KPMT)" Language="C#" MasterPageFile="~/kpmt/kpmt.master" %>

<script runat="server">

</script>
<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Knights of Pi Math Tournament
    </h2>
    <p>
        Welcome to Newport High School&#39;s <b>Knights of Pi Math Tournament</b>! This
        exciting and challenging math contest for students in grades 5-8 is hosted by Newport
        Math Club, the math team at Newport High School. The tournament is designed to be
        a positive and fun math experience for all involved and provide the highly motivated
        students a chance to demonstrate what they have learned throughout the year. If
        you have any questions about the competition, feel free to <a href="contact.aspx">let
            us know</a>.<br />
        <br />
        <b>2014-2015 KPMT Date: Saturday, December 20, 2014</b>
        <br />
        <br />
        The Coaches' Feedback Form is available online. Coaches can go <a href="pdf/KPMT%20Feedback%20Form.pdf">here</a> and fill out electronic feedback forms about Knights of Pi Math Tournament.
        <br />
        <br />
        Visit the links above to learn more about the competition.</p>
    <h3>
        Register Online
    </h3>
    <p>
        <b>Registration is now closed!</b></p>
    <h3>
        Registration Fees &amp; Deadlines</h3>
    <table style="width: 100%">
        <tr>
            <td style="height: 21px">
            </td>
            <td class="style1" style="height: 21px">
                Fee
            </td>
            <td class="style1" style="height: 21px">
                Register Teams by:
            </td>
            <td class="style1" style="height: 21px">
                Enter Students’ Names by:
            </td>
        </tr>
        <tr>
            <td>
                Normal Registration Period:
            </td>
            <td>
                $40 / team
            </td>
            <td>
                Sun. November 23</td>
            <td>
                Sun. December 7</td>
        </tr>
        <tr>
            <td>
                Late Registration Period:
            </td>
            <td>
                $54 / team
            </td>
            <td>
                Sun. December 7</td>
            <td>
                Sun. December 7</td>
        </tr>
    </table>
    <!--<td style="padding-left: 3px; vertical-align:top;">
                                <img alt="KPMT 2009-2010" src="KPMT_09-10-link.png" width="400" /><br />
								<asp:Image ID="imgLogo" runat="server" ImageUrl="~/images/kpmt.png" 
                                    Width="200px" ImageAlign="Top" />
                            </td>-->
    <h3>
        History</h3>
    <p>
        The fifth annual KPMT Tournament was held on Saturday, December 15th, 2012, featuring
        a huge expansion in tournament attendance with an amazing 108 teams signed up, 20 more
        than the previous year. The special round was again the relay round and we had special
        questions at the end to recite the digits of pi and to solve interesting puzzles.
        A special thanks to all our math club and National Honors Society volunteers, as well as
        the parents and teachers who contributed. We could not have done it without you.<br />
        <br />
        The fourth annual KPMT Tournament was held on Saturday, January 28, 2012, featuring
        a new relay round in which teams had to cooperate in order to answer questions both
        quickly and accurately and relay the answer down to the next person to be used in the
        next question. A special thanks to all our math club and National Honor Society volunteers,
        as well as the parents, and teachers who contributed.<br />
        <br />
        The third annual KPMT Tournament was held on Saturday, December 4, 2010, featuring
        the tournament's greatest attendance yet and KPMT Live! where teams faced off in
        exciting rounds of Jeopardy and College Bowl for exclusive prizes. A special thanks
        to all our math club and National Honor Society volunteers, as well as the parents
        and teachers who contributed.<br />
        <br />
        The second annual KPMT Tournament was held on Saturday, December 12, 2009, featuring
        the new Joust! round in which teams raced to complete various math- and logic-related
        activities. A special thanks to all our math club and National Honor Society volunteers,
        as well as the parents, teachers, and sponsors who contributed.<br />
        <br />
        The first annual KPMT Tournament was held on Saturday, May 16, 2009. Thanks to all
        everyone who attended, and a special thanks to all the wonderful coaches, parents,
        volunteers, and sponsors.<br />
        <br />
        See more information about past tests and results in our <a href="archive.aspx">archive</a>.<br />
        <br />
    </p>
</asp:Content>
