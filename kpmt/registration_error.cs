﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class kpmt_registration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (chkCompleteCorrect.Attributes.Count == 0)
            chkCompleteCorrect.Attributes.Add("onclick", "enableButton();");
    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        // Reset
        lblRequired.Visible = false;
        lblDuplicate.Visible = false;
        lblBadEmail.Visible = false;
        lblRules.Visible = false;
        lblPasswordMismatch.Visible = false;
        lblError.Visible = true;

        // Verify Checkbox
        if (!chkCompleteCorrect.Checked)
        {
            lblRules.Visible = true;
            return;
        }

        // Verify Email
        String email = ((TextBox)(RegistrationForm.FindControl("EmailTextBox"))).Text;
        if (!isValidEmail(email))
        {
            lblBadEmail.Visible = true;
            return;
        }
        String password1 = ((TextBox)(RegistrationForm.FindControl("PasswordTextBox"))).Text;
        String password2 = ((TextBox)(RegistrationForm.FindControl("PasswordTextBox2"))).Text;
        if (!password1.Equals(password2))
        {
            lblPasswordMismatch.Visible = true;
            return;
        }

        // Try to create account
        try
        {
            RegistrationForm.InsertItem(true);
        }
        catch (Exception ex)
        {

            if (ex.Message.Contains("Required"))
                // Required Field Left Blank
                lblRequired.Visible = true;
            else if (ex.Message.Contains("duplicate"))
                // Duplicate Name or Username
                lblDuplicate.Visible = true;
            else
                // Unknown Error
                sendMessage("webmaster@newportmathclub.org", "webmaster@newportmathclub.org", "Error",
                    "Error in registration for Knights of Pi Math Tournament.\n" +
                    "catch statement, btnRegister_Click, registration.aspx\n\n"
                    + ex.ToString() + "\n" + ex.Message);
            return;
        }

        // Registration Successful!
        lblError.Visible = false; // no error
        //ShowAlert("Account created successfully! Please login.");
        sendMessage("webmaster@newportmathclub.org", "webmaster@newportmathclub.org", "KPMT Registration",
                    "New registration completed for Knights of Pi Math Tournament.\n" +
                    "contests/kpmt/registration.aspx\n\n"
                    + ((TextBox)(RegistrationForm.FindControl("School_NameTextBox"))).Text + "\n"
                    + ((TextBox)(RegistrationForm.FindControl("CityTextBox"))).Text + ", "
                    + ((TextBox)(RegistrationForm.FindControl("StateTextBox"))).Text + "\n"
                    + ((TextBox)(RegistrationForm.FindControl("Coach_NameTextBox"))).Text + "\n");
        Response.Redirect("registration-success.aspx");
    }

    public void sendMessage(String from, String to, String subject, String message)
    {
        try
        {
            System.Net.Mail.MailMessage myMessage = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress myFrom = new System.Net.Mail.MailAddress(from);
            myMessage.From = myFrom;
            System.Net.Mail.MailAddress myTo = new System.Net.Mail.MailAddress(to);
            myMessage.To.Add(myTo);
            myMessage.Body = message;
            myMessage.Subject = subject;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Send(myMessage);
        }
        catch (Exception)
        {
            ShowAlert("An error occured.");
        }
    }

    public static void ShowAlert(string message)
    {
        // Cleans the message to allow single quotation marks
        string cleanMessage = message.Replace("'", "\\'");
        string script = "<script type=\"text/javascript\">alert('" + cleanMessage;
        script += "');<";
        script += "/script>";

        // Gets the executing web page
        Page page = HttpContext.Current.CurrentHandler as Page;

        // Checks if the handler is a Page and that the script isn't allready on the Page
        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
        {
            page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", script);
        }
    }

    // Tests if an email address is valid.
    public static bool isValidEmail(String s)
    {
        if (s.Length < 5)
            return false;
        int at = s.IndexOf('@', 1);
        int dot = s.IndexOf('.', at + 2);
        return at > 0 && dot > 0 && dot < s.Length - 1;
    }
}