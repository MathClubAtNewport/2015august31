﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

public partial class kpmt_coaches : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Update();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        lblUsername.Text = txtUsername.Text;
        lblPassword.Text = txtPassword.Text;


        // Yes, you are seeing an error because the page is under construction.
        // Maybe it'll work once I stop getting FTP errors. Come back soon! 
        nameGrid.DataBind();

        // Haha I wonder if anyone will see this?

        emailGrid.DataBind();
        lblLoginError.Visible = false;
        lblUsernameError.Visible = false;
        btnEmail.Visible = false;
        if (nameGrid.Rows.Count <= 0)
        {

            if (emailGrid.Rows.Count <= 0)
                lblUsernameError.Visible = true;
            else
            {
                lblLoginError.Visible = true;
                btnEmail.Visible = true;
            }
        }
        else
        {
            lblName.Text = ", " + nameGrid.Rows[0].Cells[1].Text + "!";
            pnlLogin.Visible = false;
            pnlOptions.Visible = true;
            lblSchoolID.Text = nameGrid.Rows[0].Cells[0].Text;
        }

    }

    public static void ShowAlert(string message)
    {
        string cleanMessage = message.Replace("'", "\\'");
        string script = "<script type=\"text/javascript\">alert('" + cleanMessage;
        script += "');<";
        script += "/script>";
        Page page = HttpContext.Current.CurrentHandler as Page;
        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
            page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", script);
    }

    protected void btnEmail_Click(object sender, EventArgs e)
    {
        try
        {
            emailGrid.DataBind();
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress from = new System.Net.Mail.MailAddress("webmaster@newportmathclub.org", "Newport Math Club");
            message.From = from;
            String email = emailGrid.Rows[0].Cells[0].Text;
            String username = txtUsername.Text;
            IEnumerator data = PasswordData.Select(DataSourceSelectArguments.Empty).GetEnumerator();
            data.MoveNext();
            String password = ((System.Data.DataRowView)(data.Current))[0].ToString();
            String name = emailGrid.Rows[0].Cells[1].Text;
            System.Net.Mail.MailAddress to = new System.Net.Mail.MailAddress(email, name);
            message.To.Add(to);
            message.Body = "Knights of Pi Math Tournament\n\n" + name + "\nUsername: " + username + "\nPassword: " + password + "\n\n" +
                "Please return to www.newportmathclub.org/contests/kpmt/default.aspx to login and manage your registration.\n\n-Newport Math Club";
            message.Subject = "Knights of Pi Math Tournament - Login Details";
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Send(message);
            ShowAlert("Your password has been emailed to you at " + email + ".");
        }
        catch (Exception ex)
        {
            ShowAlert("An error occurred during the password recovery process. Please contact the webmaster.\n\nDetails:" + ex.Message);
        }
    }

    protected void Update()
    {
        TeamsGrid.DataBind();
        TeamMembersGrid.DataBind();
        CompetitorsGrid.DataBind();
        CommandField competitorsCF = (CommandField)(CompetitorsGrid.Columns[0]);
        competitorsCF.ShowSelectButton = (TeamMembersGrid.Rows.Count < 4) && (TeamsGrid.SelectedRow != null);
        lblTeamFull.Visible = (TeamMembersGrid.Rows.Count >= 4);
    }



    protected void TeamsGrid_SelectedIndexChanged(object sender, EventArgs e) { Update(); }

    protected void CompetitorsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CompetitorsGrid.SelectedIndex >= 0 && CompetitorsGrid.SelectedIndex < CompetitorsGrid.Rows.Count)
            TeamMembersData.Insert();
        CompetitorsGrid.SelectedIndex = -1;
        Update();
        CompetitorsGrid.DataBind();
    }

    protected void TeamsData_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        Update();
    }

    protected void CompetitorsData_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        Update();
    }

    protected void TeamMembersData_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        Update();
    }

    protected void TeamsGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int i = TeamsGrid.SelectedIndex;
        TeamsGrid.SelectedIndex = e.RowIndex;
        if (TeamMembersGrid.Rows.Count > 0)
        {
            ShowAlert("You cannot delete a team until all of its members have been removed.");
            e.Cancel = true;
        }
        TeamsGrid.SelectedIndex = i;
    }
}