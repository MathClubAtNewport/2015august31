﻿<%@ Page Title="Knights of Pi Math Tournament - Coaches&#39; Login" Language="C#"
    MasterPageFile="~/kpmt/kpmt.master" AutoEventWireup="true" CodeFile="coaches.aspx.cs"
    Inherits="kpmt_coaches" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style17
        {
            color: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Coaches&#39; Login</h2>
    <asp:AccessDataSource ID="LoginData" runat="server" DataFile="~/App_Data/KPMT.mdb"
        SelectCommand="SELECT [ID], [Coach Name] FROM [Schools] WHERE (([Username] = ?) AND ([Password] = ?))">
        <SelectParameters>
            <asp:ControlParameter ControlID="lblUsername" Name="Username" PropertyName="Text"
                Type="String" DefaultValue="no user" />
            <asp:ControlParameter ControlID="lblPassword" Name="Password" PropertyName="Text"
                Type="String" DefaultValue="no password" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="PasswordData" runat="server" DataFile="~/App_Data/KPMT.mdb"
        SelectCommand="SELECT [Password] FROM [Schools] WHERE ([Username] = ?)">
        <SelectParameters>
            <asp:ControlParameter ControlID="lblUsername" Name="Username" PropertyName="Text"
                Type="String" DefaultValue="no user" />
            <asp:ControlParameter ControlID="lblPassword" Name="Password" PropertyName="Text"
                Type="String" DefaultValue="no password" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:Panel ID="pnlLogin" runat="server">
        <p>
            Coaches can use this page to log in and manage their registrations of teams
            and individuals. If you do not yet have a coach account, please take a look at our
            <a href="docs.aspx">contest materials</a> and<b> <a href="registration.aspx">register
                your school</a></b> to get started on the fun right away!
            <br />
            The contest will take place at Newport High School, 4333 Factoria Blvd. SE, Bellevue,
            WA 98006.
        </p>
        <p>
            <span class="style17">
            Online Registration is currently closed.
            
             If you wish to make a late registration or need to change 
					your existing registration, feel free to email us at <a href="mailto:kpmt@newportmathclub.org">kpmt@newportmathclub.org</a>
					 and we will do our best to assist you. 
            </span>

            Online Registration is <strong>closed</strong> for the 2013-2014 KPMT.
            Please <a href="contact.aspx">contact us</a> if you have any questions.
        </p>
        <table style="width: 475px; height: 81px;">
            <tr>
                <td colspan="3">
                    <hr style="height: -12px; margin-bottom: 0px" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h3>
                        <b>Math Coaches Login</b></h3>
                </td>
                <td class="style3" rowspan="3">
                    <a href="registration.aspx">
                        <img id="img2" align="right" alt="Register Your School" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img1',/*url*/'buttons/button5.gif')"
                            onmouseout="FP_swapImg(0,0,/*id*/'img1',/*url*/'buttons/button3.gif')" onmouseover="FP_swapImg(0,0,/*id*/'img1',/*url*/'.buttons/button4.gif')"
                            onmouseup="FP_swapImg(0,0,/*id*/'img1',/*url*/'buttons/button4.gif')" src="buttons/button3.gif"
                            style="border: 0" width="200" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 10; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-transparent: 1; fp-proportional: 0" fp-title="Register Your School" --></a>
                </td>
            </tr>
            <tr>
                <td style="width: 30px;">
                    Username:
                </td>
                <td class="style12">
                    <asp:TextBox ID="txtUsername" runat="server" Style="margin-top: 0px" Width="150px"
                        Enabled="False"></asp:TextBox><!--To open registration, Enabled="False"-->
                </td>
            </tr>
            <tr>
                <td style="width: 30px;">
                    Password:
                </td>
                <td class="style12">
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="150px" Enabled="False"></asp:TextBox>
                    <!--To open registration, Enabled="False"-->
                </td>
            </tr>
        </table>
        <p>
            <asp:Label ID="lblUsernameError" runat="server" ForeColor="Red" Text="Username not found. Have you registered your school for this contest?"
                Visible="False"></asp:Label>
            <asp:Label ID="lblLoginError" runat="server" ForeColor="Red" Text="Incorrect password. Please try again."
                Visible="False"></asp:Label>
        </p>
        <p>
            <asp:Button ID="btnSubmit" runat="server" Enabled="False" OnClick="btnSubmit_Click"
                Text="Submit" /><!--To open registration, Enabled="False"-->
            <asp:Button ID="btnEmail" runat="server" OnClick="btnEmail_Click" Text="Email my Password"
                Visible="False" Enabled="True" />
            <asp:GridView ID="nameGrid" runat="server" AutoGenerateColumns="False" DataSourceID="LoginData"
                Visible="False">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="School ID" SortExpression="ID" />
                    <asp:BoundField DataField="Coach Name" HeaderText="Name" SortExpression="Coach Name" />
                </Columns>
            </asp:GridView>
            <asp:GridView ID="emailGrid" runat="server" AutoGenerateColumns="False" DataSourceID="EmailData"
                Visible="false">
                <Columns>
                    <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                    <asp:BoundField DataField="Coach Name" HeaderText="Name" SortExpression="Coach Name" />
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblSchoolID" runat="server"></asp:Label>
        </p>
    </asp:Panel>
    <asp:AccessDataSource ID="EmailData" runat="server" DataFile="~/App_Data/KPMT.mdb"
        SelectCommand="SELECT [ID], [Email], [Password], [Coach Name] FROM [Schools] WHERE ([Username] = ?)">
        <SelectParameters>
            <asp:ControlParameter ControlID="lblUsername" Name="Username" PropertyName="Text"
                Type="String" DefaultValue="no user" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:Panel ID="pnlOptions" runat="server" Visible="false">
        <hr />
        <p>
            <b>Welcome</b><b><asp:Label ID="lblName" runat="server" Text="!"></asp:Label>
            </b>&nbsp;<asp:Label ID="lblUsername" runat="server" Text="&lt;Username&gt;" Visible="false"></asp:Label>
            <asp:Label ID="lblPassword" Visible="false" runat="server" Text="<Username>"></asp:Label>
        </p>
        <p class="style5">
            Use the tools below to register your teams and competitors for the tournament. 
            Please note: <b>November 23rd</b> is the deadline for early 
            registration. <b>December 7</b> is the deadline for late team 
            registration and the last day to modify the names for previously registered teams.
        </p>
        <br />
        <table class="style3">
            <tr bgcolor="#FFFFCC" style="border: 1px solid #FFFFCC; background-color: #FFFFCC">
                <td class="style5">
                    <b>
                        <img alt="help" src="../images/help.png" width="24" /></b>
                </td>
                <td style="border: 5px solid #FFFFCC">
                    <b>Need help?</b> Please refer to the <a href="pdf/13-14/Support/KPMT%2013-14%20Registration%20Payment%20Guide.pdf">
                        Registration &amp; Payment Guide</a>.
                </td>
            </tr>
        </table>
        <asp:AccessDataSource ID="TeamsData" runat="server" DataFile="~/App_Data/KPMT.mdb"
            SelectCommand="SELECT [ID], [Team Number] AS Team_Number, [Grade] FROM [Teams Query] WHERE ([School ID] = ?) ORDER BY [GRADE]"
            DeleteCommand="DELETE FROM [Teams] WHERE [ID] = ?" InsertCommand="INSERT INTO [Teams] ([School], [Grade]) VALUES (?, ?)"
            OnUpdated="TeamsData_Updated">
            <SelectParameters>
                <asp:ControlParameter ControlID="lblSchoolID" Name="School" PropertyName="Text" Type="Int32" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter Name="School" ControlID="lblSchoolID" PropertyName="Text" Type="Int32" />
                <asp:Parameter Name="Grade" Type="Int32" />
            </InsertParameters>
        </asp:AccessDataSource>
        <asp:AccessDataSource ID="CompetitorsData" runat="server" DataFile="~/App_Data/KPMT.mdb"
            SelectCommand="SELECT [ID], [Student Name] AS Student_Name, [Grade] FROM [Competitors Without Teams] WHERE ([School ID] = ?) ORDER BY [Student Name] ASC"
            InsertCommand="INSERT INTO [Competitors] ([Student Name], [Grade], [School]) VALUES (?, ?, ?)"
            DeleteCommand="DELETE FROM [Competitors] WHERE ([ID] = ?)" UpdateCommand="UPDATE [Competitors] SET [Student Name]=?, [Grade]=? WHERE [ID]=?"
            OnUpdated="CompetitorsData_Updated">
            <SelectParameters>
                <asp:ControlParameter ControlID="lblSchoolID" Name="School" PropertyName="Text" Type="Int32" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="Student_Name" Type="String" />
                <asp:Parameter Name="Grade" Type="Int32" />
                <asp:ControlParameter ControlID="lblSchoolID" Name="School" PropertyName="Text" Type="Int32" />
            </InsertParameters>
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Student_Name" Type="String" />
                <asp:Parameter Name="Grade" Type="Int32" />
                <asp:Parameter Name="ID" Type="Int32" />
            </UpdateParameters>
        </asp:AccessDataSource>
        <asp:AccessDataSource ID="GradesData" runat="server" DataFile="~/App_Data/KPMT.mdb"
            SelectCommand="SELECT [Grade] FROM [Grades]"></asp:AccessDataSource>
        <asp:AccessDataSource ID="TeamMembersData" runat="server" DataFile="~/App_Data/KPMT.mdb"
            SelectCommand="SELECT [ID], [CompetitorID], [Student Name] AS Student_Name, [Grade] FROM [Competitors With Teams] WHERE (([SchoolID] = ?) AND ([TeamID] = ?)) ORDER BY [Student Name]"
            DeleteCommand="DELETE FROM [Team Members] WHERE ([ID]=?)" InsertCommand="INSERT INTO [Team Members] ([Competitor], [Team]) VALUES(?, ?)"
            OnUpdated="TeamMembersData_Updated">
            <SelectParameters>
                <asp:ControlParameter ControlID="lblSchoolID" Name="SchoolID" PropertyName="Text"
                    Type="Int32" />
                <asp:ControlParameter ControlID="TeamsGrid" Name="TeamID" PropertyName="SelectedValue"
                    Type="Int32" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="CompetitorsGrid" Name="Competitor" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="TeamsGrid" Name="Team" PropertyName="SelectedValue"
                    Type="Int32" />
            </InsertParameters>
        </asp:AccessDataSource>
        <table class="style6">
            <tr valign="top">
                <td>
                    <h4>
                        My Competitors</h4>
                    <asp:FormView ID="AddCompetitors" runat="server" DataKeyNames="ID" DataSourceID="CompetitorsData"
                        DefaultMode="Insert" Style="margin-bottom: 14px; margin-top: 7px;" Width="320px">
                        <EditItemTemplate>
                            ID:
                            <asp:Label ID="IDLabel2" runat="server" Text='<%# Eval("ID") %>' />
                            <br />
                            Student_Name:
                            <asp:TextBox ID="Student_NameTextBox" runat="server" Text='<%# Bind("Student_Name") %>' />
                            <br />
                            Grade:
                            <asp:TextBox ID="GradeTextBox0" runat="server" Text='<%# Bind("Grade") %>' />
                            <br />
                            <asp:LinkButton ID="UpdateButton0" runat="server" CausesValidation="True" CommandName="Update"
                                Text="Update" />
                            &nbsp;<asp:LinkButton ID="UpdateCancelButton0" runat="server" CausesValidation="False"
                                CommandName="Cancel" Text="Cancel" />
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <table class="style6">
                                <tr valign="top">
                                    <td class="style10" valign="middle">
                                        Name:
                                    </td>
                                    <td class="style9">
                                        <asp:TextBox ID="Student_NameTextBox0" runat="server" Text='<%# Bind("Student_Name") %>'
                                            Width="220px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style10">
                                        Grade:
                                    </td>
                                    <td class="style9">
                                        <asp:DropDownList ID="GradeDropDownList0" runat="server" DataSourceID="GradesData"
                                            DataTextField="Grade" DataValueField="Grade" SelectedValue='<%# Bind("Grade") %>'>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="InsertCompetitorButton" runat="server" CausesValidation="True" CommandName="Insert"
                                            Style="margin-top: 5px" Text="Add Competitor" />
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            ID:
                            <asp:Label ID="IDLabel3" runat="server" Text='<%# Eval("ID") %>' />
                            <br />
                            Student_Name:
                            <asp:Label ID="Student_NameLabel" runat="server" Text='<%# Bind("Student_Name") %>' />
                            <br />
                            Grade:
                            <asp:Label ID="GradeLabel0" runat="server" Text='<%# Bind("Grade") %>' />
                            <br />
                        </ItemTemplate>
                    </asp:FormView>
                    <b>Competitors Not Assigned to a Team:</b>
                    <asp:GridView ID="CompetitorsGrid" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="CompetitorsData"
                        ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="CompetitorsGrid_SelectedIndexChanged"
                        PageSize="12" Style="margin-top: 11px">
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <Columns>
                            <asp:CommandField SelectText="Add to Team &raquo;" ShowDeleteButton="True" ShowSelectButton="True" />
                            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                SortExpression="ID" Visible="False" />
                            <asp:BoundField DataField="Student_Name" HeaderText="Student_Name" SortExpression="Student_Name" />
                            <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade" />
                        </Columns>
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Use the form above to add competitors for your school.
                        </EmptyDataTemplate>
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                    <br />
                </td>
                <td>
                    <h4>
                        My Teams</h4>
                    <asp:FormView ID="AddTeams" runat="server" DataKeyNames="ID" DataSourceID="TeamsData"
                        DefaultMode="Insert" Style="margin-top: 7px">
                        <EditItemTemplate>
                            ID:
                            <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' />
                            <br />
                            Team_Number:
                            <asp:TextBox ID="Team_NumberTextBox" runat="server" Text='<%# Bind("Team_Number") %>' />
                            <br />
                            Grade:
                            <asp:TextBox ID="GradeTextBox" runat="server" Text='<%# Bind("Grade") %>' />
                            <br />
                            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                                Text="Update" />
                            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False"
                                CommandName="Cancel" Text="Cancel" />
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            Grade:&nbsp;
                            <asp:DropDownList ID="GradeDropDownList" runat="server" DataSourceID="GradesData"
                                DataTextField="Grade" DataValueField="Grade" SelectedValue='<%# Bind("Grade") %>'>
                            </asp:DropDownList>
                            <br />
                            <asp:Button ID="InsertTeamButton" runat="server" CausesValidation="True" CommandName="Insert"
                                Style="margin-top: 5px" Text="Add Team" />
                            <!--<br/>
                            <b>The normal registration deadline has passed! </b>
                            <br/>
                            <b>New team registrations will incur a late fee.</b>-->
                        </InsertItemTemplate>
                        <ItemTemplate>
                            ID:
                            <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' />
                            <br />
                            Team_Number:
                            <asp:Label ID="Team_NumberLabel" runat="server" Text='<%# Bind("Team_Number") %>' />
                            <br />
                            Grade:
                            <asp:Label ID="GradeLabel" runat="server" Text='<%# Bind("Grade") %>' />
                            <br />
                            <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete"
                                Text="Delete" />
                            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New"
                                Text="New" />
                        </ItemTemplate>
                    </asp:FormView>
                    <br />
                    <asp:GridView ID="TeamsGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        CellPadding="4" DataKeyNames="ID" DataSourceID="TeamsData" ForeColor="#333333"
                        GridLines="None" OnRowDeleting="TeamsGrid_RowDeleting" OnSelectedIndexChanged="TeamsGrid_SelectedIndexChanged">
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <Columns>
                            <asp:CommandField DeleteText="Delete Team" SelectText="Edit Members" ShowDeleteButton="True"
                                ShowSelectButton="True" />
                            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                SortExpression="ID" Visible="False" />
                            <asp:BoundField DataField="Team_Number" HeaderText="Team_Number" SortExpression="Team_Number"
                                Visible="False" />
                            <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade" />
                        </Columns>
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            You currently have no teams.
                        </EmptyDataTemplate>
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                    <br />
                    <b>Team Members: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblTeamFull" runat="server" ForeColor="Red" Text="*This team is full."
                        Visible="False"></asp:Label>
                    <br />
                    <asp:GridView ID="TeamMembersGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        CellPadding="4" DataKeyNames="ID" DataSourceID="TeamMembersData" ForeColor="#333333"
                        GridLines="None" Style="margin-top: 10px">
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <Columns>
                            <asp:CommandField DeleteText="&laquo; Remove From Team" ShowDeleteButton="True" />
                            <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" Visible="false" />
                            <asp:BoundField DataField="Student_Name" HeaderText="Student Name" SortExpression="Student_Name" />
                            <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade" />
                        </Columns>
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Add a team and click &quot;Edit Members&quot; to view or edit team members. Then
                            add members to the team from the table to the left.
                        </EmptyDataTemplate>
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
