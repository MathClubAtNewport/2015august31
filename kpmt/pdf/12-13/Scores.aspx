﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpmt/kpmt.master" AutoEventWireup="true" CodeFile="Scores.aspx.cs" Inherits="kpmt_pdf_12_13_Scores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 600px;
            border-width: medium;
            border-style: solid;
        }
        .style4
        {
            width: 104px;
            background-color: #EEDDBB;
        }
        .style7
        {
            width: 46px;
            background-color: #EEDDBB;
        }
        .style13
        {
            width: 62px;
            background-color: #EEDDBB;
        }
        .style15
        {
            width: 59px;
            background-color: #EEDDBB;
        }
        .style17
        {
            width: 171px;
            background-color: #EEDDBB;
        }
        .style48
        {
            width: 46px;
            background-color: #CCFFCC;
        }
        .style49
        {
            width: 62px;
            background-color: #CCFFCC;
        }
        .style50
        {
            width: 59px;
            background-color: #CCFFCC;
        }
        .style51
        {
            width: 104px;
            background-color: #CCFFCC;
        }
        .style52
        {
            width: 171px;
            font-weight: bold;
            background-color: #CCFFCC;
        }
        .style53
        {
            width: 46px;
            background-color: #FFFFFF;
        }
        .style54
        {
            width: 62px;
            background-color: #FFFFFF;
        }
        .style55
        {
            width: 59px;
            background-color: #FFFFFF;
        }
        .style56
        {
            width: 104px;
            background-color: #FFFFFF;
        }
        .style57
        {
            width: 171px;
            font-weight: bold;
            background-color: #FFFFFF;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
    <h2>
        5th Annual KPMT Scoresheets and Results</h2>
    <p>
        This document displays a list of correct individual and team places at the KPMT on December 15th, 2012.
        For those of you who deserved trophies but did not receive them, we will mail new ones to the coaches after we are back from Winter Break.
        We are very sorry for the inconvenience.
        </p>
    <br />
    <p>
        All documents are available in .pdf format. If you have any more questions about the results or the awards, please
        feel free to <a href="http://newportmathclub.org/kpmt/contact.aspx">contact us</a>.</p>
    <table class="style1">
        <tr>
            <td class="style17">
                <h4>Score Type (Ind. or Team)</h4>
            </td>
            <td class="style4">
                <h4>Grades</h4>
            </td>
            <td class="style15">
                <h4>Scores</h4>
            </td>
        </tr>
        <tr>
            <td class="style57" rowspan="4">
                Individual
            </td>
            <td class="style56">
                5th
            </td>
            <td class="style55">
                <a href="5ind.pdf">pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style56">
                6th
            </td>
            <td class="style55">
                <a href="6ind.pdf">pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style56">
                7th
            </td>
            <td class="style55">
                <a href="7ind.pdf">pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style56">
                8th
            </td>
            <td class="style55">
                <a href="8ind.pdf">pdf</a>
            </td>
        </tr>

        <tr>
            <td class="style52" rowspan="4">
                Team
            </td>
            <td class="style51">
                5th
            </td>
            <td class="style50">
                <a href="5team.pdf">pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style51">
                6th
            </td>
            <td class="style50">
                <a href="6team.pdf">pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style51">
                7th
            </td>
            <td class="style50">
                <a href="7team.pdf">pdf</a>
            </td>
        </tr>
        <tr>
            <td class="style51">
                8th
            </td>
            <td class="style50">
                <a href="8team.pdf">pdf</a>
            </td>
        </tr>
    </table>
</asp:Content>

