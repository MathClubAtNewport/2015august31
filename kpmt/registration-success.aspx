﻿<%@ Page Title="Knights of Pi Math Tournament - Registration Successful" Language="C#"
    MasterPageFile="~/kpmt/kpmt.master" %>

<script runat="server">

</script>
<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Registration Successful</h2>
    <table class="style1">
        <tr>
            <td>
                Your account was created successfully!<br />
                Please continue to login and complete your registration.<br />
                <br />
                <a href="coaches.aspx">
                    <img id="img1" alt="Continue »" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img1',/*url*/'buttons/buttonD.gif')"
                        onmouseout="FP_swapImg(0,0,/*id*/'img1',/*url*/'buttons/buttonB.gif')" onmouseover="FP_swapImg(0,0,/*id*/'img1',/*url*/'buttons/buttonC.gif')"
                        onmouseup="FP_swapImg(0,0,/*id*/'img1',/*url*/'buttons/buttonC.gif')" src="buttons/buttonB.gif"
                        style="border: 0" width="150" /><!--MSComment="ibutton" fp-style="fp-btn: Soft Capsule 3; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-transparent: 1" fp-title="Continue »" --></a>
            </td>
            <td>
                <asp:Image ID="imgLogo" runat="server" ImageUrl="~/kpmt/kpmt.png" Width="160px" ImageAlign="Top" />
            </td>
        </tr>
    </table>
    <br />
    <p>
    </p>
</asp:Content>
