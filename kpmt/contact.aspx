﻿<%@ Page Title="Knights of Pi Math Tournament - Contact" Language="C#" MasterPageFile="~/kpmt/kpmt.master" %>

<script runat="server">

</script>
<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Contact</h2>
    <h4>
        Mailing Address:</h4>
    <p>
        Newport Math Club<br />
        Newport High School<br />
        4333 Factoria Blvd. SE<br />
        Bellevue, WA 98006</p>
    <h4>
        Emails:<br />
    </h4>
    <p>
        Tournament Questions and Concerns: <a href="mailto:kpmt@newportmathclub.org">kpmt@newportmathclub.org</a>
        <br />
        Website Support: <a href="mailto:webmaster@newportmathclub.org">webmaster@newportmathclub.org</a>
    </p>
    <p>
        <a href="../pages/officers.aspx">Club Officer Contacts &raquo;</a>
    </p>
</asp:Content>
