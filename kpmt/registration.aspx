﻿<%@ Page Title="Knights of Pi Math Tournament - Registration" Language="C#" MasterPageFile="~/kpmt/kpmt.master"
    AutoEventWireup="true" CodeFile="registration.aspx.cs" Inherits="kpmt_registration" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style2
        {
            text-align: right;
        }
        .style3
        {
            width: 100%;
        }
        .style4
        {
            width: 120px;
        }
        .style5
        {
            width: 32px;
        }
        .style6
        {
            width: 621px;
        }
        .style7
        {
            width: 436px;
        }
        .style8
        {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <table style="width: 100%">
        <tr>
            <td class="style6">
                <h2>
                    Knights of Pi Math Tournament - Registration</h2>
            </td>
            <td class="style2">
                <a href="coaches.aspx">
                    <img id="loginButton" alt="Login" height="30" src="buttons/buttonA.gif" style="border: 0"
                        width="150" /></a>
            </td>
        </tr>
    </table>
    <p>
        If you are the coach of a math team, use the form below to register for the <a href="default.aspx">
            Knights of Pi Math Tournament</a>. The competition will be held at Newport High
        School on Saturday, December 20th, 2014. Please fill out the form completely and accurately
        to ensure successful registration.</p>
    <script type="text/javascript">        // this script should come after the register button only (in aspx page)
        if (!document.getElementById('<%=chkCompleteCorrect.ClientID%>').checked)
            document.getElementById('<%=btnRegister.ClientID%>').disabled = true;
        function enableButton() {
            var obj = document.getElementById('<%=btnRegister.ClientID%>');
            obj.disabled = !obj.disabled;
        }
    </script>
    <p style="color: red">
        <b>Online registration is now closed.</b>
    </p>
    <table class="style3">
        <tr bgcolor="#FFFFCC" style="border: 1px solid #FFFFCC; background-color: #FFFFCC">
            <td class="style5">
                <b>
                    <img alt="help" src="../images/help.png" width="24" /></b>
            </td>
            <td style="border: 5px solid #FFFFCC">
                <b>Need help?</b> Please refer to the <a href="pdf/13-14/Support/KPMT%2014-15%20Registration%20Payment%20Guide.pdf">Registration
                        &amp; Payment Guide</a>.
            </td>
        </tr>
    </table>
    <table class="style3">
        <tr>
            <td class="style7">
                <!--To open registration, Enabled="False"-->
                <asp:FormView ID="RegistrationForm" runat="server" DataKeyNames="ID" DataSourceID="SchoolsData"
                    DefaultMode="Insert" Width="405px" Enabled="False">
                    <InsertItemTemplate>
                        <h4 style="height: 24px">
                            School Account Creation</h4>
                        <table class="style3">
                            <tr>
                                <td class="style4">
                                    School Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="School_NameTextBox" runat="server" Height="19px" Text='<%# Bind("School_Name") %>'
                                        Width="95%" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    Address:
                                </td>
                                <td>
                                    <asp:TextBox ID="AddressTextBox" runat="server" Text='<%# Bind("Address") %>' Width="95%" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    City:
                                </td>
                                <td>
                                    <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' Width="155px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    State:
                                </td>
                                <td>
                                    <asp:TextBox ID="StateTextBox" runat="server" Text='<%# Bind("State") %>' Width="42px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    ZIP:&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox ID="ZIPTextBox" runat="server" Text='<%# Bind("ZIP") %>' Width="79px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    Coach Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="Coach_NameTextBox" runat="server" Text='<%# Bind("Coach_Name") %>'
                                        Width="200px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    Prefix:&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox ID="PrefixTextBox" runat="server" Text='<%# Bind("Prefix") %>' Width="79px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    Email:
                                </td>
                                <td>
                                    <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' Width="95%" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    Phone:
                                </td>
                                <td>
                                    <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' Width="155px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    Username:
                                </td>
                                <td>
                                    <asp:TextBox ID="UsernameTextBox" runat="server" Text='<%# Bind("Username") %>' Width="155px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    Password:
                                </td>
                                <td>
                                    <asp:TextBox ID="PasswordTextBox" runat="server" Text='<%# Bind("Password") %>' Width="155px"
                                        TextMode="Password" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    Confirm Password:
                                </td>
                                <td>
                                    <asp:TextBox ID="PasswordTextBox2" runat="server" Text='' TextMode="Password" Width="155px" />
                                </td>
                            </tr>
                        </table>
                    </InsertItemTemplate>
                </asp:FormView>
            </td>
            <td>
                <asp:Image ID="imgLogo" runat="server" ImageUrl="kpmt.png" Width="250px" ImageAlign="Top" />
            </td>
        </tr>
    </table>
    <p>
        Note: Your email address will only be used to send you contest updates and materials
        and to send you your password if you lose it. It will not be posted or distributed
        to others.
        <asp:AccessDataSource ID="SchoolsData" runat="server" DataFile="~/App_Data/KPMT.mdb"
            InsertCommand="INSERT INTO [Schools] ([School Name], [Address], [City], [State], [ZIP], [Coach Name], [Prefix], [Email], [Phone], [Username], [Password]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            DeleteCommand="DELETE FROM [Schools] WHERE [ID] = ?" SelectCommand="SELECT [ID], [School Name] AS School_Name, [Address], [City], [State], [ZIP], [Coach Name] AS Coach_Name, [Prefix], [Email], [Phone], [Username], [Password] FROM [Schools]"
            UpdateCommand="UPDATE [Schools] SET [School Name] = ?, [Address] = ?, [City] = ?, [State] = ?, [ZIP] = ?, [Coach Name] = ?, [Prefix]=?, [Email] = ?, [Phone] = ?, [Username] = ?, [Password] = ? WHERE [ID] = ?">
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="School_Name" Type="String" />
                <asp:Parameter Name="Address" Type="String" />
                <asp:Parameter Name="City" Type="String" />
                <asp:Parameter Name="State" Type="String" />
                <asp:Parameter Name="ZIP" Type="String" />
                <asp:Parameter Name="Coach_Name" Type="String" />
                <asp:Parameter Name="Prefix" Type="String" />
                <asp:Parameter Name="Email" Type="String" />
                <asp:Parameter Name="Phone" Type="String" />
                <asp:Parameter Name="Username" Type="String" />
                <asp:Parameter Name="Password" Type="String" />
                <asp:Parameter Name="ID" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="School_Name" Type="String" />
                <asp:Parameter Name="Address" Type="String" />
                <asp:Parameter Name="City" Type="String" />
                <asp:Parameter Name="State" Type="String" />
                <asp:Parameter Name="ZIP" Type="String" />
                <asp:Parameter Name="Coach_Name" Type="String" />
                <asp:Parameter Name="Prefix" Type="String" />
                <asp:Parameter Name="Email" Type="String" />
                <asp:Parameter Name="Phone" Type="String" />
                <asp:Parameter Name="Username" Type="String" />
                <asp:Parameter Name="Password" Type="String" />
            </InsertParameters>
        </asp:AccessDataSource>
    </p>
    <p>
        <asp:CheckBox ID="chkCompleteCorrect" runat="server" Enabled="False" /><!--To open registration, Enabled="True"-->&nbsp;
        I assert that the above information is complete and correct.
    </p>
    <p>
        <asp:Button ID="btnRegister" runat="server" OnClick="btnRegister_Click" Text="Register"
            Visible="True" Enabled="True" />
        <script type="text/javascript">            // this script should come after the register button only (in aspx page)
            if (!document.getElementById('<%=chkCompleteCorrect.ClientID%>').checked)
                document.getElementById('<%=btnRegister.ClientID%>').disabled = true;
            function enableButton() {
                var obj = document.getElementById('<%=btnRegister.ClientID%>');
                obj.disabled = !obj.disabled;

            }
        </script>
         <br />
        <span style="color: Red;"><b>Online Registration is now CLOSED! If you have any questions
            or wish to ask about making a late registration or changing an existing registration,
            please email <a class="style8" href="mailto:kpmt@newportmathclub.org">kpmt@newportmathclub.org</a>
            and we will be happy to help as best we can.</b></span>
        <br /> 
        <span class="style8"><b>
            <asp:Label ID="lblRequired" runat="server" ForeColor="Red" Text="*Error: All fields are required. Please fill out all fields before continuing."
                Visible="False"></asp:Label>
            <asp:Label ID="lblDuplicate" runat="server" ForeColor="Red" Text="*Error: There is already an account with the requested username. Please try again."
                Visible="False"></asp:Label>
            <asp:Label ID="lblBadEmail" runat="server" ForeColor="Red" Text="*Error: Please enter a valid email address."
                Visible="False"></asp:Label>
            <asp:Label ID="lblRules" runat="server" ForeColor="Red" Text="*Error: You must check the box to continue."
                Visible="False"></asp:Label>
            <asp:Label ID="lblPasswordMismatch" runat="server" ForeColor="Red" Text="*Error: The passwords you typed do not match."
                Visible="False"></asp:Label>
            <br />
            <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="*An error occured during registration. Please reenter your password and try again. If the error continues, please contact the webmaster."
                Visible="False"></asp:Label>
        </b></span>
    </p>
</asp:Content>
