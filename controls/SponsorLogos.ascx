﻿<%@ Control Language="C#" ClassName="SponsorLogos" %>
<h3 style="margin-bottom: 5px">
    <!-- 2009-2010 Sponsors</h3>
<a href="http://www.tecplot.com/" target="_new_sponsors">
    <img id="logo_tecplot" alt="Tecplot" src="~/images/sponsors/Tecplot.jpg" runat="server" /></a>
<a href="http://www.knowledgepoints.com/seattle" target="_new_sponsors">
    <img id="logo_knowledgepoints" alt="KnowledgePoints" src="~/images/sponsors/KnowledgePoints.jpg"
        runat="server" /></a>
<br />-->
    <asp:LoginView ID="LoginViewAdvertise" runat="server">
        <AnonymousTemplate>
            Place your company<br />
            logo here!
            <br />
            <a id="A1" href="~/pages/sponsors.aspx" runat="server">Learn more &raquo;</a>
        </AnonymousTemplate>
    </asp:LoginView>
