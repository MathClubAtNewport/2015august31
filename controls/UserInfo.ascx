﻿<%@ Control Language="C#" ClassName="UserInfo" %>
<asp:LoginView ID="LoginViewUserInfo" runat="server">
    <LoggedInTemplate>
        <div id="user">
            <p>
                <asp:LoginName runat="server" ID="LoginNameUser" />
                <span class="float_right">
                    <asp:LoginStatus ID="LoginStatusLogout" runat="server" />
                    &nbsp;</span></p>
        </div>
    </LoggedInTemplate>
</asp:LoginView>
