﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="YearDropDownList.ascx.cs"
    Inherits="controls_YearDropDownList" %>
<asp:AccessDataSource ID="YearsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
    SelectCommand="SELECT [Year] FROM [Years] ORDER BY [Year] DESC"></asp:AccessDataSource>
<asp:Label ID="ChooseYearLabel" runat="server">Choose a year:</asp:Label>
<asp:DropDownList ID="YearDropDownList" runat="server" AutoPostBack="True" DataSourceID="YearsData"
    DataTextField="Year" DataValueField="Year">
</asp:DropDownList>
