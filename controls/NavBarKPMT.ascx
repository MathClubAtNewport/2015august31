﻿<%@ Control Language="C#" ClassName="NavBarKPMT" %>
<div id="nav">
    <ul class="dropdown dropdown-horizontal">
        <li><a id="D" href="~/kpmt/default.aspx" runat="server">KPMT Home</a></li>
        <li><a id="I" href="~/kpmt/docs.aspx" runat="server">Contest Info</a> </li>
        <li><a id="J" href="~/kpmt/registration.aspx" runat="server">Register School</a></li>
        <li><a id="K" href="~/kpmt/coaches.aspx" runat="server">Coaches' Login</a></li>
        <li><a id="S" href="~/kpmt/archive.aspx" runat="server">Past Tests &amp; Results</a></li>
        <li><a id="T" href="~/kpmt/contact.aspx" runat="server">Contact</a> </li>
        <li><a id="R" href="~/pages/sponsors.aspx" runat="server">Sponsors</a></li>
        <li><a id="A" href="~/default.aspx" runat="server">Newport Math Club</a></li>
        <asp:LoginView ID="LoginViewKPMTAdminMenu" runat="server">
            <RoleGroups>
                <asp:RoleGroup Roles="Officers, Admins">
                    <ContentTemplate>
                        <li><span class="dir">View</span>
                            <ul>
                                <li><a id="A1" href="~/officers/kpmt.aspx" runat="server">View Registrations</a></li>
                            </ul>
                        </li>
                    </ContentTemplate>
                </asp:RoleGroup>
            </RoleGroups>
        </asp:LoginView>
    </ul>
    <div class="searchbox" style="padding-top: 2px;">
        <script type="text/javascript" src="http://www.google.com/cse/brand?form=cse-search-box&amp;lang=en">
        </script>
        <script language="javascript" type="text/javascript">
            function GoogleSiteSerach() {
                document.location.href = 'http://www.newportmathclub.org/pages/search.aspx?cx=partner-pub-8846177013257426:k2c5vu-fqre&cof=FORID:9&ie=ISO-8859-1&q=' + document.getElementById('q').value + '&sa=Search';
            }
        </script>
        <script language="javascript" type="text/javascript">
            function clickButton(e, buttonid) {
                var evt = e ? e : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.click();
                        return false;
                    }
                }
            }
        </script>
        <input type="text" name="q" size="30" id="q" onkeypress="return clickButton(event,'sa')" />
        <input type="button" onclick="JavaScript:GoogleSiteSerach();" name="sa" value="Search"
            id="sa" />
    </div>
</div>
