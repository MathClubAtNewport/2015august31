﻿<%@ Control Language="C#" ClassName="Footer" %>
<div id="footer">
    <span class="footer-left">Copyright &copy; 2008-2015 by Newport Math Club </span>
    <span class="footer-right"><a id="A1" href="~/pages/about.aspx" runat="server">About
        Us</a> | <a id="A2" href="~/pages/contests.aspx" runat="server">Our Contests</a>
        | <a id="A3" href="~/pages/officers.aspx" runat="server">Contact Us</a> </span>
</div>
