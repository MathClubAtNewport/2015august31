﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_YearDropDownList : System.Web.UI.UserControl
{

    public string LabelText
    {
        get
        {
            return ChooseYearLabel.Text;
        }
        set
        {
            ChooseYearLabel.Text = value;
        }
    }

    public string SelectedYear
    {
        get
        {
            return YearDropDownList.SelectedValue;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}