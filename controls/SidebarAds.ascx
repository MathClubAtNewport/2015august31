﻿<%@ Control Language="C#" ClassName="SidebarAds" %>
<h3 style="margin-bottom: 5px">
    Sponsored Links</h3>
<asp:LoginView ID="LoginViewSidebarAds" runat="server">
    <AnonymousTemplate>
        <script type="text/javascript">
            google_ad_client = "pub-8846177013257426";
            // large - Newport Math Club Right Wide Skyscraper
            google_ad_slot = "9208928404";
            google_ad_width = 160;
            google_ad_height = 600;
        </script>
        <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
    </AnonymousTemplate>
    <LoggedInTemplate>
        <script type="text/javascript">
            // small
            google_ad_client = "pub-8846177013257426";
            google_ad_slot = "0880078497";
            google_ad_width = 120;
            google_ad_height = 240;
        </script>
        <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
    </LoggedInTemplate>
</asp:LoginView>
