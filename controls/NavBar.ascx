﻿<%@ Control Language="C#" ClassName="NavBar" %>
<div id="nav">
    <ul class="dropdown dropdown-horizontal">
        <li><a id="A1" href="~/default.aspx" runat="server">Home</a></li>
        <li><span class="dir">About</span>
            <ul>
                <li><a id="A2" href="~/pages/about.aspx" runat="server">About the Club</a></li>
                <li><a id="A3" href="~/pages/achievements.aspx" runat="server">Achievements</a></li>
                <li><a id="A4" href="~/pages/officers.aspx" runat="server">Officers</a></li>
                <li><a id="A5" href="~/pages/sponsors.aspx" runat="server">Sponsors</a></li>
            </ul>
        </li>
        <li><span class="dir">Events</span>
            <ul>
                <li><a id="A6" href="~/pages/meetings.aspx" runat="server">Meetings</a></li>
                <li><a id="A7" href="~/pages/competitions.aspx" runat="server">Upcoming Competitions</a></li>
            </ul>
        </li>
        <li><span class="dir">Contests We Host</span>
            <ul>
                <li><a id="A8" href="~/pages/contests.aspx" runat="server">All Contests</a></li>
                <li><a id="A9" href="~/kpmt/default.aspx" runat="server">KPMT</a></li>
            </ul>
        </li>
        <li><span class="dir">Resources</span>
            <ul>
                <li><a id="A10" href="~/pages/articles.aspx" runat="server">Articles</a></li>
                <li><a id="A11" href="~/pages/practice.aspx" runat="server">Practice</a></li>
                <li><a id="A12" href="~/pages/links.aspx" runat="server">Links</a></li>
            </ul>
        </li>
        <li><span class="dir">Members</span>
            <ul>
                <asp:LoginView ID="MembersMenuLoginView" runat="server">
                    <RoleGroups>
                        <asp:RoleGroup Roles="Members">
                            <ContentTemplate>
                                <li><a id="A13" href="~/members/mymeetings.aspx" runat="server">My Meetings</a></li>
                                <li><a id="A14" href="~/members/mycompetitions.aspx" runat="server">My Competitions</a></li>
                                <li><a id="A15" href="~/members/mypoints.aspx" runat="server">My Pi Points</a></li>
                                <li><a id="A16" href="~/members/myawards.aspx" runat="server">My Awards</a></li>
                                <li><a id="A17" href="~/members/email.aspx" runat="server">Send E-mail</a></li>
                                <li><a id="A18" href="~/members/changepassword.aspx" runat="server">Change Password</a></li>
                            </ContentTemplate>
                        </asp:RoleGroup>
                    </RoleGroups>
                </asp:LoginView>
                <li>
                    <asp:LoginStatus ID="LoginStatusNavBar" runat="server" />
                </li>
            </ul>
        </li>
        <asp:LoginView ID="AdminsMenuLoginView" runat="server">
            <RoleGroups>
                <asp:RoleGroup Roles="Officers, Admins">
                    <ContentTemplate>
                        <li><span class="dir">Admin</span>
                            <ul>
                                <li><span class="dir">Meet</span>
                                    <ul>
                                        <li><a id="MeetA1" href="~/officers/members.aspx" runat="server">Members</a></li>
                                        <li><a id="MeetA2" href="~/officers/meetings.aspx" runat="server">Meetings</a></li>
                                        <li><a id="MeetA3" href="~/officers/attendance.aspx" runat="server">Attendance</a></li>
                                        <li><a id="MeetA4" href="~/officers/points.aspx" runat="server">Pi Points</a></li>
                                    </ul>
                                    <li><span class="dir">Compete</span>
                                        <ul>
                                            <li><a id="CompeteA1" href="~/officers/competitions.aspx" runat="server">Competitions</a></li>
                                            <li><a id="CompeteA2" href="~/officers/competitors.aspx" runat="server">Competitors</a></li>
                                            <li><a id="CompeteA3" href="~/officers/teams.aspx" runat="server">Teams</a></li>
                                            <li><a id="CompeteA4" href="~/officers/transportation.aspx" runat="server">Transportation</a></li>
                                            <li><a id="CompeteA5" href="~/officers/awards.aspx" runat="server">Awards</a></li>
                                        </ul>
                                    </li>
                                    <li><span class="dir">Track</span>
                                        <ul>
                                            <li><a id="TrackA1" href="~/officers/parents.aspx" runat="server">Parents</a></li>
                                            <li><a id="TrackA2" href="~/officers/sponsors.aspx" runat="server">Sponsors</a></li>
                                            <asp:LoginView ID="LoginViewAdmins_Users" runat="server">
                                                <RoleGroups>
                                                    <asp:RoleGroup Roles="Admins">
                                                        <ContentTemplate>
                                                            <li><a id="TrackA3" href="~/admin/users.aspx" runat="server">Users</a></li>
                                                            <li><a id="TrackA4" href="~/admin/classes_teachers.aspx" runat="server">Classes &amp;
                                                                Teachers</a></li>
                                                        </ContentTemplate>
                                                    </asp:RoleGroup>
                                                </RoleGroups>
                                            </asp:LoginView>
                                        </ul>
                                    </li>
                                    <li><span class="dir">Host</span>
                                        <ul>
                                            <li><a id="HostA1" href="~/officers/kpmt.aspx" runat="server">KPMT Registrations</a></li>
                                            <li><a id="HostA2" href="~/contests/amc/admin/register.aspx" runat="server">AMC Registrations</a></li>
                                        </ul>
                                    </li>
                                    <li><span class="dir">Report</span>
                                        <ul>
                                            <li><a id="ReportA1" href="~/officers/reports/reports.aspx" runat="server">All Reports</a></li>
                                            <li><a id="ReportA2" href="~/officers/reports/memberlist.aspx" runat="server">
                                                Member List</a></li>
                                            <li><a id="ReportA3" href="~/officers/reports/yearlypoints.aspx" runat="server">Pi Points
                                                (Yearly)</a></li>
                                            <li><a id="ReportA4" href="~/officers/reports/totalpoints.aspx" runat="server">Pi Points
                                                (Total)</a></li>
                                            <li><a id="ReportA5" href="~/officers/reports/individualstats.aspx" runat="server">Individual Stats</a></li>
                                        </ul>
                                    </li>
                            </ul>
                        </li>
                    </ContentTemplate>
                </asp:RoleGroup>
            </RoleGroups>
        </asp:LoginView>
    </ul>
    <div class="searchbox" style="padding-top: 2px;">
        <script type="text/javascript" src="http://www.google.com/cse/brand?form=cse-search-box&amp;lang=en">
        </script>
        <script language="javascript" type="text/javascript">
            function GoogleSiteSerach() {
                document.location.href = 'http://www.newportmathclub.org/pages/search.aspx?cx=partner-pub-8846177013257426:k2c5vu-fqre&cof=FORID:9&ie=ISO-8859-1&q=' + document.getElementById('q').value + '&sa=Search';
            }
        </script>
        <script language="javascript" type="text/javascript">
            function clickButton(e, buttonid) {
                var evt = e ? e : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.click();
                        return false;
                    }
                }
            }
        </script>
        <input type="text" name="q" size="30" id="q" onkeypress="return clickButton(event,'sa')" />
        <input type="button" onclick="JavaScript:GoogleSiteSerach();" name="sa" value="Search"
            id="sa" />
    </div>
</div>
