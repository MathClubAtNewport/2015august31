﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class contests_amc_admin_register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool hasPrivileges = (User.IsInRole("Officers") || User.IsInRole("Admins") || User.IsInRole("Teachers") || User.Identity.Name.Equals("Rose Brallier"));
        RegistrationsGrid.AutoGenerateEditButton = hasPrivileges;
        RegistrationsGrid.AutoGenerateDeleteButton = hasPrivileges;
        btnExport.Visible = hasPrivileges;
        lblEditAndDeleteTitle.Visible = hasPrivileges;
    }
    protected void InsertRegistrationDetailsView_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        ErrorsLabel.Text = "";
        if ((bool)e.Values["AMC10A"] && (bool)e.Values["AMC12A"])
        {
            ErrorsLabel.Text += "ERROR: Students can't take both A tests. " + Environment.NewLine;
            e.Cancel = true;
        }
        if ((bool)e.Values["AMC10B"] && (bool)e.Values["AMC12B"])
        {
            ErrorsLabel.Text += "ERROR: Students can't take both B tests. " + Environment.NewLine;
            e.Cancel = true;
        }
        if (Int32.Parse(e.Values["Grade"].ToString()) > 10 && ((bool)e.Values["AMC10A"] || (bool)e.Values["AMC10B"]))
        {
            ErrorsLabel.Text += "ERROR: Upperclassmen cannot take AMC10 tests. " + Environment.NewLine;
            e.Cancel = true;
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        // Prepare the headers for file attachment
        string attachment = "attachment; filename=Registrations.csv";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "text/csv";

        // Get registration data
        System.Collections.IEnumerable data = RegistrationsDataSource.Select(DataSourceSelectArguments.Empty);

        // Write column headers
        IEnumerator enumerator = data.GetEnumerator();
        enumerator.MoveNext();
        DataRowView firstRow = (DataRowView)enumerator.Current;
        foreach (DataColumn col in firstRow.Row.Table.Columns)
             Response.Write(col.ColumnName.ToUpper() + ",");
        Response.Write(Environment.NewLine);

        // Write column data
        foreach (DataRowView row in data)
        {
            foreach (object o in row.Row.ItemArray)
                Response.Write(o.ToString() + ",");
            Response.Write(Environment.NewLine);
        }
        Response.End();
    }
}