﻿<%@ Page Title="AMC Registrations &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="contests_amc_admin_register" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        AMC Registrations</h2>
    <asp:AccessDataSource ID="RegistrationsDataSource" runat="server" DataFile="~/App_Data/AMC2011.mdb"
        DeleteCommand="DELETE FROM [Registrations] WHERE [RegistrationID] = @RegistrationID"
        InsertCommand="INSERT INTO [Registrations] ([Last], [First], [Grade], [Class], [Teacher], [Period], [AMC10A], [AMC12A], [AMC10B], [AMC12B], [School]) VALUES (@Last, @First, @Grade, @Class, @Teacher, @Period, @AMC10A, @AMC12A, @AMC10B, @AMC12B, @School)"
        SelectCommand="SELECT * FROM [Registrations]" UpdateCommand="UPDATE [Registrations] SET [Last] = @Last, [First] = @First, [Grade] = @Grade, [Class] = @Class, [Teacher] = @Teacher, [Period] = @Period, [AMC10A] = @AMC10A, [AMC12A] = @AMC12A, [AMC10B] = @AMC10B, [AMC12B] = @AMC12B, [School] = @School WHERE [RegistrationID] = @RegistrationID">
        <DeleteParameters>
            <asp:Parameter Name="RegistrationID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Last" Type="String" />
            <asp:Parameter Name="First" Type="String" />
            <asp:Parameter Name="Grade" Type="Int32" />
            <asp:Parameter Name="Class" Type="String" />
            <asp:Parameter Name="Teacher" Type="String" />
            <asp:Parameter Name="Period" Type="Int32" />
            <asp:Parameter Name="AMC10A" Type="Boolean" />
            <asp:Parameter Name="AMC12A" Type="Boolean" />
            <asp:Parameter Name="AMC10B" Type="Boolean" />
            <asp:Parameter Name="AMC12B" Type="Boolean" />
            <asp:Parameter Name="School" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Last" Type="String" />
            <asp:Parameter Name="First" Type="String" />
            <asp:Parameter Name="Grade" Type="Int32" />
            <asp:Parameter Name="Class" Type="String" />
            <asp:Parameter Name="Teacher" Type="String" />
            <asp:Parameter Name="Period" Type="Int32" />
            <asp:Parameter Name="AMC10A" Type="Boolean" />
            <asp:Parameter Name="AMC12A" Type="Boolean" />
            <asp:Parameter Name="AMC10B" Type="Boolean" />
            <asp:Parameter Name="AMC12B" Type="Boolean" />
            <asp:Parameter Name="School" Type="String" />
            <asp:Parameter Name="RegistrationID" Type="Int32" />
        </UpdateParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="ClassesData" runat="server" DataFile="~/App_Data/AMC2011.mdb"
        SelectCommand="SELECT * FROM [Classes]"></asp:AccessDataSource>
    <asp:AccessDataSource ID="TeachersData" runat="server" DataFile="~/App_Data/AMC2011.mdb"
        SelectCommand="SELECT * FROM [Teachers] ORDER BY [TeacherName]"></asp:AccessDataSource>
    <asp:AccessDataSource ID="PeriodsData" runat="server" DataFile="~/App_Data/AMC2011.mdb"
        SelectCommand="SELECT * FROM [Periods] ORDER BY [Period]"></asp:AccessDataSource>
    <asp:AccessDataSource ID="GradesData" runat="server" DataFile="~/App_Data/AMC2011.mdb"
        SelectCommand="SELECT * FROM [Grades] ORDER BY [Grade]"></asp:AccessDataSource>
    <p>
        Use the following forms to manage AMC registrations.
        <br />
        <span style="color: Gray">Note: The database is currently configured for the 2011 AMC
            contests. This page will need to be updated for future years.</span>
    </p>
    <h4>
        Add a New Registation</h4>
    <asp:Label ID="ErrorsLabel" runat="server" ForeColor="Red" Text=""></asp:Label>
    <p>
        <asp:DetailsView ID="InsertRegistrationDetailsView" runat="server" AutoGenerateRows="False"
            DataKeyNames="RegistrationID" DataSourceID="RegistrationsDataSource" DefaultMode="Insert"
            Height="50px" Width="180px" CellPadding="4" ForeColor="#333333" GridLines="None"
            OnItemInserting="InsertRegistrationDetailsView_ItemInserting">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
            <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="RegistrationID" HeaderText="RegistrationID" InsertVisible="False"
                    ReadOnly="True" SortExpression="RegistrationID" />
                <asp:BoundField DataField="First" HeaderText="First" SortExpression="First" />
                <asp:BoundField DataField="Last" HeaderText="Last" SortExpression="Last" />
                <asp:TemplateField HeaderText="Grade" SortExpression="First">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Grade") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:DropDownList ID="DropDownList4" runat="server" SelectedValue='<%# Bind("Grade") %>'
                            DataSourceID="GradesData" DataValueField="Grade">
                        </asp:DropDownList>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("Grade") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Teacher" SortExpression="Teacher">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Teacher") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:DropDownList ID="DropDownList2" runat="server" SelectedValue='<%# Bind("Teacher") %>'
                            DataSourceID="TeachersData" DataValueField="TeacherName">
                        </asp:DropDownList>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Teacher") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Class" SortExpression="Class">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Class") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" SelectedValue='<%# Bind("Class") %>'
                            DataSourceID="ClassesData" DataValueField="ClassName">
                        </asp:DropDownList>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Class") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Period" SortExpression="Period">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Period") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:DropDownList ID="DropDownList3" runat="server" SelectedValue='<%# Bind("Period") %>'
                            DataSourceID="PeriodsData" DataValueField="Period">
                        </asp:DropDownList>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("Period") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CheckBoxField DataField="AMC10A" HeaderText="AMC10A" SortExpression="AMC10A" />
                <asp:CheckBoxField DataField="AMC12A" HeaderText="AMC12A" SortExpression="AMC12A" />
                <asp:CheckBoxField DataField="AMC10B" HeaderText="AMC10B" SortExpression="AMC10B"
                    Visible="true" />
                <asp:CheckBoxField DataField="AMC12B" HeaderText="AMC12B" SortExpression="AMC12B"
                    Visible="true" />
                <asp:BoundField DataField="School" HeaderText="School (if not NHS)" SortExpression="School" />
                <asp:CommandField ShowInsertButton="True" InsertText="Add Registration" ShowCancelButton="False" />
            </Fields>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        </asp:DetailsView>
    </p>
    <h4>
        View<asp:Label ID="lblEditAndDeleteTitle" runat="server" 
            Text=", Edit, and Delete"></asp:Label>
&nbsp;Existing Registrations</h4>
<p>
        <asp:Button ID="btnExport" runat="server" onclick="btnExport_Click" 
            Text="Export All Data to Excel" />
</p>
    <p>
        <asp:GridView ID="RegistrationsGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            DataKeyNames="RegistrationID" DataSourceID="RegistrationsDataSource" 
            AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None" 
            PageSize="50">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="RegistrationID" HeaderText="ID" InsertVisible="False"
                    ReadOnly="True" Visible="False" SortExpression="RegistrationID" />
                <asp:BoundField DataField="Last" HeaderText="Last" SortExpression="Last" />
                <asp:BoundField DataField="First" HeaderText="First" SortExpression="First" />
                <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade" />
                <asp:BoundField DataField="Class" HeaderText="Class" SortExpression="Class" />
                <asp:BoundField DataField="Teacher" HeaderText="Teacher" SortExpression="Teacher" />
                <asp:BoundField DataField="Period" HeaderText="Per" SortExpression="Period" />
                <asp:CheckBoxField DataField="AMC10A" HeaderText="10A" SortExpression="AMC10A" />
                <asp:CheckBoxField DataField="AMC12A" HeaderText="12A" SortExpression="AMC12A" />
                <asp:CheckBoxField DataField="AMC10B" HeaderText="10B" SortExpression="AMC10B" />
                <asp:CheckBoxField DataField="AMC12B" HeaderText="12B" SortExpression="AMC12B" />
                <asp:BoundField DataField="School" HeaderText="School" SortExpression="School" />
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
    </p>
    <p>
    </p>
</asp:Content>
