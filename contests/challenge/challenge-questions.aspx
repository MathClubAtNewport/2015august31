﻿<%@ Page Language="C#" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml">

<!-- #BeginTemplate "../../master.dwt" -->

<head>
<script type="text/javascript">
window.google_analytics_uacct = "UA-4106925-2";
</script>

<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<!-- #BeginEditable "doctitle" -->
<title>Newport Math Challenge - Questions &amp; Solutions</title>
<script type="text/javascript">
<!--

function showLink(url)
{ 
    document.getElementById('frame').src=url;
} 


function FP_swapImg() {//v1.0
 var doc=document,args=arguments,elm,n; doc.$imgSwaps=new Array(); for(n=2; n<args.length;
 n+=2) { elm=FP_getObjectByID(args[n]); if(elm) { doc.$imgSwaps[doc.$imgSwaps.length]=elm;
 elm.$src=elm.src; elm.src=args[n+1]; } }
}
function FP_getObjectByID(id,o) {//v1.0
 var c,el,els,f,m,n; if(!o)o=document; if(o.getElementById) el=o.getElementById(id);
 else if(o.layers) c=o.layers; else if(o.all) el=o.all[id]; if(el) return el;
 if(o.id==id || o.name==id) return o; if(o.childNodes) c=o.childNodes; if(c)
 for(n=0; n<c.length; n++) { el=FP_getObjectByID(id,c[n]); if(el) return el; }
 f=o.forms; if(f) for(n=0; n<f.length; n++) { els=f[n].elements;
 for(m=0; m<els.length; m++){ el=FP_getObjectByID(id,els[n]); if(el) return el; } }
 return null;
}

function FP_preloadImgs() {//v1.0
 var d=document,a=arguments; if(!d.FP_imgs) d.FP_imgs=new Array();
 for(var i=0; i<a.length; i++) { d.FP_imgs[i]=new Image; d.FP_imgs[i].src=a[i]; }
}
// -->
</script>
        <style type="text/css">












































































            .style2
            {
                width: 100%;
            }
            .style5
            {
                color: #A0522D;
                font-size: large;
                font-weight: bold;
            }
            </style>
<!-- #EndEditable -->
<link href="../../styles/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form runat="server" name="theForm">

<!-- Begin Container -->
<div id="container">
	<!-- Begin Masthead -->
	<div id="masthead">
		<a href="../../default.aspx">
		<img alt="Newport Math Club" src="../../images/MathClubBanner.png" height="160" width="1000" /></a>
	</div>
	<!-- End Masthead -->
	<!-- Begin Navigation -->
	<div class="dropdown">
		<ul>
			<li><a href="../../default.aspx">Home</a></li>
			<li><a href="../../pages/about.aspx">About</a>
				<ul>
					<li><a href="../../pages/about.aspx">About the Club</a></li>
					<li><a href="../../pages/achievements.aspx">Achievements</a></li>
					<li><a href="../../pages/officers.aspx">Officers</a></li>
					<li><a href="../../pages/sponsors.aspx">Sponsors</a></li>
				</ul>
			</li>
			<li><p>Events</p>
				<ul>
					<li><a href="../../pages/meetings.aspx">Meetings</a></li>
					<li><a href="../../pages/competitions.aspx">Upcoming Competitions</a></li>
				</ul>
			</li>
			<li><a href="../../pages/contests.aspx">Contests We Host</a>
				<ul>
					<li><a href="../../pages/contests.aspx">List of Contests</a></li>
					<li><a href="../../kpmt/default.aspx">KPMT</a></li>
				</ul>
			</li>
			<li><p>Resources</p>
				<ul>
					<li><a href="../../pages/articles.aspx">Articles</a></li>
					<li><a href="../../pages/practice.aspx">Practice</a></li>
					<li><a href="../../pages/links.aspx">Links</a></li>
				</ul>
			</li>
			<li><p>Members</p>
				<ul>
					<asp:loginview id="MembersMenuLoginView" runat="server">
						<RoleGroups>
							<asp:RoleGroup Roles="Members">
								<ContentTemplate>
									<li><a href="../../members/mymeetings.aspx">My Meetings</a></li>
									<li>
									<a href="../../members/mycompetitions.aspx">My Competitions</a></li>
									<li><a href="../../members/mypoints.aspx">My Pi Points</a></li>
									<li><a href="../../members/myawards.aspx">My Awards</a></li>
									<li><a href="../../members/email.aspx">Send E-mail</a></li>
									<li>
									<a href="../../members/changepassword.aspx">Change Password</a></li>
								</ContentTemplate>
							</asp:RoleGroup>
						</RoleGroups>
					</asp:loginview>
					<li><asp:LoginStatus runat="server"></asp:LoginStatus></li>					
				</ul>
			</li>
			<asp:loginview id="AdminsMenuLoginView" runat="server">
				<RoleGroups>
					<asp:rolegroup Roles="Officers, Admins">
						<ContentTemplate>
							<li><p>Administration</p>
								<ul>
									<li><a href="../../officers/meetings.aspx">Meetings</a></li>
									<li>
									<a href="../../officers/attendance.aspx">Attendance</a></li>
									<li>
									<a href="../../officers/competitions.aspx">Contests</a></li>
									<li>
									<a href="../../officers/competitors.aspx">Competitors</a></li>
									<li><a href="../../officers/teams.aspx">Teams</a></li>
									<li>
									<a href="../../officers/transportation.aspx">Transportation</a></li>
									<li><a href="../../officers/awards.aspx">Awards</a></li>
									<li><a href="../../officers/points.aspx">Pi Points</a></li>
									<li><a href="../../officers/members.aspx">Members</a></li>
									<li><a href="../../officers/parents.aspx">Parents</a></li>
									<li><a href="../../officers/sponsors.aspx">Sponsors</a></li>
									<asp:loginview id="LoginViewAdmins_Users" runat="server">
										<RoleGroups>
											<asp:rolegroup Roles="Admins">
												<ContentTemplate>
													<li>
													<a href="../../admin/users.aspx">Users</a></li>
													<li>
													<a href="../../admin/classes_teachers.aspx">Classes &amp; Teachers</a></li>
												</ContentTemplate>
											</asp:rolegroup>						
										</RoleGroups>
									</asp:loginview>
									<li>
									<a href="../../officers/reports/reports.aspx">Reports</a>
										<ul>
											<li>
											<a href="../../officers/reports/report-points.aspx">Pi Points</a></li>
										</ul>
									</li>
								</ul>
							</li>	
						</ContentTemplate>
					</asp:rolegroup>
				</RoleGroups>
			</asp:loginview>
		</ul>
		
		<div class="searchbox">
			<script type="text/javascript" src="http://www.google.com/cse/brand?form=cse-search-box&amp;lang=en">
			</script>
			<script language="javascript" type="text/javascript">
		        function GoogleSiteSerach() {
		            document.location.href = 'http://www.newportmathclub.org/pages/search.aspx?cx=partner-pub-8846177013257426:k2c5vu-fqre&cof=FORID:9&ie=ISO-8859-1&q=' + document.getElementById('q').value + '&sa=Search';
		        }
			</script>
			<script language="javascript" type="text/javascript">
			function clickButton(e, buttonid){ 
	      		var evt = e ? e : window.event;
	     		var bt = document.getElementById(buttonid);
			    if (bt){ 
			        if (evt.keyCode == 13){ 
			              bt.click(); 
			              return false; 
			        } 
		    	} 
			}
			</script>	
			<input type="text" name="q" size="30" id="q" onkeypress="return clickButton(event,'sa')" />
			<input type="button" onclick="JavaScript:GoogleSiteSerach();" name="sa" value="Search" id="sa" />
		</div>
		
	</div>
	<!-- End Navigation -->
	<!-- Begin Page Content -->
	<div id="page_content">
		<!-- Begin Content -->
		<div id="content">
			<!-- #BeginEditable "content" -->
			<p>Newport Math Challenge!</p>
			<h2>Newport Math Challenge - Questions &amp; Solutions</h2>
				<table class="style2">
                    <tr align="center">
                        <td valign="bottom">
                            <p class="style5">
                                Week 1</p>
                        </td>
                        <td valign="bottom">
                            <p class="style5">
                                Week 2</p>
                        </td>
                        <td valign="bottom">
                            <p class="style5">
                                Week 3</p>
                        </td>
                        <td valign="bottom">
                            <p class="style5">
                                Week 4</p>
                        </td>
                        <td valign="bottom">
                            <p class="style5">
                                Week 5</p>
                        </td>
                        <td>
                            <a href="challenge.aspx" target="_new2">
							<img id="img35" alt="Login" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button4F.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button4D.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button4E.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button4E.jpg')" src="buttons/button4D.jpg" style="border: 0" width="150" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 9; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0" fp-title="Login" --></a><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 9; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0" fp-title="Login" --></td>
                    </tr>
                    <tr align="center">
                        <td>
                            <a href="javascript:showLink(&quot;challenge-week1.pdf&quot;);">
							<img id="img25" alt="Questions" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img25',/*url*/'buttons/button1A.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img25',/*url*/'buttons/button18.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img25',/*url*/'buttons/button19.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img25',/*url*/'buttons/button19.jpg')" src="buttons/button18.jpg" style="border: 0" width="100" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 10; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-proportional: 0" fp-title="Questions" --></a></td>
                        <td>
                            <a href="javascript:showLink(&quot;challenge-week2.pdf&quot;);">
							<img id="img26" alt="Questions" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img26',/*url*/'buttons/button26.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img26',/*url*/'buttons/button24.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img26',/*url*/'buttons/button25.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img26',/*url*/'buttons/button25.jpg')" src="buttons/button24.jpg" style="border: 0" width="100" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 10; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-proportional: 0" fp-title="Questions" --></a></td>
                        <td>
                            <a href="javascript:showLink(&quot;challenge-week3.pdf&quot;);">
							<img id="img27" alt="Questions" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img27',/*url*/'buttons/button2E.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img27',/*url*/'buttons/button2C.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img27',/*url*/'buttons/button2D.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img27',/*url*/'buttons/button2D.jpg')" src="buttons/button2C.jpg" style="border: 0" width="100" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 10; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-proportional: 0" fp-title="Questions" --></a></td>
                        <td>
                            <a href="javascript:showLink(&quot;challenge-week4.pdf&quot;);">
							<img id="img28" alt="Questions" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img28',/*url*/'buttons/button31.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img28',/*url*/'buttons/button2F.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img28',/*url*/'buttons/button30.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img28',/*url*/'buttons/button30.jpg')" src="buttons/button2F.jpg" style="border: 0" width="100" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 10; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-proportional: 0" fp-title="Questions" --></a></td>
                        <td>
                            <a href="javascript:showLink(&quot;challenge-week5.pdf&quot;);">
							<img id="img29" alt="Questions" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img29',/*url*/'buttons/button34.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img29',/*url*/'buttons/button32.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img29',/*url*/'buttons/button33.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img29',/*url*/'buttons/button33.jpg')" src="buttons/button32.jpg" style="border: 0" width="100" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 10; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-proportional: 0" fp-title="Questions" --></a></td>
                        <td>
                            <a href="challenge-register.aspx" target="_new2">
							<img id="img36" alt="Create Account" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button52.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button50.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button51.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button51.jpg')" src="buttons/button50.jpg" style="border: 0" width="150" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 10; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0" fp-title="Create Account" --></a></td>
                    </tr>
                    <tr align="center">
                        <td>
                            <a href="javascript:showLink(&quot;challenge-week1-solutions.pdf&quot;);">
							<img id="img30" alt="Solutions" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img30',/*url*/'buttons/button39.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img30',/*url*/'buttons/button37.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img30',/*url*/'buttons/button38.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img30',/*url*/'buttons/button38.jpg')" src="buttons/button37.jpg" style="border: 0" width="100" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 3; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-proportional: 0" fp-title="Solutions" --></a></td>
                        <td>
                            <a href="javascript:showLink(&quot;challenge-week2-solutions.pdf&quot;);">
							<img id="img31" alt="Solutions" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img31',/*url*/'buttons/button3F.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img31',/*url*/'buttons/button3D.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img31',/*url*/'buttons/button3E.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img31',/*url*/'buttons/button3E.jpg')" src="buttons/button3D.jpg" style="border: 0" width="100" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 3; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-proportional: 0" fp-title="Solutions" --></a></td>
                        <td>
                            <a href="javascript:showLink(&quot;challenge-week3-solutions.pdf&quot;);">
							<img id="img32" alt="Solutions" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img32',/*url*/'buttons/button42.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img32',/*url*/'buttons/button40.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img32',/*url*/'buttons/button41.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img32',/*url*/'buttons/button41.jpg')" src="buttons/button40.jpg" style="border: 0" width="100" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 3; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-proportional: 0" fp-title="Solutions" --></a></td>
                        <td>
                            <a href="javascript:showLink(&quot;challenge-week4-solutions.pdf&quot;);">
							<img id="img33" alt="Solutions" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img33',/*url*/'buttons/button46.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img33',/*url*/'buttons/button43.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img33',/*url*/'buttons/button44.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img33',/*url*/'buttons/button44.jpg')" src="buttons/button43.jpg" style="border: 0" width="100" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 3; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-proportional: 0" fp-title="Solutions" --></a></td>
                        <td>
                            <a href="javascript:showLink(&quot;notavailable.aspx&quot;);">
							<img id="img34" alt="Solutions" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img34',/*url*/'buttons/button4A.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img34',/*url*/'buttons/button47.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img34',/*url*/'buttons/button48.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img34',/*url*/'buttons/button48.jpg')" src="buttons/button47.jpg" style="border: 0" width="100" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 3; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-proportional: 0" fp-title="Solutions" --></a></td>
                        <td>
                            <a href="challenge-rules.aspx" target="_new2">
							<img id="img37" alt="Contest Rules" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button55.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button53.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button54.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button54.jpg')" src="buttons/button53.jpg" style="border: 0" width="150" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 3; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0" fp-title="Contest Rules" --></a></td>
                    </tr>
            </table>
				<p>
				<iframe id="frame" width="100%" height="500px" src="challenge-flyer.pdf">
				Your browser does not support viewing Newport Math Challenge question. Try a better browser or contact an administrator for details.
				</iframe></p>
			<!-- #EndEditable -->
			
			<br/>
			<script type="text/javascript"><!--
				google_ad_client = "pub-8846177013257426";
				/* Newport Math Club Bottom Leaderboard */
				google_ad_slot = "0637970847";
				google_ad_width = 728;
				google_ad_height = 90;
			//-->
			</script>
			<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script>
		</div>
		<!-- End Content -->
		<!-- Begin Sidebar -->
		<div id="sidebar">
			<asp:LoginView id="LoginView" runat="server">
				<AnonymousTemplate>
					<!--<asp:LoginStatus id="LoginStatus1" runat="server" />-->
				</AnonymousTemplate>
				<LoggedInTemplate>
					<div id="user">
					<p><asp:LoginName runat="server" id="LoginName1"></asp:LoginName>
					<span class="float_right"><asp:loginstatus id="LoginStatus1" runat="server" />&nbsp;</span></p>
					</div>
				</LoggedInTemplate>
			</asp:LoginView>
			<h3 style="margin-bottom:5px">2009-10 Sponsors</h3>
				<a href="http://www.tecplot.com/" target="_new_sponsors">
				<img alt="Tecplot" src="../../images/sponsors/Tecplot.jpg" /></a>
				<a href="http://www.knowledgepoints.com/seattle" target="_new_sponsors">
				<img alt="KnowledgePoints" src="../../images/sponsors/KnowledgePoints.jpg" /></a>
				<br/>
			<asp:LoginView ID="LoginViewAds" runat="server">
				<AnonymousTemplate>
					Place your company<br/>logo here! 
					<a href="../../pages/sponsors.aspx">Learn more &raquo;</a>
					<h3 style="margin-bottom:5px">Sponsored Links</h3>
					<script type="text/javascript">
					google_ad_client = "pub-8846177013257426";
					// large - Newport Math Club Right Wide Skyscraper
					google_ad_slot = "9208928404";
					google_ad_width = 160;
					google_ad_height = 600;
					</script>
					<script type="text/javascript"
					src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
					</script>
				</AnonymousTemplate>
				<LoggedInTemplate>
					<h3>Sponsored Links</h3>
					<script type="text/javascript">
						// small
						google_ad_client = "pub-8846177013257426";
						google_ad_slot = "0880078497";
						google_ad_width = 120;
						google_ad_height = 240;
					</script>
					<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
					</script>
				</LoggedInTemplate>
			</asp:LoginView>
			</div>
			<!-- End Sidebar -->
		</div>
		<!-- End Page Content -->
	</div>
	<!-- End Container -->

	<!-- Begin Footer -->
	<div id="footer">
		<span class="footer-left">
			Copyright &copy; 2008-2009 by Newport Math Club
		</span>
		<span class="footer-right">
			<a href="../../pages/about.aspx">About Us</a> |
			<a href="../../pages/contests.aspx">Our Contests</a> |
			<a href="../../pages/officers.aspx">Contact Us</a>
		</span>
	</div>
	<!-- End Footer -->
</form>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-4106925-2");
pageTracker._trackPageview();
} catch(err) {}</script>

</body>

<!-- #EndTemplate -->

</html>
