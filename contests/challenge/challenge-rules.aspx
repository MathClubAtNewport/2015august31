﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml">

<!-- #BeginTemplate "../../master.dwt" -->

<head>
<script type="text/javascript">
window.google_analytics_uacct = "UA-4106925-2";
</script>

<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<!-- #BeginEditable "doctitle" -->
<title>Newport Math Challenge - Official Rules</title>
<script type="text/javascript">
<!--
function FP_swapImg() {//v1.0
 var doc=document,args=arguments,elm,n; doc.$imgSwaps=new Array(); for(n=2; n<args.length;
 n+=2) { elm=FP_getObjectByID(args[n]); if(elm) { doc.$imgSwaps[doc.$imgSwaps.length]=elm;
 elm.$src=elm.src; elm.src=args[n+1]; } }
}
function FP_getObjectByID(id,o) {//v1.0
 var c,el,els,f,m,n; if(!o)o=document; if(o.getElementById) el=o.getElementById(id);
 else if(o.layers) c=o.layers; else if(o.all) el=o.all[id]; if(el) return el;
 if(o.id==id || o.name==id) return o; if(o.childNodes) c=o.childNodes; if(c)
 for(n=0; n<c.length; n++) { el=FP_getObjectByID(id,c[n]); if(el) return el; }
 f=o.forms; if(f) for(n=0; n<f.length; n++) { els=f[n].elements;
 for(m=0; m<els.length; m++){ el=FP_getObjectByID(id,els[n]); if(el) return el; } }
 return null;
}

function FP_preloadImgs() {//v1.0
 var d=document,a=arguments; if(!d.FP_imgs) d.FP_imgs=new Array();
 for(var i=0; i<a.length; i++) { d.FP_imgs[i]=new Image; d.FP_imgs[i].src=a[i]; }
}
// -->
</script>
<style type="text/css">













































































.style1 {
	text-align: right;
}
    .style2
    {
        text-align: right;
        width: 228px;
    }
</style>
<!-- #EndEditable -->
<link href="../../styles/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form runat="server" name="theForm">

<!-- Begin Container -->
<div id="container">
	<!-- Begin Masthead -->
	<div id="masthead">
		<a href="../../default.aspx">
		<img alt="Newport Math Club" src="../../images/MathClubBanner.png" height="160" width="1000" /></a>
	</div>
	<!-- End Masthead -->
	<!-- Begin Navigation -->
	<div class="dropdown">
		<ul>
			<li><a href="../../default.aspx">Home</a></li>
			<li><a href="../../pages/about.aspx">About</a>
				<ul>
					<li><a href="../../pages/about.aspx">About the Club</a></li>
					<li><a href="../../pages/achievements.aspx">Achievements</a></li>
					<li><a href="../../pages/officers.aspx">Officers</a></li>
					<li><a href="../../pages/sponsors.aspx">Sponsors</a></li>
				</ul>
			</li>
			<li><p>Events</p>
				<ul>
					<li><a href="../../pages/meetings.aspx">Meetings</a></li>
					<li><a href="../../pages/competitions.aspx">Upcoming Competitions</a></li>
				</ul>
			</li>
			<li><a href="../../pages/contests.aspx">Contests We Host</a>
				<ul>
					<li><a href="../../pages/contests.aspx">List of Contests</a></li>
					<li><a href="../../kpmt/default.aspx">KPMT</a></li>
				</ul>
			</li>
			<li><p>Resources</p>
				<ul>
					<li><a href="../../pages/articles.aspx">Articles</a></li>
					<li><a href="../../pages/practice.aspx">Practice</a></li>
					<li><a href="../../pages/links.aspx">Links</a></li>
				</ul>
			</li>
			<li><p>Members</p>
				<ul>
					<asp:loginview id="MembersMenuLoginView" runat="server">
						<RoleGroups>
							<asp:RoleGroup Roles="Members">
								<ContentTemplate>
									<li><a href="../../members/mymeetings.aspx">My Meetings</a></li>
									<li>
									<a href="../../members/mycompetitions.aspx">My Competitions</a></li>
									<li><a href="../../members/mypoints.aspx">My Pi Points</a></li>
									<li><a href="../../members/myawards.aspx">My Awards</a></li>
									<li><a href="../../members/email.aspx">Send E-mail</a></li>
									<li>
									<a href="../../members/changepassword.aspx">Change Password</a></li>
								</ContentTemplate>
							</asp:RoleGroup>
						</RoleGroups>
					</asp:loginview>
					<li><asp:LoginStatus runat="server"></asp:LoginStatus></li>					
				</ul>
			</li>
			<asp:loginview id="AdminsMenuLoginView" runat="server">
				<RoleGroups>
					<asp:rolegroup Roles="Officers, Admins">
						<ContentTemplate>
							<li><p>Administration</p>
								<ul>
									<li><a href="../../officers/meetings.aspx">Meetings</a></li>
									<li>
									<a href="../../officers/attendance.aspx">Attendance</a></li>
									<li>
									<a href="../../officers/competitions.aspx">Contests</a></li>
									<li>
									<a href="../../officers/competitors.aspx">Competitors</a></li>
									<li><a href="../../officers/teams.aspx">Teams</a></li>
									<li>
									<a href="../../officers/transportation.aspx">Transportation</a></li>
									<li><a href="../../officers/awards.aspx">Awards</a></li>
									<li><a href="../../officers/points.aspx">Pi Points</a></li>
									<li><a href="../../officers/members.aspx">Members</a></li>
									<li><a href="../../officers/parents.aspx">Parents</a></li>
									<li><a href="../../officers/sponsors.aspx">Sponsors</a></li>
									<asp:loginview id="LoginViewAdmins_Users" runat="server">
										<RoleGroups>
											<asp:rolegroup Roles="Admins">
												<ContentTemplate>
													<li>
													<a href="../../admin/users.aspx">Users</a></li>
													<li>
													<a href="../../admin/classes_teachers.aspx">Classes &amp; Teachers</a></li>
												</ContentTemplate>
											</asp:rolegroup>						
										</RoleGroups>
									</asp:loginview>
									<li>
									<a href="../../officers/reports/reports.aspx">Reports</a>
										<ul>
											<li>
											<a href="../../officers/reports/report-points.aspx">Pi Points</a></li>
										</ul>
									</li>
								</ul>
							</li>	
						</ContentTemplate>
					</asp:rolegroup>
				</RoleGroups>
			</asp:loginview>
		</ul>
		
		<div class="searchbox">
			<script type="text/javascript" src="http://www.google.com/cse/brand?form=cse-search-box&amp;lang=en">
			</script>
			<script language="javascript" type="text/javascript">
		        function GoogleSiteSerach() {
		            document.location.href = 'http://www.newportmathclub.org/pages/search.aspx?cx=partner-pub-8846177013257426:k2c5vu-fqre&cof=FORID:9&ie=ISO-8859-1&q=' + document.getElementById('q').value + '&sa=Search';
		        }
			</script>
			<script language="javascript" type="text/javascript">
			function clickButton(e, buttonid){ 
	      		var evt = e ? e : window.event;
	     		var bt = document.getElementById(buttonid);
			    if (bt){ 
			        if (evt.keyCode == 13){ 
			              bt.click(); 
			              return false; 
			        } 
		    	} 
			}
			</script>	
			<input type="text" name="q" size="30" id="q" onkeypress="return clickButton(event,'sa')" />
			<input type="button" onclick="JavaScript:GoogleSiteSerach();" name="sa" value="Search" id="sa" />
		</div>
		
	</div>
	<!-- End Navigation -->
	<!-- Begin Page Content -->
	<div id="page_content">
		<!-- Begin Content -->
		<div id="content">
			<!-- #BeginEditable "content" -->
			<p>Newport Math Challenge!</p>
			<h2>
			<table style="width: 100%">
				<tr>
					<td>Newport Math Challenge - Rules</td>
					<td class="style2">
                            <a href="challenge.aspx">
							<img id="img35" alt="Login" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button63.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button61.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button62.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button62.jpg')" src="buttons/button61.jpg" style="border: 0" width="150" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 9; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0" fp-title="Login" --></a></td>
					<td class="style1">
                            <a href="challenge-register.aspx">
							<img id="img36" alt="Create Account" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button66.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button64.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button65.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button65.jpg')" src="buttons/button64.jpg" style="border: 0" width="150" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 10; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0" fp-title="Create Account" --></a></td>
				</tr>
			</table>
			</h2>
			<p>
                <asp:Image ID="ChallengeImage" runat="server" Height="95px" ImageAlign="Right" 
                    ImageUrl="~/images/Challenge2.png" />
                The following are the official rules for the <a href="challenge.aspx">Newport Math Challenge</a>. All 
                contestants must agree to follow these rules before they will be allowed to 
                register for the contest.</p>
            <h3>Summary of Important Rules</h3>
            <ul>
                <li>This challenge is designed to help you appreciate mathematics, grow as a math 
                    student, and be more prepared to take the American Mathematics Competition (AMC) 
                    in February 2009.</li>
                <li>You must be a student at Newport High School to participate.</li>
                <li>You must <a href="challenge-register.aspx">register</a> for an account and fill 
                    out all required information.</li>
                <li>Questions will be posted at school near the math display case and online at this 
                    website.</li>
                <li>Answers are due online each Friday at 9:00 PM.</li>
                <li>You may use a calculator, but not your friends!</li>
                <li>Be careful - some questions are worth more points than others.</li>
                <li>20 points possible per week x 5 weeks = 100 points possible. No guessing 
                    penalty.</li>
                <li>The top scorers in each grade level (9, 10, 11, and 12) will be recognized at 
                    the end of the contest.</li>
                <li>Your score will only be compared to other people in your grade.</li>
                <li>Check back each week to review the previous weeks&#39; solutions - this is how you 
                    will learn the most.</li>
                <li>If you have a question or would like to report an error, email <a href="mailto:webmaster@newportmathclub.org">
                    challenge@newportmathclub.org</a>.</li>
                <li>If you have further questions, please review the detailed rules below.</li>
                <li>Good luck, and have fun solving!</li>
            </ul>
            <h2>Official Rules</h2>
            <h4>Eligibility and Registration</h4>
            <ul>
                <li>A contestant is defined as anyone who meets the qualifications to enter the 
                    contest and chooses to do so.</li>
                <li>All contestants must be students in grades 9-12 currently enrolled at Newport 
                    High School at the time of entry.</li>
                <li>No contestant may be an officer on leadership in the Newport Math Club at the 
                    time of the contest.</li>
                <li>All contestants must register online, accept these rules, and provide their full 
                    name, grade level, math level, and email address to be eligible to compete. 
                    Failure to provide accurate information or failing to abide by the rules could 
                    result in a penalty up to and including disqualification.</li>
            </ul>
			<h4>
                Contest Format</h4>
            <ul>
                <li>There will be five&nbsp; questions given each of the five weeks during the 
                    contest period, for a total of 25 questions.</li>
                <li>Questions will cover a variety of math topics at a variety of levels of 
                    difficulty. Students should not feel discouraged if questions seem challenging, 
                    as they are aimed to be slightly more difficult than those that will appear on 
                    the AMC.</li>
                <li>Questions will be posted online on the contest page and also at Newport High 
                    School near or on the math display case, which is located between Mr. Nonis&#39; and 
                    Mrs. Fiveash&#39;s rooms on the first floor math wing.</li>
                <li>The answer to each question will be an integer. No answer should be a fraction 
                    or contain a decimal part.</li>
                <li>Answers will be due online at 9:00 PM each Friday evening during the contest 
                    period. Deadlines for each week will be posted along with that week&#39;s questions.</li>
                <li>No late answers will be accepted for any reason.</li>
                <li>Answers and solutions will be posted for each week sometime after the deadline 
                    for that week. It is recommended that all contestants review the solutions to 
                    sharpen their skills for future math tests such as the American Mathematics 
                    Competition (AMC).</li>
             </ul>
            <h4>Use of Aides</h4>
            <ul>
                <li>Use of a standard four-function, scientific, or graphing calculator is 
                    permitted, but by no means required.</li>
                <li>Use of computer software is permitted.</li>
                <li>Contestants may not share or discuss the questions or answers for any week by 
                    any means until after 9:00 PM on the Friday of that week, after the deadline has 
                    passed.</li>
                <li>Students found to have collaborated, distributed answers, or otherwise cheated 
                    and/or violated the spirit of the contest may be given a penalty up to and 
                    including disqualification.</li>
            </ul>
            <h4>
                Scoring</h4>
            <ul>
                <li>Each question will be assigned a point value between 1 and 10 points. Most 
                    questions will be worth between 2 and 6 points.</li>
                <li>Contestants will receive the point value designated for a question if and only 
                    if he/she submitted the correct answer to the question online before the 
                    deadline for the week of the question.</li>
                <li>There will be no point deduction for the submission of an incorrect answer.</li>
                <li>There will be 20 points possible each week, for a possible total of 100 points 
                    throughout the week.</li>
                <li>At the end of the contest, the score of each contestant will be totaled.</li>
                <li>The top scorers in each grade level (9, 10, 11, and 12) will be recognized. Each 
                    student&#39;s score will only be compared to other contestants in his/her grade 
                    level.</li>
                <li>A list of the top scorers in each grade level may be posted at Newport High 
                    School near the display case, kept on file with Newport Math Club, or displayed 
                    on a secured section of the Newport Math Club website only available to club 
                    members.</li>
            </ul>
            <h4>Errors</h4>
            <ul>
                <li>Due to the nature of the contest, questions may arise that contain errors or are 
                    unclear.</li>
                <li>The contest administrators will do their best to clarify the questions and 
                    eliminate any errors that arise.</li>
                <li>Contestants may file a complaint about any question during the contest by 
                    emailing a corrected question, solution, or proof with sufficient clarification 
                    as to the problem with the unclear or flawed question to
                    <a href="mailto:challenge@newportmathclub.org">challenge@newportmathclub.org</a>.</li>
                <li>Such complaints must be received by the submission deadline for the final week 
                    of the contest to be valid for consideration.</li>
                <li>Please notify <a href="mailto:webmaster@newportmathclub.org">
                    webmaster@newportmathclub.org</a> if there is an error on the website, 
                    especially if it causes difficulty in participating in the contest or submitting 
                    answers. All comments and suggestions are appreciated.</li>
                <li>Any contestant who attempts to modify, hack, or otherwise gain access to or 
                    manipulate the website with the intent or result of interfering with the contest 
                    or otherwise affecting its outcome may be penalized or disqualified.</li>
            </ul>
            <h4>
                Contest Administration</h4>
            <ul>
                <li>The contest will be overseen by the officers and advisor of the Newport Math 
                    Club.</li>
                <li>The contest administrators reserve the right to alter any question or answer at 
                    any time in an effort to make the contest more fair and rewarding to all who 
                    participate.</li>
                <li>The contest administrators reserve the right to change these rules at any time 
                    and without warning to make the contest more fair and rewarding to all who 
                    participate.</li>
                <li>The contest administrators reserve the right to disqualify or diminish the score 
                    of any contestant who does not meet the requirements to enter the contest or 
                    violates any of the rules on this page.</li>
            </ul>
            <a id="privacy"></a>
            <h4>
                Privacy Policy</h4>
            <ul>
                <li>We value the privacy of all our contestants.</li>
                <li>All submissions of data, answers, complaints, error requests, and scores become 
                    the property of Newport Math Club.</li>
                <li>Information used to recognize top scorers and that may be published in score 
                    reports is limited to each contestants&#39; name, grade, and math level.</li>
                <li>Email addresses, usernames, and passwords submitted during contestant 
                    registration will be kept strictly among club administration and will not be 
                    posted publicly, distributed to third parties, or displayed on score reports.</li>
                <li>All contest data may be kept on record electronically on a secure section of the 
                    Newport Math Club Website, electronically with Newport Math Club&#39;s other data 
                    files, and on paper records filed with the Newport Math Club.</li>
                <li>For more information about privacy, or if you have any concerns, please email <a href="mailto:webmaster@newportmathclub.org">
                    webmaster@newportmathclub.org</a>.</li>
            </ul>
			<p>
                Good luck, and have fun!</p>
            <p>
                &nbsp;</p>
			<!-- #EndEditable -->
			
			<br/>
			<script type="text/javascript"><!--
				google_ad_client = "pub-8846177013257426";
				/* Newport Math Club Bottom Leaderboard */
				google_ad_slot = "0637970847";
				google_ad_width = 728;
				google_ad_height = 90;
			//-->
			</script>
			<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script>
		</div>
		<!-- End Content -->
		<!-- Begin Sidebar -->
		<div id="sidebar">
			<asp:LoginView id="LoginView" runat="server">
				<AnonymousTemplate>
					<!--<asp:LoginStatus id="LoginStatus1" runat="server" />-->
				</AnonymousTemplate>
				<LoggedInTemplate>
					<div id="user">
					<p><asp:LoginName runat="server" id="LoginName1"></asp:LoginName>
					<span class="float_right"><asp:loginstatus id="LoginStatus1" runat="server" />&nbsp;</span></p>
					</div>
				</LoggedInTemplate>
			</asp:LoginView>
			<h3 style="margin-bottom:5px">2009-10 Sponsors</h3>
				<a href="http://www.tecplot.com/" target="_new_sponsors">
				<img alt="Tecplot" src="../../images/sponsors/Tecplot.jpg" /></a>
				<a href="http://www.knowledgepoints.com/seattle" target="_new_sponsors">
				<img alt="KnowledgePoints" src="../../images/sponsors/KnowledgePoints.jpg" /></a>
				<br/>
			<asp:LoginView ID="LoginViewAds" runat="server">
				<AnonymousTemplate>
					Place your company<br/>logo here! 
					<a href="../../pages/sponsors.aspx">Learn more &raquo;</a>
					<h3 style="margin-bottom:5px">Sponsored Links</h3>
					<script type="text/javascript">
					google_ad_client = "pub-8846177013257426";
					// large - Newport Math Club Right Wide Skyscraper
					google_ad_slot = "9208928404";
					google_ad_width = 160;
					google_ad_height = 600;
					</script>
					<script type="text/javascript"
					src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
					</script>
				</AnonymousTemplate>
				<LoggedInTemplate>
					<h3>Sponsored Links</h3>
					<script type="text/javascript">
						// small
						google_ad_client = "pub-8846177013257426";
						google_ad_slot = "0880078497";
						google_ad_width = 120;
						google_ad_height = 240;
					</script>
					<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
					</script>
				</LoggedInTemplate>
			</asp:LoginView>
			</div>
			<!-- End Sidebar -->
		</div>
		<!-- End Page Content -->
	</div>
	<!-- End Container -->

	<!-- Begin Footer -->
	<div id="footer">
		<span class="footer-left">
			Copyright &copy; 2008-2009 by Newport Math Club
		</span>
		<span class="footer-right">
			<a href="../../pages/about.aspx">About Us</a> |
			<a href="../../pages/contests.aspx">Our Contests</a> |
			<a href="../../pages/officers.aspx">Contact Us</a>
		</span>
	</div>
	<!-- End Footer -->
</form>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-4106925-2");
pageTracker._trackPageview();
} catch(err) {}</script>

</body>

<!-- #EndTemplate -->

</html>
