﻿<%@ Page Language="C#" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        // Reset
        lblRequired.Visible = false;
        lblDuplicate.Visible = false;
        lblBadEmail.Visible = false;
        lblRules.Visible = false;
        lblError.Visible = true;
        
        // Verify Rules
        if (!CheckBox.Checked)
        {
            lblRules.Visible = true;
            return;
        }
        
        // Verify Email
        String email = ((TextBox)(RegistrationForm.FindControl("EmailTextBox"))).Text;
        if (!isValidEmail(email))
        {
            lblBadEmail.Visible = true;
            return;
        }
        
        // Try to create account
        try
        {
            RegistrationForm.InsertItem(true);
        }
        catch (Exception ex)
        {

            if (ex.Message.Contains("Required"))
                // Required Field Left Blank
                lblRequired.Visible = true;
            else if (ex.Message.Contains("duplicate"))
                // Duplicate Name or Username
                lblDuplicate.Visible = true;
            else
                // Unknown Error
                sendMessage("webmaster@newportmathclub.org", "webmaster@newportmathclub.org", "Error",
                    "Error in registration for Newport Math Challenge.\ncatch statement, btnRegister_Click, challenge-register.aspx");
            return;
        }

        // Registration Successful!
        lblError.Visible = false; // no error
        Response.Redirect("challenge.aspx");
        //ShowAlert("Account created successfully! Please login.");
    }

    public void sendMessage(String from, String to, String subject, String message)
    {
        try
        {
            System.Net.Mail.MailMessage myMessage = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress myFrom = new System.Net.Mail.MailAddress(from);
            myMessage.From = myFrom;
            System.Net.Mail.MailAddress myTo = new System.Net.Mail.MailAddress(to);
            myMessage.To.Add(myTo);
            myMessage.Body = message;
            myMessage.Subject = subject;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Send(myMessage);
        }
        catch (Exception)
        {
            ShowAlert("An error occured.");
        }
    }

    public static void ShowAlert(string message)
    {
        // Cleans the message to allow single quotation marks
        string cleanMessage = message.Replace("'", "\\'");
        string script = "<script type=\"text/javascript\">alert('" + cleanMessage;
        script += "');<";
        script += "/script>";

        // Gets the executing web page
        Page page = HttpContext.Current.CurrentHandler as Page;

        // Checks if the handler is a Page and that the script isn't allready on the Page
        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
        {
            page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", script);
        }
    }

    // Tests if an email address is valid.
    public static bool isValidEmail(String s)
    {
        if (s.Length < 5)
            return false;
        int at = s.IndexOf('@', 1);
        int dot = s.IndexOf('.', at + 2);
        
        return at > 0 && dot > 0 && dot < s.Length - 1;
    }

    protected void CheckBox_CheckedChanged(object sender, EventArgs e)
    {
        /*btnRegister.Enabled = CheckBox.Checked;
        if (CheckBox.Checked)
            btnRegister.Focus();
        else
            CheckBox.Focus();*/
        //Control c = RegistrationForm.FindControl("PasswordTextBox");
        //TextBox t = (TextBox)c;
        //t.Text = ViewState["Password"].ToString();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (CheckBox.Attributes.Count == 0)
            CheckBox.Attributes.Add("onclick", "enableButton();");
        // TODO: Need to keep password from clearing when box is checked and unchecked.
        //ViewState["Password"] = ((TextBox)(RegistrationForm.FindControl("PasswordTextBox"))).Text;
    }
</script>

<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml">

<!-- #BeginTemplate "../../master.dwt" -->

<head>
<script type="text/javascript">
window.google_analytics_uacct = "UA-4106925-2";
</script>

<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<!-- #BeginEditable "doctitle" -->
<title>Newport Math Challenge - Registration</title>
        <style type="text/css">















































































            .style1
            {
                width: 99%;
            }
        .style2 {
	text-align: right;
}
        </style>
<script type="text/javascript">
<!--
function FP_swapImg() {//v1.0
 var doc=document,args=arguments,elm,n; doc.$imgSwaps=new Array(); for(n=2; n<args.length;
 n+=2) { elm=FP_getObjectByID(args[n]); if(elm) { doc.$imgSwaps[doc.$imgSwaps.length]=elm;
 elm.$src=elm.src; elm.src=args[n+1]; } }
}
function FP_getObjectByID(id,o) {//v1.0
 var c,el,els,f,m,n; if(!o)o=document; if(o.getElementById) el=o.getElementById(id);
 else if(o.layers) c=o.layers; else if(o.all) el=o.all[id]; if(el) return el;
 if(o.id==id || o.name==id) return o; if(o.childNodes) c=o.childNodes; if(c)
 for(n=0; n<c.length; n++) { el=FP_getObjectByID(id,c[n]); if(el) return el; }
 f=o.forms; if(f) for(n=0; n<f.length; n++) { els=f[n].elements;
 for(m=0; m<els.length; m++){ el=FP_getObjectByID(id,els[n]); if(el) return el; } }
 return null;
}

function FP_preloadImgs() {//v1.0
 var d=document,a=arguments; if(!d.FP_imgs) d.FP_imgs=new Array();
 for(var i=0; i<a.length; i++) { d.FP_imgs[i]=new Image; d.FP_imgs[i].src=a[i]; }
}
// -->
</script>
<!-- #EndEditable -->
<link href="../../styles/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form runat="server" name="theForm">

<!-- Begin Container -->
<div id="container">
	<!-- Begin Masthead -->
	<div id="masthead">
		<a href="../../default.aspx">
		<img alt="Newport Math Club" src="../../images/MathClubBanner.png" height="160" width="1000" /></a>
	</div>
	<!-- End Masthead -->
	<!-- Begin Navigation -->
	<div class="dropdown">
		<ul>
			<li><a href="../../default.aspx">Home</a></li>
			<li><a href="../../pages/about.aspx">About</a>
				<ul>
					<li><a href="../../pages/about.aspx">About the Club</a></li>
					<li><a href="../../pages/achievements.aspx">Achievements</a></li>
					<li><a href="../../pages/officers.aspx">Officers</a></li>
					<li><a href="../../pages/sponsors.aspx">Sponsors</a></li>
				</ul>
			</li>
			<li><p>Events</p>
				<ul>
					<li><a href="../../pages/meetings.aspx">Meetings</a></li>
					<li><a href="../../pages/competitions.aspx">Upcoming Competitions</a></li>
				</ul>
			</li>
			<li><a href="../../pages/contests.aspx">Contests We Host</a>
				<ul>
					<li><a href="../../pages/contests.aspx">List of Contests</a></li>
					<li><a href="../../kpmt/default.aspx">KPMT</a></li>
				</ul>
			</li>
			<li><p>Resources</p>
				<ul>
					<li><a href="../../pages/articles.aspx">Articles</a></li>
					<li><a href="../../pages/practice.aspx">Practice</a></li>
					<li><a href="../../pages/links.aspx">Links</a></li>
				</ul>
			</li>
			<li><p>Members</p>
				<ul>
					<asp:loginview id="MembersMenuLoginView" runat="server">
						<RoleGroups>
							<asp:RoleGroup Roles="Members">
								<ContentTemplate>
									<li><a href="../../members/mymeetings.aspx">My Meetings</a></li>
									<li>
									<a href="../../members/mycompetitions.aspx">My Competitions</a></li>
									<li><a href="../../members/mypoints.aspx">My Pi Points</a></li>
									<li><a href="../../members/myawards.aspx">My Awards</a></li>
									<li><a href="../../members/email.aspx">Send E-mail</a></li>
									<li>
									<a href="../../members/changepassword.aspx">Change Password</a></li>
								</ContentTemplate>
							</asp:RoleGroup>
						</RoleGroups>
					</asp:loginview>
					<li><asp:LoginStatus runat="server"></asp:LoginStatus></li>					
				</ul>
			</li>
			<asp:loginview id="AdminsMenuLoginView" runat="server">
				<RoleGroups>
					<asp:rolegroup Roles="Officers, Admins">
						<ContentTemplate>
							<li><p>Administration</p>
								<ul>
									<li><a href="../../officers/meetings.aspx">Meetings</a></li>
									<li>
									<a href="../../officers/attendance.aspx">Attendance</a></li>
									<li>
									<a href="../../officers/competitions.aspx">Contests</a></li>
									<li>
									<a href="../../officers/competitors.aspx">Competitors</a></li>
									<li><a href="../../officers/teams.aspx">Teams</a></li>
									<li>
									<a href="../../officers/transportation.aspx">Transportation</a></li>
									<li><a href="../../officers/awards.aspx">Awards</a></li>
									<li><a href="../../officers/points.aspx">Pi Points</a></li>
									<li><a href="../../officers/members.aspx">Members</a></li>
									<li><a href="../../officers/parents.aspx">Parents</a></li>
									<li><a href="../../officers/sponsors.aspx">Sponsors</a></li>
									<asp:loginview id="LoginViewAdmins_Users" runat="server">
										<RoleGroups>
											<asp:rolegroup Roles="Admins">
												<ContentTemplate>
													<li>
													<a href="../../admin/users.aspx">Users</a></li>
													<li>
													<a href="../../admin/classes_teachers.aspx">Classes &amp; Teachers</a></li>
												</ContentTemplate>
											</asp:rolegroup>						
										</RoleGroups>
									</asp:loginview>
									<li>
									<a href="../../officers/reports/reports.aspx">Reports</a>
										<ul>
											<li>
											<a href="../../officers/reports/report-points.aspx">Pi Points</a></li>
										</ul>
									</li>
								</ul>
							</li>	
						</ContentTemplate>
					</asp:rolegroup>
				</RoleGroups>
			</asp:loginview>
		</ul>
		
		<div class="searchbox">
			<script type="text/javascript" src="http://www.google.com/cse/brand?form=cse-search-box&amp;lang=en">
			</script>
			<script language="javascript" type="text/javascript">
		        function GoogleSiteSerach() {
		            document.location.href = 'http://www.newportmathclub.org/pages/search.aspx?cx=partner-pub-8846177013257426:k2c5vu-fqre&cof=FORID:9&ie=ISO-8859-1&q=' + document.getElementById('q').value + '&sa=Search';
		        }
			</script>
			<script language="javascript" type="text/javascript">
			function clickButton(e, buttonid){ 
	      		var evt = e ? e : window.event;
	     		var bt = document.getElementById(buttonid);
			    if (bt){ 
			        if (evt.keyCode == 13){ 
			              bt.click(); 
			              return false; 
			        } 
		    	} 
			}
			</script>	
			<input type="text" name="q" size="30" id="q" onkeypress="return clickButton(event,'sa')" />
			<input type="button" onclick="JavaScript:GoogleSiteSerach();" name="sa" value="Search" id="sa" />
		</div>
		
	</div>
	<!-- End Navigation -->
	<!-- Begin Page Content -->
	<div id="page_content">
		<!-- Begin Content -->
		<div id="content">
			<!-- #BeginEditable "content" -->
			<p>Newport Math Challenge!</p>
			<h2>
			<table style="width: 100%">
				<tr>
					<td >Newport Math Challenge Registration</td>
					<td class="style2">
                            <a href="challenge.aspx">
							<img id="img35" alt="Login" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button60.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button5E.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button5F.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img35',/*url*/'buttons/button5F.jpg')" src="buttons/button5E.jpg" style="border: 0" width="150" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 9; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0" fp-title="Login" --></a></td>
					<td class="style2">
                            <a href="challenge-rules.aspx" target="_new2">
							<img id="img37" alt="Contest Rules" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button6C.jpg')" onmouseout="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button6A.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button6B.jpg')" onmouseup="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button6B.jpg')" src="buttons/button6A.jpg" style="border: 0" width="150" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 3; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0" fp-title="Contest Rules" --></a></td>
				</tr>
			</table>
		    </h2>
			<p>
				<asp:Image ID="ChallengeImage" runat="server" Height="168px" ImageAlign="Right" 
                    ImageUrl="~/images/Challenge.png" Width="351px" />
                Use the form below to register for the <a href="challenge.aspx">Newport Math 
                Challenge</a>. These accounts are separate from the account you have on this 
                site if you are a Newport Math Club member. Please fill out the form completely 
                and accurately to be eligible to submit answers.
                </p>
            <p>
				<asp:FormView ID="RegistrationForm" runat="server" DataKeyNames="Username" 
                    DataSourceID="ContestantsData" DefaultMode="Insert" Width="385px">
                    <InsertItemTemplate>
                        <table class="style1">
                            <tr>
                                <td>
                                    Name (First and Last):</td>
                                <td>
                                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' 
                                        Width="171px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Grade:</td>
                                <td>
                                    <asp:DropDownList ID="ClassDropDown0" runat="server" DataSourceID="GradesData" 
                                        DataTextField="Grade" DataValueField="Grade" 
                                        SelectedValue='<%# Bind("Grade") %>'>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Math Class:</td>
                                <td>
                                    <asp:DropDownList ID="ClassDropDown" runat="server" DataSourceID="ClassesData" 
                                        DataTextField="Class_Name" DataValueField="Class_Name" 
                                        SelectedValue='<%# Bind("Class") %>'>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Username:</td>
                                <td>
                                    <asp:TextBox ID="UsernameTextBox" runat="server" Text='<%# Bind("Username") %>' 
                                        Width="140px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Password:</td>
                                <td>
                                    <asp:TextBox ID="PasswordTextBox" runat="server" Text='<%# Bind("Password") %>' 
                                        TextMode="Password" Width="140px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Email:</td>
                                <td>
                                    <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' 
                                        Width="223px" />
                                </td>
                            </tr>
                        </table>
                        <asp:AccessDataSource ID="GradesData" runat="server" 
                            DataFile="../../App_Data/MathChallenge.mdb" 
                            SelectCommand="SELECT * FROM [Grades] ORDER BY [Grade]">
                        </asp:AccessDataSource>
                        <asp:AccessDataSource ID="ClassesData" runat="server" 
                            DataFile="../../App_Data/MathChallenge.mdb" 
                            SelectCommand="SELECT [Class Name] AS Class_Name FROM [Classes] ORDER BY [Class Name]">
                        </asp:AccessDataSource>
                    </InsertItemTemplate>
                </asp:FormView>
            </p>
            <p>
                Note: Your email address will only be used to send you contest updates and to 
                send you your password if you lose it. It will not be posted or distributed to 
                others. For more information, please refer to our
                <a href="challenge-rules.aspx#privacy">privacy policy</a>.
                <asp:AccessDataSource ID="ContestantsData" runat="server" 
                    ConflictDetection="CompareAllValues" DataFile="../../App_Data/MathChallenge.mdb" 
                    InsertCommand="INSERT INTO [Contestants] ([Name], [Grade], [Class], [Username], [Password], [Email]) VALUES (?, ?, ?, ?, ?, ?)" > 
                    <InsertParameters>
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="Grade" Type="Int32" />
                        <asp:Parameter Name="Class" Type="String" />
                        <asp:Parameter Name="Username" Type="String" />
                        <asp:Parameter Name="Password" Type="String" />
                        <asp:Parameter Name="Email" Type="String" />
                    </InsertParameters>
                </asp:AccessDataSource>
            </p>
            <p>
				<asp:CheckBox ID="CheckBox" runat="server" oncheckedchanged="CheckBox_CheckedChanged" />&nbsp; 
                I have read and agree to the <a href="challenge-rules.aspx" target="_blank" >
                official contest rules</a> and assert that the above information is complete and 
                correct.
            </p>
            <p>
				<asp:Button ID="btnRegister" runat="server" onclick="btnRegister_Click" Text="Register" />
                <script type="text/javascript">              // this script should come after the register button only (in aspx page)
                    if (!document.getElementById('<%=CheckBox.ClientID%>').checked)
                        document.getElementById('<%=btnRegister.ClientID%>').disabled=true;
                    function enableButton()
                    {
                        var obj=document.getElementById('<%=btnRegister.ClientID%>'); 
                        obj.disabled=!obj.disabled;
                    }
                </script>
                <br />
				<asp:Label ID="lblRequired" runat="server" ForeColor="Red" 
                    Text="*Error: All fields are required. Please fill out all fields before continuing." 
                    Visible="False"></asp:Label>
				<asp:Label ID="lblDuplicate" runat="server" ForeColor="Red" 
                    Text="*Error: There is already an account with the requested name and/or username. Please try again." 
                    Visible="False"></asp:Label>
				<asp:Label ID="lblBadEmail" runat="server" ForeColor="Red" 
                    Text="*Error: Please enter a valid email address." Visible="False"></asp:Label>
				<asp:Label ID="lblRules" runat="server" ForeColor="Red" 
                    Text="*Error: You must accept the contest rules to continue." Visible="False"></asp:Label>
                    <br />
				<asp:Label ID="lblError" runat="server" ForeColor="Red" 
                    Text="*An error occured during registration. Please reenter your password and try again." Visible="False"></asp:Label>
            </p>
			<!-- #EndEditable -->
			
			<br/>
			<script type="text/javascript"><!--
				google_ad_client = "pub-8846177013257426";
				/* Newport Math Club Bottom Leaderboard */
				google_ad_slot = "0637970847";
				google_ad_width = 728;
				google_ad_height = 90;
			//-->
			</script>
			<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script>
		</div>
		<!-- End Content -->
		<!-- Begin Sidebar -->
		<div id="sidebar">
			<asp:LoginView id="LoginView" runat="server">
				<AnonymousTemplate>
					<!--<asp:LoginStatus id="LoginStatus1" runat="server" />-->
				</AnonymousTemplate>
				<LoggedInTemplate>
					<div id="user">
					<p><asp:LoginName runat="server" id="LoginName1"></asp:LoginName>
					<span class="float_right"><asp:loginstatus id="LoginStatus1" runat="server" />&nbsp;</span></p>
					</div>
				</LoggedInTemplate>
			</asp:LoginView>
			<h3 style="margin-bottom:5px">2009-10 Sponsors</h3>
				<a href="http://www.tecplot.com/" target="_new_sponsors">
				<img alt="Tecplot" src="../../images/sponsors/Tecplot.jpg" /></a>
				<a href="http://www.knowledgepoints.com/seattle" target="_new_sponsors">
				<img alt="KnowledgePoints" src="../../images/sponsors/KnowledgePoints.jpg" /></a>
				<br/>
			<asp:LoginView ID="LoginViewAds" runat="server">
				<AnonymousTemplate>
					Place your company<br/>logo here! 
					<a href="../../pages/sponsors.aspx">Learn more &raquo;</a>
					<h3 style="margin-bottom:5px">Sponsored Links</h3>
					<script type="text/javascript">
					google_ad_client = "pub-8846177013257426";
					// large - Newport Math Club Right Wide Skyscraper
					google_ad_slot = "9208928404";
					google_ad_width = 160;
					google_ad_height = 600;
					</script>
					<script type="text/javascript"
					src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
					</script>
				</AnonymousTemplate>
				<LoggedInTemplate>
					<h3>Sponsored Links</h3>
					<script type="text/javascript">
						// small
						google_ad_client = "pub-8846177013257426";
						google_ad_slot = "0880078497";
						google_ad_width = 120;
						google_ad_height = 240;
					</script>
					<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
					</script>
				</LoggedInTemplate>
			</asp:LoginView>
			</div>
			<!-- End Sidebar -->
		</div>
		<!-- End Page Content -->
	</div>
	<!-- End Container -->

	<!-- Begin Footer -->
	<div id="footer">
		<span class="footer-left">
			Copyright &copy; 2008-2009 by Newport Math Club
		</span>
		<span class="footer-right">
			<a href="../../pages/about.aspx">About Us</a> |
			<a href="../../pages/contests.aspx">Our Contests</a> |
			<a href="../../pages/officers.aspx">Contact Us</a>
		</span>
	</div>
	<!-- End Footer -->
</form>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-4106925-2");
pageTracker._trackPageview();
} catch(err) {}</script>

</body>

<!-- #EndTemplate -->

</html>
