﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuestionView.ascx.cs" Inherits="contests_QuestionView" %>
<style type="text/css">


    .styleQuestionView1
    {
        color: #CC3300;
    }
    .styleQuestionView2
    {
        width: 333px;
    }


</style>
<table width="100%">
    <tr>
        <td class="styleQuestionView2">
            Question <asp:Label ID="lblQuestion" runat="server" Text="#" />: <asp:Label ID="lblCategory" runat="server" Text="Category" />
            <span class="styleQuestionView1"><b>&nbsp;(&nbsp;<asp:Label ID="lblPoints" runat="server" Text="# Points" />)&nbsp;</b></span>
        </td>
        <td>
<asp:DetailsView ID="DetailsView" runat="server" AutoGenerateRows="False" DataSourceID="DataSource"
    Height="30px" Width="100%" EmptyDataText="No Answer Submitted.">
    <EmptyDataRowStyle HorizontalAlign="Center" />
    <Fields>
        <asp:BoundField DataField="Answer" HeaderText="Answer" SortExpression="Answer" ShowHeader="False">
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
    </Fields>
</asp:DetailsView>
        </td>
    </tr>
</table>
<asp:AccessDataSource ID="DataSource" runat="server" DataFile="../../App_Data/MathChallenge.mdb" SelectCommand="SELECT [Answer] FROM [Answers] WHERE ([Username] = ? AND [QuestionID] = ?)"
    InsertCommand="INSERT INTO [Answers]([Username], [QuestionID], [Answer]) Values(?, ?, ?)"
    OnSelecting="DataSource_Selecting" OnInserting="DataSource_Inserting" >
    <SelectParameters>
        <asp:ControlParameter ControlID="txtUsername" Name="Username" PropertyName="Text" Type="String" />
        <asp:Parameter Name="QuestionID" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:ControlParameter ControlID="txtUsername" Name="Username" PropertyName="Text" Type="String" />
        <asp:Parameter Name="QuestionID" Type="Int32" />
    </InsertParameters>
</asp:AccessDataSource>
<asp:AccessDataSource ID="QuestionData" runat="server" 
    DataFile="../../App_Data/MathChallenge.mdb" 
    
    
    SelectCommand="SELECT [ID], [Category], [PointValue] FROM [Questions] WHERE ([ID] = ?)" >
    <SelectParameters>
        <asp:ControlParameter ControlID="lblQuestion" Name="ID" 
            PropertyName="Text" Type="Int32" />
    </SelectParameters>
</asp:AccessDataSource>

<asp:GridView ID="QuestionGrid" runat="server" AutoGenerateColumns="False" 
    DataKeyNames="ID" DataSourceID="QuestionData" Visible="False" 
    ondatabound="QuestionGrid_DataBound">
    <Columns>
        <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" 
            SortExpression="ID" />
        <asp:BoundField DataField="Category" HeaderText="Category" 
            SortExpression="Category" />
        <asp:BoundField DataField="PointValue" HeaderText="PointValue" 
            SortExpression="PointValue" />
    </Columns>
</asp:GridView>


