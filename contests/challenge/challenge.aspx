﻿<%@ Page Language="C#" %>

<%@ Register Src="WeekView.ascx" TagName="WeekView" TagPrefix="uc" %>
<%@ Register Src="QuestionView.ascx" TagName="QuestionView" TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        nameGrid.DataBind();
        emailGrid.DataBind();
        lblLoginError.Visible = false;
        lblUsernameError.Visible = false;
        btnEmail.Visible = false;
        if (nameGrid.Rows.Count <= 0)
        {

            if (emailGrid.Rows.Count <= 0)
                lblUsernameError.Visible = true;
            else
            {
                lblLoginError.Visible = true;
                btnEmail.Visible = true;
            }
        }
        else
        {
            lblName.Text = nameGrid.Rows[0].Cells[0].Text;
            pnlLogin.Visible = false;
            pnlAnswer.Visible = true;

            WeekView1.Update();
            WeekView2.Update();
            WeekView3.Update();
            WeekView4.Update();
            WeekView5.Update();
        }
    }

    public static void ShowAlert(string message)
    {
        // Cleans the message to allow single quotation marks
        string cleanMessage = message.Replace("'", "\\'");
        string script = "<script type=\"text/javascript\">alert('" + cleanMessage;
        script += "');<";
        script += "/script>";

        // Gets the executing web page
        Page page = HttpContext.Current.CurrentHandler as Page;

        // Checks if the handler is a Page and that the script isn't allready on the Page
        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
        {
            page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", script);
        }
    }

    protected void btnEmail_Click(object sender, EventArgs e)
    {
        try
        {
            emailGrid.DataBind();
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress from = new System.Net.Mail.MailAddress("challenge@newportmathclub.org", "Newport Math Challenge");
            message.From = from;
            String email = emailGrid.Rows[0].Cells[0].Text;
            String username = txtUsername.Text;
            String password = emailGrid.Rows[0].Cells[1].Text;
            String name = emailGrid.Rows[0].Cells[2].Text;
            System.Net.Mail.MailAddress to = new System.Net.Mail.MailAddress(email, name);
            message.To.Add(to);
            message.Body = "Newport Math Challenge\n\n" + name + "\nUsername: " + username + "\nPassword: " + password + "\n\n" +
                "Please return to www.newportmathclub.org/contests/challenge.aspx to login and submit your answers.\n\n-Newport Math Club";
            message.Subject = "Newport Math Challenge - Login Details";
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Send(message);
            ShowAlert("Your password has been emailed to you at " + email + ".");
        }
        catch (Exception)
        {
            ShowAlert("An error occured during the password recovery process. Please contact a club officer.");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected enum WeekState { Hidden, Open, Closed };

    [Serializable]
    protected class Week
    {
        public WeekState state;
        [NonSerialized]
        public Panel panel;
        [NonSerialized]
        public Button submitButton;
        [NonSerialized]
        public Label formatError;
        [NonSerialized]
        public HiddenField questionID;
        public Question[] questions;

        public Week(WeekState state, Panel panel, Button submit, Label error, HiddenField questionID, Question[] questions)
        {
            this.state = state;
            this.panel = panel;
            this.submitButton = submit;
            this.formatError = error;
            this.questionID = questionID;
            this.questions = questions;
        }

        public void Update()
        {

        }

        public void SubmitAnswers()
        {
            formatError.Visible = false;
            foreach (Question q in questions)
            {
                try
                {
                    if (q.view.CurrentMode == DetailsViewMode.Insert)
                    {
                        questionID.Value = q.id.ToString();
                        q.view.InsertItem(true);
                    }
                }
                catch (FormatException)
                {
                    formatError.Visible = true;
                }
            }
            this.Update();
        }
    }

    [Serializable]
    protected struct Question
    {
        public int id;
        [NonSerialized]
        public DetailsView view;

        public Question(int id, DetailsView view)
        {
            this.id = id;
            this.view = view;
        }

    }

</script>
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml">
<!-- #BeginTemplate "../../master.dwt" -->
<head>
    <script type="text/javascript">
        window.google_analytics_uacct = "UA-4106925-2";
    </script>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <!-- #BeginEditable "doctitle" -->
    <title>Newport Math Club - Newport Math Challenge</title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 128px;
        }
        .style4
        {
            width: 73px;
        }
        .style5
        {
            text-align: right;
        }
        .style6
        {
            text-align: right;
            width: 319px;
        }
    </style>
    <script type="text/javascript">
<!--
        function FP_swapImg() {//v1.0
            var doc = document, args = arguments, elm, n; doc.$imgSwaps = new Array(); for (n = 2; n < args.length;
 n += 2) {
                elm = FP_getObjectByID(args[n]); if (elm) {
                    doc.$imgSwaps[doc.$imgSwaps.length] = elm;
                    elm.$src = elm.src; elm.src = args[n + 1];
                } 
            }
        }

        function FP_preloadImgs() {//v1.0
            var d = document, a = arguments; if (!d.FP_imgs) d.FP_imgs = new Array();
            for (var i = 0; i < a.length; i++) { d.FP_imgs[i] = new Image; d.FP_imgs[i].src = a[i]; }
        }

        function FP_getObjectByID(id, o) {//v1.0
            var c, el, els, f, m, n; if (!o) o = document; if (o.getElementById) el = o.getElementById(id);
            else if (o.layers) c = o.layers; else if (o.all) el = o.all[id]; if (el) return el;
            if (o.id == id || o.name == id) return o; if (o.childNodes) c = o.childNodes; if (c)
                for (n = 0; n < c.length; n++) { el = FP_getObjectByID(id, c[n]); if (el) return el; }
            f = o.forms; if (f) for (n = 0; n < f.length; n++) {
                els = f[n].elements;
                for (m = 0; m < els.length; m++) { el = FP_getObjectByID(id, els[n]); if (el) return el; } 
            }
            return null;
        }
// -->
    </script>
    <!-- #EndEditable -->
    <link href="../../styles/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form runat="server" name="theForm">
    <!-- Begin Container -->
    <div id="container">
        <!-- Begin Masthead -->
        <div id="masthead">
            <a href="../../default.aspx">
                <img alt="Newport Math Club" src="../../images/MathClubBanner.png" height="160" width="1000" /></a>
        </div>
        <!-- End Masthead -->
        <!-- Begin Navigation -->
        <div class="dropdown">
            <ul>
                <li><a href="../../default.aspx">Home</a></li>
                <li><a href="../../pages/about.aspx">About</a>
                    <ul>
                        <li><a href="../../pages/about.aspx">About the Club</a></li>
                        <li><a href="../../pages/achievements.aspx">Achievements</a></li>
                        <li><a href="../../pages/officers.aspx">Officers</a></li>
                        <li><a href="../../pages/sponsors.aspx">Sponsors</a></li>
                    </ul>
                </li>
                <li>
                    <p>
                        Events</p>
                    <ul>
                        <li><a href="../../pages/meetings.aspx">Meetings</a></li>
                        <li><a href="../../pages/competitions.aspx">Upcoming Competitions</a></li>
                    </ul>
                </li>
                <li><a href="../../pages/contests.aspx">Contests We Host</a>
                    <ul>
                        <li><a href="../../pages/contests.aspx">List of Contests</a></li>
                        <li><a href="../../kpmt/default.aspx">KPMT</a></li>
                    </ul>
                </li>
                <li>
                    <p>
                        Resources</p>
                    <ul>
                        <li><a href="../../pages/articles.aspx">Articles</a></li>
                        <li><a href="../../pages/practice.aspx">Practice</a></li>
                        <li><a href="../../pages/links.aspx">Links</a></li>
                    </ul>
                </li>
                <li>
                    <p>
                        Members</p>
                    <ul>
                        <asp:LoginView ID="MembersMenuLoginView" runat="server">
                            <RoleGroups>
                                <asp:RoleGroup Roles="Members">
                                    <ContentTemplate>
                                        <li><a href="../../members/mymeetings.aspx">My Meetings</a></li>
                                        <li><a href="../../members/mycompetitions.aspx">My Competitions</a></li>
                                        <li><a href="../../members/mypoints.aspx">My Pi Points</a></li>
                                        <li><a href="../../members/myawards.aspx">My Awards</a></li>
                                        <li><a href="../../members/email.aspx">Send E-mail</a></li>
                                        <li><a href="../../members/changepassword.aspx">Change Password</a></li>
                                    </ContentTemplate>
                                </asp:RoleGroup>
                            </RoleGroups>
                        </asp:LoginView>
                        <li>
                            <asp:LoginStatus runat="server"></asp:LoginStatus>
                        </li>
                    </ul>
                </li>
                <asp:LoginView ID="AdminsMenuLoginView" runat="server">
                    <RoleGroups>
                        <asp:RoleGroup Roles="Officers, Admins">
                            <ContentTemplate>
                                <li>
                                    <p>
                                        Administration</p>
                                    <ul>
                                        <li><a href="../../officers/meetings.aspx">Meetings</a></li>
                                        <li><a href="../../officers/attendance.aspx">Attendance</a></li>
                                        <li><a href="../../officers/competitions.aspx">Contests</a></li>
                                        <li><a href="../../officers/competitors.aspx">Competitors</a></li>
                                        <li><a href="../../officers/teams.aspx">Teams</a></li>
                                        <li><a href="../../officers/transportation.aspx">Transportation</a></li>
                                        <li><a href="../../officers/awards.aspx">Awards</a></li>
                                        <li><a href="../../officers/points.aspx">Pi Points</a></li>
                                        <li><a href="../../officers/members.aspx">Members</a></li>
                                        <li><a href="../../officers/parents.aspx">Parents</a></li>
                                        <li><a href="../../officers/sponsors.aspx">Sponsors</a></li>
                                        <asp:LoginView ID="LoginViewAdmins_Users" runat="server">
                                            <RoleGroups>
                                                <asp:RoleGroup Roles="Admins">
                                                    <ContentTemplate>
                                                        <li><a href="../../admin/users.aspx">Users</a></li>
                                                        <li><a href="../../admin/classes_teachers.aspx">Classes &amp; Teachers</a></li>
                                                    </ContentTemplate>
                                                </asp:RoleGroup>
                                            </RoleGroups>
                                        </asp:LoginView>
                                        <li><a href="../../officers/reports/reports.aspx">Reports</a>
                                            <ul>
                                                <li><a href="../../officers/reports/report-points.aspx">Pi Points</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ContentTemplate>
                        </asp:RoleGroup>
                    </RoleGroups>
                </asp:LoginView>
            </ul>
            <div class="searchbox">
                <script type="text/javascript" src="http://www.google.com/cse/brand?form=cse-search-box&amp;lang=en">
                </script>
                <script language="javascript" type="text/javascript">
                    function GoogleSiteSerach() {
                        document.location.href = 'http://www.newportmathclub.org/pages/search.aspx?cx=partner-pub-8846177013257426:k2c5vu-fqre&cof=FORID:9&ie=ISO-8859-1&q=' + document.getElementById('q').value + '&sa=Search';
                    }
                </script>
                <script language="javascript" type="text/javascript">
                    function clickButton(e, buttonid) {
                        var evt = e ? e : window.event;
                        var bt = document.getElementById(buttonid);
                        if (bt) {
                            if (evt.keyCode == 13) {
                                bt.click();
                                return false;
                            }
                        }
                    }
                </script>
                <input type="text" name="q" size="30" id="q" onkeypress="return clickButton(event,'sa')" />
                <input type="button" onclick="JavaScript:GoogleSiteSerach();" name="sa" value="Search"
                    id="sa" />
            </div>
        </div>
        <!-- End Navigation -->
        <!-- Begin Page Content -->
        <div id="page_content">
            <!-- Begin Content -->
            <div id="content">
                <!-- #BeginEditable "content" -->
                <p>
                    Newport Math Challenge!</p>
                <h2>
                    <table style="width: 100%">
                        <tr>
                            <td>
                                Newport Math Challenge
                            </td>
                            <td class="style6">
                                <a href="challenge-register.aspx">
                                    <img id="img36" alt="Create Account" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button69.jpg')"
                                        onmouseout="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button67.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button68.jpg')"
                                        onmouseup="FP_swapImg(0,0,/*id*/'img36',/*url*/'buttons/button68.jpg')" src="buttons/button67.jpg"
                                        style="border: 0" width="150" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 10; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0" fp-title="Create Account" --></a>
                            </td>
                            <td class="style5">
                                <a href="challenge-rules.aspx" target="_new2">
                                    <img id="img37" alt="Contest Rules" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button6F.jpg')"
                                        onmouseout="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button6D.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button6E.jpg')"
                                        onmouseup="FP_swapImg(0,0,/*id*/'img37',/*url*/'buttons/button6E.jpg')" src="buttons/button6D.jpg"
                                        style="border: 0" width="150" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 3; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0" fp-title="Contest Rules" --></a>
                            </td>
                        </tr>
                    </table>
                </h2>
                <asp:AccessDataSource ID="LoginData" runat="server" DataFile="../../App_Data/MathChallenge.mdb"
                    SelectCommand="SELECT [Name] FROM [Contestants] WHERE (([Username] = ?) AND ([Password] = ?))">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtUsername" Name="Username" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtPassword" Name="Password" PropertyName="Text"
                            Type="String" />
                    </SelectParameters>
                </asp:AccessDataSource>
                <asp:AccessDataSource ID="EmailData" runat="server" DataFile="../../App_Data/MathChallenge.mdb"
                    SelectCommand="SELECT [Email], [Password], [Name] FROM [Contestants] WHERE ([Username] = ?)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtUsername" Name="Username" PropertyName="Text"
                            Type="String" />
                    </SelectParameters>
                </asp:AccessDataSource>
                <asp:Panel ID="pnlLogin" runat="server">
                    <p>
                        <asp:Image ID="ChallengeImage" runat="server" Height="125px" ImageAlign="Right" ImageUrl="~/images/Challenge2.png" />
                        Welcome to the <b>Newport Math Challenge</b>! This 25-question contest will be held
                        over a five-week period and is open to <i>all</i> Newport High School students.
                        It is designed to help sharpen students&#39; math skills, prepare them for the AMC,
                        and get the entire student body excited about math! Please <b>login</b> to view
                        the questions, submit your answers, and view past solutions.</p>
                    <p>
                        New to Newport Math Challenge? <b><a href="challenge-register.aspx">Create an account!</a>
                        </b>&nbsp;This will register you for the contest and allow you to submit answers.<br />
                        (Note: Accounts for this contest are separate from the general Newport Math Club
                        member accounts on this website.)<br />
                        If you have questions about the contest, please check out the <a href="challenge-rules.aspx"
                            target="_blank">Official Contest Rules</a>.</p>
                    <table width="50px">
                        <tr>
                            <td class="style4">
                                Username:
                            </td>
                            <td class="style3">
                                <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style4">
                                Password:
                            </td>
                            <td class="style3">
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <asp:Label ID="lblUsernameError" runat="server" ForeColor="Red" Text="Username not found. Have you made a separate account for this contest?"
                            Visible="False"></asp:Label>
                        <asp:Label ID="lblLoginError" runat="server" ForeColor="Red" Text="Incorrect password. Please try again."
                            Visible="False"></asp:Label>
                    </p>
                    <p>
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
                        &nbsp;<asp:Button ID="btnEmail" runat="server" Text="Email my Password" Visible="False"
                            OnClick="btnEmail_Click" />
                        <asp:GridView ID="nameGrid" runat="server" AutoGenerateColumns="False" DataSourceID="LoginData"
                            Visible="False">
                            <Columns>
                                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                            </Columns>
                        </asp:GridView>
                        <asp:GridView ID="emailGrid" runat="server" AutoGenerateColumns="False" DataSourceID="EmailData"
                            Visible="False">
                            <Columns>
                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password" />
                                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                            </Columns>
                        </asp:GridView>
                    </p>
                </asp:Panel>
                <asp:Panel ID="pnlAnswer" runat="server" Visible="False">
                    <asp:Image ID="ChallengeImage2" runat="server" Height="106px" ImageAlign="Left" ImageUrl="~/images/Challenge2.png" />
                    Welcome,
                    <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>! Use the form below
                    to view your submitted answers and to submit additional answers. Remember to check
                    back each week so that you can answer each week&#39;s problems and view solutions
                    from previous weeks. Good luck!<br />
                    <br />
                    <a href="challenge-questions.aspx" target="_new">
                        <img id="img38" alt="View Questions &amp; Solutions" height="30" onmousedown="FP_swapImg(0,0,/*id*/'img38',/*url*/'button8.jpg')"
                            onmouseout="FP_swapImg(0,0,/*id*/'img38',/*url*/'button4.jpg')" onmouseover="FP_swapImg(0,0,/*id*/'img38',/*url*/'button7.jpg')"
                            onmouseup="FP_swapImg(0,0,/*id*/'img38',/*url*/'button7.jpg')" src="button4.jpg"
                            style="border: 0" width="250" /><!-- MSComment="ibutton" fp-style="fp-btn: Soft Capsule 9; fp-font-style: Bold; fp-font-size: 12; fp-preload: 0; fp-proportional: 0" fp-title="View Questions and Solutions" --></a><br />
                    <br />
                    <br />
                    <!--It is currently <b>Week 5</b> of the contest. -->
                    The contest is now closed. Solutions to weeks 1-4 can be found by clicking the tan
                    button above.<br />
                    <!--<b>Important Reminders: All answers are integers! Answers are due each Friday at 
                9:00 PM. <a href="challenge-rules.aspx" target="_blank" >More Information»</a></b>-->
                    <br />
                    <br />
                    Jump To: <a href="#week1">Week 1</a> ·&nbsp;<a href="#week2">Week 2</a> ·&nbsp;
                    <a href="#week3">Week 3</a> ·&nbsp;<a href="#week4">Week 4</a> ·&nbsp;<a href="#week5">Week
                        5</a> · <a href="challenge-rules.aspx" target="_blank">Official Contest Rules</a><br />
                    <a id="week1"></a>
                    <uc:WeekView ID="WeekView1" runat="server" State="Closed" Week="1" />
                    <a id="week2"></a>
                    <uc:WeekView ID="WeekView2" runat="server" State="Closed" Week="2" />
                    <a id="week3"></a>
                    <uc:WeekView ID="WeekView3" runat="server" State="Closed" Week="3" />
                    <a id="week4"></a>
                    <uc:WeekView ID="WeekView4" runat="server" State="Closed" Week="4" />
                    <a id="week5"></a>
                    <uc:WeekView ID="WeekView5" runat="server" State="Closed" Week="5" />
                    <br />
                </asp:Panel>
                <!-- #EndEditable -->
                <br />
                <script type="text/javascript"><!--
                    google_ad_client = "pub-8846177013257426";
                    /* Newport Math Club Bottom Leaderboard */
                    google_ad_slot = "0637970847";
                    google_ad_width = 728;
                    google_ad_height = 90;
			//-->
                </script>
                <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                </script>
            </div>
            <!-- End Content -->
            <!-- Begin Sidebar -->
            <div id="sidebar">
                <asp:LoginView ID="LoginView" runat="server">
                    <AnonymousTemplate>
                        <!--<asp:LoginStatus id="LoginStatus1" runat="server" />-->
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        <div id="user">
                            <p>
                                <asp:LoginName runat="server" ID="LoginName1"></asp:LoginName>
                                <span class="float_right">
                                    <asp:LoginStatus ID="LoginStatus1" runat="server" />
                                    &nbsp;</span></p>
                        </div>
                    </LoggedInTemplate>
                </asp:LoginView>
                <h3 style="margin-bottom: 5px">
                    2009-10 Sponsors</h3>
                <a href="http://www.tecplot.com/" target="_new_sponsors">
                    <img alt="Tecplot" src="../../images/sponsors/Tecplot.jpg" /></a> <a href="http://www.knowledgepoints.com/seattle"
                        target="_new_sponsors">
                        <img alt="KnowledgePoints" src="../../images/sponsors/KnowledgePoints.jpg" /></a>
                <br />
                <asp:LoginView ID="LoginViewAds" runat="server">
                    <AnonymousTemplate>
                        Place your company<br />
                        logo here! <a href="../../pages/sponsors.aspx">Learn more &raquo;</a>
                        <h3 style="margin-bottom: 5px">
                            Sponsored Links</h3>
                        <script type="text/javascript">
                            google_ad_client = "pub-8846177013257426";
                            // large - Newport Math Club Right Wide Skyscraper
                            google_ad_slot = "9208928404";
                            google_ad_width = 160;
                            google_ad_height = 600;
                        </script>
                        <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                        </script>
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        <h3>
                            Sponsored Links</h3>
                        <script type="text/javascript">
                            // small
                            google_ad_client = "pub-8846177013257426";
                            google_ad_slot = "0880078497";
                            google_ad_width = 120;
                            google_ad_height = 240;
                        </script>
                        <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                        </script>
                    </LoggedInTemplate>
                </asp:LoginView>
            </div>
            <!-- End Sidebar -->
        </div>
        <!-- End Page Content -->
    </div>
    <!-- End Container -->
    <!-- Begin Footer -->
    <div id="footer">
        <span class="footer-left">Copyright &copy; 2008-2009 by Newport Math Club </span>
        <span class="footer-right"><a href="../../pages/about.aspx">About Us</a> | <a href="../../pages/contests.aspx">
            Our Contests</a> | <a href="../../pages/officers.aspx">Contact Us</a> </span>
    </div>
    <!-- End Footer -->
    </form>
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-4106925-2");
            pageTracker._trackPageview();
        } catch (err) { }</script>
</body>
<!-- #EndTemplate -->
</html>
