﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class contests_QuestionView : System.Web.UI.UserControl
{
    public int? QuestionID
    {
        set 
        {
            ViewState["QuestionID"] = value;
            lblQuestion.Text = value.ToString();
        }
        get
        {
            if (ViewState["QuestionID"] != null)
                return (int)ViewState["QuestionID"];
            return null;
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)    
    {

    }

    public void UpdateData()
    {
        QuestionGrid.DataBind();
        if (QuestionGrid.Rows.Count < 1)
        {
            lblCategory.Text = "NOT FOUND";
            lblPoints.Text = "ERROR";
        }
        else
        {
            lblCategory.Text = QuestionGrid.Rows[0].Cells[1].Text;
            int points = int.Parse(QuestionGrid.Rows[0].Cells[2].Text);
            lblPoints.Text = points.ToString() + " Point";
            if (points != 1 && points != -1)
                lblPoints.Text += "s";
        }
    }

    protected void DataSource_Selecting(object sender, SqlDataSourceSelectingEventArgs e)    
    {
        if (QuestionID.HasValue)
            e.Command.Parameters["QuestionID"].Value = QuestionID.Value;
        else
            e.Cancel = true;
    }

    protected void DataSource_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        if (QuestionID.HasValue)
        {
            e.Command.Parameters["QuestionID"].Value = QuestionID.Value;

            //string username = e.Command.Parameters["Username"].Value.ToString();
            //int question = QuestionID.Value;
            System.Data.Common.DbCommand command = e.Command;
            object answer = command.Parameters["Answer"].Value;
            //string debug_message = "Insert... Username: " + username + ", QuestionID: " + question + ", Answer: " + answer;

            // Don't insert if there is no answer. In that case, e.Command will now be null.
            if (answer == null)
                e.Cancel = true;
        }
        else
            e.Cancel = true;
    }

    public bool IsEmpty()
    {
        return DetailsView.Rows.Count == 0 || DetailsView.Rows[0].Cells[0].Text.Contains("No Answer") || DetailsView.Rows[0].Cells[0].Text.Length == 0;
    }

    public bool IsInserting()
    {
        return DetailsView.CurrentMode == DetailsViewMode.Insert;
    }

    public void UpdateInserting()
    {
        DetailsView.DataBind();
        if (IsEmpty())
            DetailsView.ChangeMode(DetailsViewMode.Insert);
        else
            DetailsView.ChangeMode(DetailsViewMode.ReadOnly);
    }

    public void Insert()
    {
            if (DetailsView.CurrentMode == DetailsViewMode.Insert)
                DetailsView.InsertItem(true);
    }

    protected void QuestionGrid_DataBound(object sender, EventArgs e)
    {

    }
}
