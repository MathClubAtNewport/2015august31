﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WeekView.ascx.cs" Inherits="contests_WeekView" %>
<%@ Register src="QuestionView.ascx" TagName="QuestionView" TagPrefix="uc" %>

<style type="text/css">

</style>

<script type="text/javascript">
<!--
function FP_swapImg() {//v1.0
 var doc=document,args=arguments,elm,n; doc.$imgSwaps=new Array(); for(n=2; n<args.length;
 n+=2) { elm=FP_getObjectByID(args[n]); if(elm) { doc.$imgSwaps[doc.$imgSwaps.length]=elm;
 elm.$src=elm.src; elm.src=args[n+1]; } }
}

function FP_preloadImgs() {//v1.0
 var d=document,a=arguments; if(!d.FP_imgs) d.FP_imgs=new Array();
 for(var i=0; i<a.length; i++) { d.FP_imgs[i]=new Image; d.FP_imgs[i].src=a[i]; }
}

function FP_getObjectByID(id,o) {//v1.0
 var c,el,els,f,m,n; if(!o)o=document; if(o.getElementById) el=o.getElementById(id);
 else if(o.layers) c=o.layers; else if(o.all) el=o.all[id]; if(el) return el;
 if(o.id==id || o.name==id) return o; if(o.childNodes) c=o.childNodes; if(c)
 for(n=0; n<c.length; n++) { el=FP_getObjectByID(id,c[n]); if(el) return el; }
 f=o.forms; if(f) for(n=0; n<f.length; n++) { els=f[n].elements;
 for(m=0; m<els.length; m++){ el=FP_getObjectByID(id,els[n]); if(el) return el; } }
 return null;
}
// -->
</script>
<body onload="FP_preloadImgs(/*url*/'button7.jpg',/*url*/'button8.jpg')">

<h3>
    Your Answers: Week
    <asp:Label ID="WeekNumberLabel" runat="server" Text="#"></asp:Label>
</h3>
<table width="70%">
    <tr>
        <td>
<uc:QuestionView ID="QuestionView1" runat="server" QuestionID="1" />
        </td>
    </tr>
    <tr>
        <td>
<uc:QuestionView ID="QuestionView2" runat="server" QuestionID="2" />
        </td>
    </tr>
    <tr>
        <td>
<uc:QuestionView ID="QuestionView3" runat="server" QuestionID="3" />
        </td>
    </tr>
    <tr>
        <td>
<uc:QuestionView ID="QuestionView4" runat="server" QuestionID="4" />
        </td>
    </tr>
    <tr>
        <td>
<uc:QuestionView ID="QuestionView5" runat="server" QuestionID="5" />
        </td>
    </tr>
    <tr>
        <td>
<asp:Label ID="ErrorLabel" runat="server" ForeColor="Red" Text="*All answers must be integers." Visible="False" Width="123%"></asp:Label>
<asp:Button ID="SubmitButton" runat="server" Text="Submit Answers" onclick="SubmitButton_Click" />
        </td>
    </tr>
</table>
</body>
    
