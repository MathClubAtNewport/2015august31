﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class contests_WeekView : System.Web.UI.UserControl
{
    public enum WeekState { Hidden, Open, Closed };

    public WeekState? State
    {
        set
        {
            ViewState["WeekState"] = value;
        }
        get
        {
            if (ViewState["WeekState"] != null)
                return (WeekState)ViewState["WeekState"];
            return null;
        }
    }

    public int? Week
    {
        set
        { 
            ViewState["Week"] = value;
            WeekNumberLabel.Text = value.ToString();
        }
        get
        {
            if (ViewState["Week"] != null)
                return (int)ViewState["Week"];
            return null;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Update()
    {
        if (State == WeekState.Hidden)
        {
            Visible = false;
            return;
        }

        QuestionView1.QuestionID = 5 * Week - 4;
        QuestionView2.QuestionID = 5 * Week - 3;
        QuestionView3.QuestionID = 5 * Week - 2;
        QuestionView4.QuestionID = 5 * Week - 1;
        QuestionView5.QuestionID = 5 * Week;
        QuestionView1.UpdateData();
        QuestionView2.UpdateData();
        QuestionView3.UpdateData();
        QuestionView4.UpdateData();
        QuestionView5.UpdateData();
        SubmitButton.Visible = false;
        if (State == WeekState.Open)
        {
            QuestionView1.UpdateInserting();
            QuestionView2.UpdateInserting();
            QuestionView3.UpdateInserting();
            QuestionView4.UpdateInserting();
            QuestionView5.UpdateInserting();
            if (QuestionView1.IsInserting() || QuestionView2.IsInserting() || QuestionView3.IsInserting() || QuestionView4.IsInserting() || QuestionView5.IsInserting())
                SubmitButton.Visible = true;
        }
    }

    public void SubmitAnswers()
    {
        ErrorLabel.Visible = false;
        try
        {
            QuestionView1.Insert();
        }
        catch (Exception)
        {
            ErrorLabel.Visible = true;
        }
        try
        {
            QuestionView2.Insert();
        }
        catch (Exception)
        {
            ErrorLabel.Visible = true;
        }
        try
        {
            QuestionView3.Insert();
        }
        catch (Exception)
        {
            ErrorLabel.Visible = true;
        }
        try
        {
            QuestionView4.Insert();
        }
        catch (Exception)
        {
            ErrorLabel.Visible = true;
        }
        try
        {
            QuestionView5.Insert();
        }
        catch (Exception)
        {
            ErrorLabel.Visible = true;
        }
        Update();
    }

    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        SubmitAnswers();
    }
}
