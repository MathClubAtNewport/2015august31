﻿<%@ Page Title="Newport Math Club &ndash; NA&T Contests" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        National Assessment and Testing Contests</h2>
    <p>
        Newport Math Club hosts the five NA&T contests at our school each year. These include
        the following:</p>
    <ul>
        <li>Fall Startup Event</li>
        <li>Team Scramble</li>
        <li>Ciphering Time Trials</li>
        <li>Four-by-Four Competition</li>
        <li>Collaborative Problem Solving Contest</li>
    </ul>
    <p>
        For more information, please visit the <a href="http://www.natassessment.com" target="_blank">
            National Assessment & Testing website</a>.</p>
</asp:Content>
