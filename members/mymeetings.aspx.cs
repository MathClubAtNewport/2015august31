﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class members_mymeetings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        namelabel.Text = User.Identity.Name;
        if (User.IsInRole("Officers"))
            AttendanceGrid.DataSourceID = AttendanceDataSource_Officer.ID;
        AttendedLabel.Text = AttendanceGrid.Rows.Count.ToString();
    }
}