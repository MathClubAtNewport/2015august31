﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class members_myteams : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //nameLabel.Text = Page.User.Identity.Name;
        //ShowHide();
    }

    protected void TeamDetails_DataBound(object sender, EventArgs e)
    {
        CompetitionDetails.DataBind();
    }

    protected void ShowHide()
    {
        if (TeamMembersGrid.SelectedIndex < 0 || MyGrid.SelectedIndex < 0)
            ContactInfoPanel.Visible = false;
        else
            ContactInfoPanel.Visible = true;
        if (MyGrid.SelectedIndex < 0)
        {
            TeamDetailsPanel.Visible = false;
            TeamMembersPanel.Visible = false;
        }
        else
        {
            TeamDetailsPanel.Visible = true;
            TeamMembersPanel.Visible = true;
        }
    }

    protected void TeamMembersGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHide();
    }

    protected void MyGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHide();
    }

    protected void CompetitionsDataSource_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["Member"].Value = User.Identity.Name;
    }
}