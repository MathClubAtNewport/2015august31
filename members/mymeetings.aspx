﻿<%@ Page Title="Newport Math Club &ndash; My Meetings" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="mymeetings.aspx.cs" Inherits="members_mymeetings" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        My Meetings</h2>
    <p>
        This page lists the meetings you have attended and details about them. Click <a href="../pages/meetings.aspx">
            here</a> to view a list of all meetings.</p>
    <asp:Label ID="namelabel" runat="server" Visible="false"></asp:Label>
    <asp:AccessDataSource ID="AttendanceDataSource_Member" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT A.[ID], M.[Date] as meetingDate, M.[Type] as meetingType, M.[Minutes], M.[Notes] FROM [Attendance] A, [Meetings] M WHERE [Name] = ? AND M.[ID] = A.[Meeting] AND M.[Type] <> ? ORDER BY M.[Date] DESC">
        <SelectParameters>
            <asp:ControlParameter ControlID="namelabel" Name="Name" PropertyName="Text" Type="String" />
            <asp:Parameter DefaultValue="Officer Meeting" Type="String" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="AttendanceDataSource_Officer" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT A.[ID], M.[Date] as meetingDate, M.[Type] as meetingType, M.[Minutes], M.[Notes] FROM [Attendance] A, [Meetings] M
        WHERE [Name] = ? AND M.[ID] = A.[Meeting] ORDER BY M.[Date] DESC">
        <SelectParameters>
            <asp:ControlParameter ControlID="namelabel" Name="Name" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:AccessDataSource>
    <b>Total Meetings Attended:
        <asp:Label ID="AttendedLabel" runat="server"></asp:Label>
        <br />
    </b>
    <br />
    <asp:GridView ID="AttendanceGrid" runat="server" AutoGenerateColumns="False" CellPadding="4"
        DataKeyNames="ID" DataSourceID="AttendanceDataSource_Member" ForeColor="#333333"
        GridLines="None" AllowSorting="True">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                SortExpression="ID" Visible="False" />
            <asp:BoundField DataField="meetingDate" HeaderText="Meeting Date" SortExpression="meetingDate"
                DataFormatString="{0:ddd. M/d/yy h:mm tt}" />
            <asp:BoundField DataField="meetingType" HeaderText="Type" SortExpression="meetingType" />
            <asp:HyperLinkField DataNavigateUrlFields="Minutes" HeaderText="Minutes" Text="Download"
                SortExpression="Minutes" />
            <asp:BoundField DataField="Notes" HeaderText="Notes" SortExpression="Notes" />
        </Columns>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>
