﻿<%@ Page Title="Newport Math Club &ndash; My Competitions" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="mycompetitions.aspx.cs" Inherits="members_mycompetitions" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <asp:AccessDataSource ID="CompetitionsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT P.[ID], C.[ID] AS CompetitionID, C.[Name], C.[Location], C.[Date], C.[StartTime] FROM [Competitors] P, [Competitions] C
                WHERE P.[Member] = ? AND C.[ID] = P.[Competition]" OnSelecting="CompetitionsData_Selecting">
        <SelectParameters>
            <asp:Parameter Name="Member" Type="String" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="EligibilityData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT P.[ID], P.[Permission], P.[Paid] FROM [Competitors] P, [Competitions] C
                WHERE P.[Member] = ? AND P.[Competition] = ?" OnSelecting="EligibilityData_Selecting">
        <SelectParameters>
            <asp:Parameter Name="Member" Type="String" />
            <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Name="ID"
                Type="Int32" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="CompetitionDetailsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT * FROM [Competitions] WHERE [ID]=? ">
        <SelectParameters>
            <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Name="ID"
                Type="Int32" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="TeamsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT T.[ID], T.[Team Description] FROM [Team_Members] M, [Teams] T, [Competitors] C WHERE T.[Competition]=? AND C.[Member]=? AND M.[Competitor]=C.[ID] AND M.[Team] = T.[ID] "
        OnSelecting="TeamsData_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Name="ID"
                Type="Int32" />
            <asp:Parameter Name="Member" Type="String" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="TeamMembersData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT M.[Name], M.[Grade], M.[Email], M.[HomePhone], M.[CellPhone] FROM [Members] M, [Competitors] C, [Team_Members] T
                WHERE T.[Team]=? AND C.[ID]=T.[Competitor] AND M.[Name] = C.[Member]">
        <SelectParameters>
            <asp:ControlParameter ControlID="TeamsGrid" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="TransportationData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT S.[ID], S.[Provider], S.[Method] FROM [Transportation_Sources] S, [Transportation] T, [Competitors] C
                WHERE C.[Member]=? AND C.[Competition]=? AND T.[Competitor] = C.[ID] AND S.[ID] = T.[Source] "
        OnSelecting="TransportationData_Selecting">
        <SelectParameters>
            <asp:Parameter Name="Member" Type="String" />
            <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Name="ID"
                Type="Int32" />
        </SelectParameters>
    </asp:AccessDataSource>
    <h2>
        My Competitions</h2>
    <p>
        This page shows details about the competitions you are attending and how to contact
        your team members.</p>
    <table class="style5">
        <tr>
            <td valign="top" class="style6">
                <h4>
                    Select a Competition:</h4>
                <br />
                <asp:GridView ID="CompetitionsGrid" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" CellPadding="4" DataKeyNames="CompetitionID" DataSourceID="CompetitionsData"
                    ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="CompetitionsGrid_SelectedIndexChanged">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" SortExpression="ID"
                            Visible="False" />
                        <asp:BoundField DataField="CompetitionID" HeaderText="CompetitionID" InsertVisible="False"
                            SortExpression="CompetitionID" Visible="False" />
                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                        <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" />
                        <asp:BoundField DataField="Date" DataFormatString="{0:M/d/yy}" HeaderText="Date"
                            SortExpression="Date" />
                        <asp:BoundField DataField="StartTime" DataFormatString="{0:h:mm tt}" HeaderText="Start Time"
                            SortExpression="StartTime" />
                    </Columns>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnShow" runat="server" Text="Show Competition Details" OnClick="btnShow_Click"
        Visible="False" />
    <br />
    <asp:Panel ID="CompetitionPanel" runat="server" Visible="False">
        <table class="style5">
            <tr>
                <td valign="top" colspan="2">
                    <asp:Panel ID="CompetitionDetailsPanel" runat="server" Visible="False">
                        <h4>
                            Competition Details &nbsp;
                            <asp:Button ID="btnHide" runat="server" Text="Hide" OnClick="btnHide_Click" />
                        </h4>
                        <asp:DetailsView ID="CompetitionDetails" runat="server" AutoGenerateRows="False"
                            CellPadding="4" DataKeyNames="ID" DataSourceID="CompetitionDetailsData" ForeColor="#333333"
                            GridLines="None" Height="50px" Width="100%">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <Fields>
                                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                    SortExpression="ID" Visible="False" />
                                <asp:BoundField DataField="Name" HeaderText="Competition Name" SortExpression="Name" />
                                <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" />
                                <asp:BoundField DataField="Date" DataFormatString="{0:M/d/yy}" HeaderText="Date"
                                    SortExpression="Date" />
                                <asp:BoundField DataField="MeetTime" DataFormatString="{0:h:mm tt}" HeaderText="Meet Time"
                                    SortExpression="MeetTime" />
                                <asp:BoundField DataField="StartTime" DataFormatString="{0:h:mm tt}" HeaderText="Start Time"
                                    SortExpression="StartTime" />
                                <asp:BoundField DataField="EndTime" DataFormatString="{0:h:mm tt}" HeaderText="End Time"
                                    SortExpression="EndTime" />
                                <asp:BoundField DataField="Pay" DataFormatString="{0:c}" HeaderText="Cost" SortExpression="Pay" />
                                <asp:BoundField DataField="Meals" HeaderText="Meals" SortExpression="Meals" />
                                <asp:BoundField DataField="Transportation" HeaderText="Transportation" SortExpression="Transportation" />
                                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                            </Fields>
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:DetailsView>
                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <h4>
                        My Eligibility</h4>
                    <br />
                    <asp:Panel ID="EligiblePanel" runat="server" ForeColor="Green" Visible="False">
                        <asp:Image ID="EligibleImage" runat="server" ImageAlign="Middle" ImageUrl="~/images/OK.bmp" />
                        &nbsp;You are eligible.</asp:Panel>
                    <asp:Panel ID="NotEligiblePanel" runat="server" ForeColor="Red">
                        <asp:Image ID="EligibleImage0" runat="server" ImageAlign="Middle" ImageUrl="~/images/Critical.bmp" />
                        &nbsp;You are not eligible!</asp:Panel>
                    <p>
                        <asp:DetailsView ID="EligibilityDetails" runat="server" AutoGenerateRows="False"
                            CellPadding="4" DataKeyNames="ID" DataSourceID="EligibilityData" ForeColor="#333333"
                            GridLines="None" Height="50px" Width="125px">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <Fields>
                                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                    SortExpression="ID" Visible="False" />
                                <asp:CheckBoxField DataField="Permission" HeaderText="Permission" SortExpression="Permission" />
                                <asp:CheckBoxField DataField="Paid" HeaderText="Paid" SortExpression="Paid" />
                            </Fields>
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:DetailsView>
                    </p>
                    <h4>
                        My Transportation</h4>
                    <p>
                        <asp:FormView ID="TransportationForm" runat="server" AllowPaging="True" CellPadding="4"
                            DataKeyNames="ID" DataSourceID="TransportationData" ForeColor="#333333">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <EditItemTemplate>
                                ID:
                                <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' />
                                <br />
                                Provider:
                                <asp:TextBox ID="ProviderTextBox" runat="server" Text='<%# Bind("Provider") %>' />
                                <br />
                                Method:
                                <asp:TextBox ID="MethodTextBox" runat="server" Text='<%# Bind("Method") %>' />
                                <br />
                                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                                    Text="Update" />
                                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False"
                                    CommandName="Cancel" Text="Cancel" />
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                Provider:
                                <asp:TextBox ID="ProviderTextBox0" runat="server" Text='<%# Bind("Provider") %>' />
                                <br />
                                Method:
                                <asp:TextBox ID="MethodTextBox0" runat="server" Text='<%# Bind("Method") %>' />
                                <br />
                                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                                    Text="Insert" />
                                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False"
                                    CommandName="Cancel" Text="Cancel" />
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' Visible="false" />
                                Provider:
                                <asp:Label ID="ProviderLabel" runat="server" Text='<%# Bind("Provider") %>' />
                                <br />
                                Method:
                                <asp:Label ID="MethodLabel" runat="server" Text='<%# Bind("Method") %>' />
                                <br />
                            </ItemTemplate>
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        </asp:FormView>
                    </p>
                </td>
                <td valign="top">
                    <h4>
                        My Teams</h4>
                    <p>
                        <asp:GridView ID="TeamsGrid" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="TeamsData"
                            ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="TeamsGrid_SelectedIndexChanged">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" SelectText="View Team Members" />
                                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                    SortExpression="ID" Visible="False" />
                                <asp:BoundField DataField="Team Description" HeaderText="Team Description" SortExpression="Team Description" />
                            </Columns>
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </p>
                    <asp:Panel ID="TeamMembersPanel" runat="server" Visible="False">
                        <h4>
                            Team Members</h4>
                        <asp:GridView ID="TeamMembersGrid" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Name" DataSourceID="TeamMembersData"
                            ForeColor="#333333" GridLines="None">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <Columns>
                                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" SortExpression="Name" />
                                <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade" />
                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                <asp:BoundField DataField="HomePhone" HeaderText="Home Phone" SortExpression="HomePhone" />
                                <asp:BoundField DataField="CellPhone" HeaderText="Cell Phone" SortExpression="CellPhone" />
                            </Columns>
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
