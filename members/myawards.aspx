﻿<%@ Page Title="Newport Math Club &ndash; My Awards" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="myawards.aspx.cs" Inherits="members_myawards" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <asp:Label ID="nameLabel" Visible="false" runat="server" />
    <asp:AccessDataSource ID="TeamData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT A.[ID], C.[Name], T.[ID] as TeamID, T.[Team Description], A.[Place], A.[Division], A.[Award Description] as description
                FROM [Team_Awards] A, [Teams] T, [Competitions] C, [Competitors] P, [Team_Members] M
                WHERE P.[Member]=? AND M.[Competitor]=P.[ID] AND A.[Team]=M.[Team] AND T.[ID]=A.[Team] AND C.[ID]=T.[Competition]
                ORDER BY C.[Date] DESC">
        <SelectParameters>
            <asp:ControlParameter ControlID="nameLabel" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="TeamMembersData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT C.[Member] FROM [Team_Members] M, [Competitors] C WHERE C.[ID]=M.[Competitor] AND M.[Team]=? ORDER BY C.[Member]">
        <SelectParameters>
            <asp:ControlParameter ControlID="TeamGrid" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="IndividualData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT A.[ID], C.[Name], P.[Member], A.[Place], A.[Division], A.[Award Description] as description
                FROM [Individual_Awards] A, [Competitors] P, [Competitions] C
                WHERE P.[Member]=? AND P.[ID]=A.[Competitor] AND C.[ID]=P.[Competition]
                ORDER BY C.[Date] DESC">
        <SelectParameters>
            <asp:ControlParameter ControlID="nameLabel" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:AccessDataSource>
    <h2>
        My Awards</h2>
    <p>
        This page lists the awards you have received at competitions.</p>
    <asp:Label ID="awardsCount" Text="Total Awards: ?" runat="server" Style="font-weight: 700" />
    <h4>
        Individual Awards</h4>
    <p>
        <asp:GridView ID="IndividualGrid" runat="server" AutoGenerateColumns="False" CellPadding="4"
            DataSourceID="IndividualData" ForeColor="#333333" GridLines="None" AllowSorting="True">
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" SortExpression="ID"
                    Visible="False" />
                <asp:BoundField DataField="Name" HeaderText="Competition" SortExpression="Name" />
                <asp:BoundField DataField="Member" HeaderText="Member" SortExpression="Member" Visible="False" />
                <asp:BoundField DataField="Place" HeaderText="Place" SortExpression="Place" />
                <asp:BoundField DataField="Division" HeaderText="Division" SortExpression="Division" />
                <asp:BoundField DataField="description" HeaderText="Award Description" SortExpression="description" />
            </Columns>
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </p>
    <h4>
        Team Awards</h4>
    <p>
        <asp:GridView ID="TeamGrid" runat="server" AutoGenerateColumns="False" DataKeyNames="TeamID"
            CellPadding="4" DataSourceID="TeamData" ForeColor="#333333" GridLines="None"
            AllowSorting="True">
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:CommandField ShowSelectButton="True" SelectText="View Team Members" />
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" SortExpression="ID"
                    Visible="False" />
                <asp:BoundField DataField="Name" HeaderText="Competition" SortExpression="Name" />
                <asp:BoundField DataField="TeamID" HeaderText="TeamID" InsertVisible="False" SortExpression="TeamID"
                    Visible="False" />
                <asp:BoundField DataField="Team Description" HeaderText="Team" SortExpression="Team Description" />
                <asp:BoundField DataField="Place" HeaderText="Place" SortExpression="Place" />
                <asp:BoundField DataField="Division" HeaderText="Division" SortExpression="Division" />
                <asp:BoundField DataField="description" HeaderText="Award Description" SortExpression="description" />
            </Columns>
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" CellPadding="4" DataSourceID="TeamMembersData" ForeColor="#333333"
            GridLines="None">
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:BoundField DataField="Member" HeaderText="Team Members" SortExpression="Member" />
            </Columns>
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </p>
    <br />
</asp:Content>
