﻿<%@ Page Title="Newport Math Club &ndash; My Teams" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="myteams.aspx.cs" Inherits="members_myteams" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        My Competitions</h2>
    <p>
        This page shows details about the competitions you are attending and how to contact
        your team members.</p>
    <asp:Label ID="nameLabel" runat="server" Visible="false" Text="-Name-" />
    <br />
    <asp:AccessDataSource ID="CompetitionsDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT C.[ID], C.[Name], C.[Date], C.[StartTime] FROM [Competitions] C, [Competitors] P
                    WHERE P.[Member]=?  AND (SELECT COUNT(*) FROM [Competitors] WHERE [Member]=P.[Member] AND [Competition]=C.[ID]) &gt; 0 ORDER BY C.[Date] DESC"
        OnSelecting="CompetitionsDataSource_Selecting">
        <SelectParameters>
            <asp:Parameter Name="Member" Type="String" DefaultValue="a" />
        </SelectParameters>
    </asp:AccessDataSource>
    <!--<asp:AccessDataSource ID="MyDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb" 
                SelectCommand="SELECT C.[ID], C.[Team] as myTeam, A.[Name] as competitionName, T.[Team Description] as teamName, C.[Permission], C.[Paid], A.[Location], A.[Date], A.[StartTime]
                FROM [Competitors] C, [Teams] T, [Competitions] A
                WHERE C.[Member]=? AND T.[ID]=C.[Team] AND A.[ID]=T.[Competition] ORDER BY A.[Date] DESC">
                <SelectParameters>
                    <asp:ControlParameter ControlID="namelabel" Name="Member" PropertyName="Text" Type="String" />
                </SelectParameters>
            </asp:AccessDataSource>
            
			<asp:AccessDataSource ID="CompetitorsDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb" 
                SelectCommand="SELECT [ID], [Member] FROM [Competitors] WHERE ([Team] = ?) ORDER BY [Member]">
                <SelectParameters>
                    <asp:ControlParameter ControlID="MyGrid" Name="Team" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:AccessDataSource>
            
			<asp:AccessDataSource ID="TeamsDataSource" runat="server" 
                DataFile="~/App_Data/MathClub.mdb"  
                SelectCommand="SELECT [Competition], [Team Description] AS Team_Description FROM [Teams] WHERE ([ID] = ?)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="MyGrid" Name="ID" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:AccessDataSource>
            
			<asp:AccessDataSource ID="MembersDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb" 
                SelectCommand="SELECT [Name], [Email], [HomePhone], [CellPhone] FROM [Members] WHERE ([Name] = ?)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="TeamMembersGrid" Name="Name" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
            </asp:AccessDataSource>
            
            
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:BoundField DataField="Date" DataFormatString="{0:M/d/yy}" 
                        HeaderText="Date" SortExpression="Date" />
                    <asp:BoundField DataField="StartTime" DataFormatString="{0:h:mm tt}" 
                        HeaderText="Start Time" SortExpression="StartTime" />
                        -->
    <br />
    <h4>
        Select a Competition</h4>
    <p>
    </p>
    <br />
    <!--
            <table class="style6">
                <tr>
                    <td valign="top">
                        <h4>Select a Team</h4>
                        <p>
                            <asp:GridView ID="MyGrid" runat="server" AllowPaging="True" 
                                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                                DataSourceID="MyDataSource" ForeColor="#333333" 
                                GridLines="None" DataKeyNames="myTeam" OnSelectedIndexChanged="MyGrid_SelectedIndexChanged">
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" SelectText="View Details"/>
                                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" Visible="false" SortExpression="ID" />
                                    <asp:BoundField DataField="competitionName" HeaderText="Competition" SortExpression="competitionName" />
                                    <asp:BoundField DataField="teamName" HeaderText="Team" SortExpression="teamName" />
                                    <asp:CheckBoxField DataField="Permission" HeaderText="Permission" SortExpression="Permission" />
                                    <asp:CheckBoxField DataField="Paid" HeaderText="Paid" SortExpression="Paid" />
                                </Columns>
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </p>
                            <table class="style6">
                                <tr>
                                    <td valign="top">
                                        <asp:Panel ID="TeamDetailsPanel" runat="server">
                                            <h4>
                                                Competition Details</h4>
                                            <asp:DetailsView ID="CompetitionDetails" runat="server" AutoGenerateRows="False" CellPadding="4"
                                            DataSourceID="MyDataSource" ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
                                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                                <Fields>
                                                    <asp:BoundField DataField="competitionName" HeaderText="Competition" SortExpression="competitionName" />
                                                    <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" />
                                                    <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" DataFormatString="{0:ddd. M/d/yy}" />
                                                    <asp:BoundField DataField="StartTime" DataFormatString="{0:h:mm tt}" HeaderText="Start Time" SortExpression="Start Time" />
                                                </Fields>
                                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                            </asp:DetailsView>
                                        </asp:Panel>
                                       
                                    </td>
                                    <td valign="top">
                                        
                                        <asp:Panel ID="TeamMembersPanel" runat="server">
                                            <h4>Team Members</h4>
                                            <asp:GridView ID="TeamMembersGrid" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                                            DataSourceID="CompetitorsDataSource" ForeColor="#333333" GridLines="None" DataKeyNames="Member" OnSelectedIndexChanged="TeamMembersGrid_SelectedIndexChanged">
                                                <FooterStyle BackColor="#990000" ForeColor="White" />
                                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True" SelectText="Contact Info"/>
                                                    <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" InsertVisible="False" ReadOnly="True" Visible="False" />
                                                    <asp:BoundField DataField="Member" HeaderText="Name" 
                                        SortExpression="Member" />
                                                </Columns>
                                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                            </asp:GridView>
                                        </asp:Panel>
                                            <asp:Panel ID="ContactInfoPanel" runat="server">
                                                <h4>
                                                    Contact Info</h4>
                                                <asp:DetailsView ID="ContactDetails" runat="server" Height="50px" Width="125px" 
                                                AutoGenerateRows="False" 
    CellPadding="4" DataKeyNames="Name" 
                                                
    DataSourceID="MembersDataSource" ForeColor="#333333" GridLines="None">
                                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                    <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                    <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                                    <Fields>
                                                        <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" 
                                                        SortExpression="Name" />
                                                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                        <asp:BoundField DataField="HomePhone" HeaderText="Home Phone" 
                                                        SortExpression="HomePhone" />
                                                        <asp:BoundField DataField="CellPhone" HeaderText="Cell Phone" 
                                                            SortExpression="CellPhone" />
                                                    </Fields>
                                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:DetailsView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        <p>
                            &nbsp;</p>
                    </td>
                </tr>
            </table>
            -->
</asp:Content>
