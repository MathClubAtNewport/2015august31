﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class members_myawards : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        nameLabel.Text = User.Identity.Name;
        int ind = IndividualGrid.Rows.Count;
        int team = TeamGrid.Rows.Count;
        awardsCount.Text = "Total Awards: " + (ind + team).ToString()
            + " (" + ind.ToString() + " as individual, " + team.ToString() + " as team)";
    }
}