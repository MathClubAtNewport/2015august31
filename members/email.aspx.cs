﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class members_email : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(User.IsInRole("Officers") || User.IsInRole("Teachers") || User.IsInRole("Administrators")))
        {
            if (rdoFromNew.Items.Count >= 2) // Items not yet removed
            {
                rdoFromNew.Items.RemoveAt(0); // Newport Math Club
                rdoTo.Items.RemoveAt(3); // All Members
            }
        }
        string myName = User.Identity.Name;
        string myAddress = "";
        System.Data.DataView view = MembersDataSource.Select(DataSourceSelectArguments.Empty) as System.Data.DataView;
        System.Data.DataTable table = view.ToTable();
        for (int i = 0; i < table.Rows.Count; i++) // 0 Name, 1 Address
        {
            string name = table.Rows[i].ItemArray[0].ToString().Trim();
            string address = table.Rows[i].ItemArray[1].ToString().Trim();
            if (name == User.Identity.Name)
                myAddress = address;
        }
        if (myAddress != null && myAddress.Length > 0)
            rdoFromNew.Items[rdoFromNew.Items.Count - 1].Text = "Me (" + myName + " &lt;" + myAddress + "&gt;)";


        /*int[] i = new int[4];
        if (User.Identity.Name == "Kevin Liu")
        {
            i[0] = 2;
            i[1] = 3;
            i[2] = 4;
            i[3] = 5;
        }
        else if (User.Identity.Name == "Austin Davis")
        {
            i[0] = 1;
            i[1] = 3;
            i[2] = 4;
            i[3] = 5;
        }
        else if (User.Identity.Name == "Andrew Wang")
        {
            i[0] = 1;
            i[1] = 2;
            i[2] = 4;
            i[3] = 5;
        }
        else if (User.Identity.Name == "Pei Tao")
        {
            i[0] = 1;
            i[1] = 2;
            i[2] = 3;
            i[3] = 5;
        }
        else if (User.Identity.Name.ToLower().Contains("nonis"))
        {
            i[0] = 1;
            i[1] = 2;
            i[2] = 3;
            i[3] = 4;
        }
        for (int j = 0; j < i.Length; j++)
            rdoFrom.Items[i[j]].Enabled = false;*/
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        System.Net.Mail.MailAddress from;
        /*switch (rdoFrom.SelectedValue)
        {
            case "Kevin Liu":
                from = new System.Net.Mail.MailAddress("newport.math.club@gmail.com", "Kevin Liu");
                break;
            case "Austin Davis":
                from = new System.Net.Mail.MailAddress("newport.math.club@gmail.com", "Austin Davis");
                break;
            case "Andrew Wang":
                from = new System.Net.Mail.MailAddress("newport.math.club@gmail.com", "Andrew Wang");
                break;
            case "Pei Tao":
                from = new System.Net.Mail.MailAddress("newport.math.club@gmail.com", "Pei Tao");
                break;
            case "Mr. Nonis":
                from = new System.Net.Mail.MailAddress("newport.math.club@gmail.com", "Mr. Nonis");
                break;
            default:
                from = new System.Net.Mail.MailAddress("newport.math.club@gmail.com", "Newport Math Club");
                break;      
        }*/
        System.Data.DataView view = MembersDataSource.Select(DataSourceSelectArguments.Empty) as System.Data.DataView;
        System.Data.DataTable table = view.ToTable();
        string myName = User.Identity.Name;
        string myAddress = "";
        for (int i = 0; i < table.Rows.Count; i++) // 0 Name, 1 Address
        {
            string name = table.Rows[i].ItemArray[0].ToString().Trim();
            string address = table.Rows[i].ItemArray[1].ToString().Trim();
            if (name == User.Identity.Name)
                myAddress = address;
        }

        System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

        switch (rdoFromNew.SelectedValue)
        {
            case "Newport Math Club":
                from = new System.Net.Mail.MailAddress("postmaster@newportmathclub.org", "Newport Math Club");
                break;
            default:
                if (myAddress == null || myAddress.Length <= 0)
                {
                    if (User.IsInRole("Officers") || User.IsInRole("Administrators") || User.IsInRole("Teachers"))
                        ShowAlert("You cannot send a message because you have no e-mail address listed in your account profile. Edit your profile on the member management page and add your address.");
                    else
                        ShowAlert("You cannot send a message because you have no e-mail address listed in your account profile. Contact an administrator to have your address added.");
                    return;
                }
                from = new System.Net.Mail.MailAddress(myAddress, myName);
                message.ReplyToList.Add(new System.Net.Mail.MailAddress(myAddress, myName));
                break;

        }
        message.From = from;

        switch (rdoTo.SelectedValue)
        {

            case "All Officers & Mr. Nonis":
                for (int i = 0; i < table.Rows.Count; i++) // 0 Name, 1 Address
                {
                    string name = table.Rows[i].ItemArray[0].ToString().Trim();
                    string address = table.Rows[i].ItemArray[1].ToString().Trim();
                    if ((name.Equals("Austin Davis") || name.Equals("Andrew Wang") || name.Equals("Sophia Wang") || name.Equals("Emily Zhang") || name.Equals("Brian Liou"))
                        && address != null && address.Length > 0)
                        message.To.Add(new System.Net.Mail.MailAddress(address, name));
                }
                message.To.Add(new System.Net.Mail.MailAddress("nonisj@bsd405.org", "Mr. Nonis"));
                break;
            case "All Officers & Mr. Nonis & Mr. Vu":
                for (int i = 0; i < table.Rows.Count; i++) // 0 Name, 1 Address
                {
                    string name = table.Rows[i].ItemArray[0].ToString().Trim();
                    string address = table.Rows[i].ItemArray[1].ToString().Trim();
                    if ((name.Equals("Austin Davis") || name.Equals("Andrew Wang") || name.Equals("Sophia Wang") || name.Equals("Emily Zhang") || name.Equals("Brian Liou"))
                        && address != null && address.Length > 0)
                        message.To.Add(new System.Net.Mail.MailAddress(address, name));
                }
                message.To.Add(new System.Net.Mail.MailAddress("nonisj@bsd405.org", "Mr. Nonis"));
                message.To.Add(new System.Net.Mail.MailAddress("vuk@bsd405.org", "Mr. Vu"));
                break;
            case "All Members":
                for (int i = 0; i < table.Rows.Count; i++) // 0 Name, 1 Address
                {
                    string name = table.Rows[i].ItemArray[0].ToString().Trim();
                    string address = table.Rows[i].ItemArray[1].ToString().Trim();
                    if (address != null && address.Length > 0)
                        message.To.Add(new System.Net.Mail.MailAddress(address, name));
                }
                message.To.Add(new System.Net.Mail.MailAddress("nonisj@bsd405.org", "Mr. Nonis"));
                message.To.Add(new System.Net.Mail.MailAddress("vuk@bsd405.org", "Mr. Vu"));
                break;
            case "Austin Davis (President)":
                for (int i = 0; i < table.Rows.Count; i++) // 0 Name, 1 Address
                {
                    string name = table.Rows[i].ItemArray[0].ToString().Trim();
                    string address = table.Rows[i].ItemArray[1].ToString().Trim();
                    if (name.Equals("Austin Davis") && address != null && address.Length > 0)
                        message.To.Add(new System.Net.Mail.MailAddress(address, name));
                }
                break;
            case "Andrew Wang (Vice President)":
                for (int i = 0; i < table.Rows.Count; i++) // 0 Name, 1 Address
                {
                    string name = table.Rows[i].ItemArray[0].ToString().Trim();
                    string address = table.Rows[i].ItemArray[1].ToString().Trim();
                    if (name.Equals("Andrew Wang") && address != null && address.Length > 0)
                        message.To.Add(new System.Net.Mail.MailAddress(address, name));
                }
                break;
            case "Sophia Wang (Treasurer)":
                for (int i = 0; i < table.Rows.Count; i++) // 0 Name, 1 Address
                {
                    string name = table.Rows[i].ItemArray[0].ToString().Trim();
                    string address = table.Rows[i].ItemArray[1].ToString().Trim();
                    if (name.Equals("Sophia Wang") && address != null && address.Length > 0)
                        message.To.Add(new System.Net.Mail.MailAddress(address, name));
                }
                break;
            case "Emily Zhang (Secretary)":
                for (int i = 0; i < table.Rows.Count; i++) // 0 Name, 1 Address
                {
                    string name = table.Rows[i].ItemArray[0].ToString().Trim();
                    string address = table.Rows[i].ItemArray[1].ToString().Trim();
                    if (name.Equals("Emily Zhang") && address != null && address.Length > 0)
                        message.To.Add(new System.Net.Mail.MailAddress(address, name));
                }
                break;
            case "Brian Liou (Webmaster)":
                for (int i = 0; i < table.Rows.Count; i++) // 0 Name, 1 Address
                {
                    string name = table.Rows[i].ItemArray[0].ToString().Trim();
                    string address = table.Rows[i].ItemArray[1].ToString().Trim();
                    if (name.Equals("Brian Liou") && address != null && address.Length > 0)
                        message.To.Add(new System.Net.Mail.MailAddress(address, name));
                }
                break;
            case "Mr. Nonis (Advisor)":
                message.To.Add(new System.Net.Mail.MailAddress("nonisj@bsd405.org", "Mr. Nonis"));
                break;
            case "Mr. Vu (Advisor)":
                message.To.Add(new System.Net.Mail.MailAddress("vuk@bsd405.org", "Mr. Vu"));
                break;
            case "Mr. Bennett Haselton (Coach)":
                message.To.Add(new System.Net.Mail.MailAddress("bennett@peacefire.org", "Bennett Haselton"));
                break;
        }
        message.Subject = SubjectBox.Text;
        if (!message.Subject.Contains("[Math Club]"))
            message.Subject = "[Math Club] " + message.Subject;
        message.Body = MessageBox.Text;
        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
        //smtp.EnableSsl = true;
        if (message.To.Count > 0)
        {
            try
            {
                smtp.Send(message);
            }
            catch (Exception)
            {
                ShowAlert("An error occured sending the email. Please contact the website administrator.");
            }
            if (message.To.Count == 1)
                ShowAlert("Message sent successfully to " + message.To[0].DisplayName + ".");
            else
                ShowAlert("Message sent successfully to " + message.To.Count + " recipients.");
        }
        else
            ShowAlert("Error: None of the chosen recepients have an address in the database.");
    }
    public static void ShowAlert(string message)
    {
        // Cleans the message to allow single quotation marks
        string cleanMessage = message.Replace("'", "\\'");
        string script = "<script type=\"text/javascript\">alert('" + cleanMessage;
        script += "');<";
        script += "/script>";

        // Gets the executing web page
        Page page = HttpContext.Current.CurrentHandler as Page;

        // Checks if the handler is a Page and that the script isn't allready on the Page
        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
        {
            page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", script);
        }
    }
}