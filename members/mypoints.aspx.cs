﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class members_mypoints : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        nameLabel.Text = User.Identity.Name;

        // Create label with pi symbol for each pi point the member has earned!
        // 0 ID, 1 Date, 2 Item, 3 Points
        int points = 0;
        for (int i = 0; i < DetailedPointsGrid.Rows.Count; i++)
        {
            string s = DetailedPointsGrid.Rows[i].Cells[3].Text;
            if (s == null || s == "")
                break;
            int dPoints = Int32.Parse(s);
            points += dPoints;
        }
        PiLabel.Text = "";
        for (int i = 0; i < points; i++)
        {
            PiLabel.Text += "&pi; ";
        }
        TotalPointsLabel.Text = points.ToString();
    }

    protected void YearlySummaryGrid_DataBound(object sender, EventArgs e)
    {
        // Hide name in yearly summary
        if (YearlySummaryGrid.HeaderRow.Cells.Count > 0)
            YearlySummaryGrid.HeaderRow.Cells[0].Visible = false;
        if (YearlySummaryGrid.Rows.Count > 0)
        {
            YearlySummaryGrid.Rows[0].Cells[0].Visible = false;
            // Hide any years with no points, which will show as cells with a nonbreaking space.
            for (int i = 0; i < YearlySummaryGrid.HeaderRow.Cells.Count; i++)
                if (YearlySummaryGrid.Rows[0].Cells[i].Text.Equals("&nbsp;"))
                {
                    YearlySummaryGrid.HeaderRow.Cells[i].Visible = false;
                    YearlySummaryGrid.Rows[0].Cells[i].Visible = false;
                }
        }

    }
}