﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class members_mycompetitions : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void CompetitionsData_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["Member"].Value = User.Identity.Name;
    }

    protected void EligibilityData_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["Member"].Value = User.Identity.Name;
    }

    protected void TeamsData_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["Member"].Value = User.Identity.Name;
    }

    protected void TransportationData_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["Member"].Value = User.Identity.Name;
    }

    protected void CompetitionsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CompetitionsGrid.SelectedIndex >= 0 && CompetitionsGrid.SelectedIndex < CompetitionsGrid.Rows.Count)
        {
            CompetitionPanel.Visible = true;
            if (!CompetitionDetailsPanel.Visible)
                btnShow.Visible = true;
        }
        else
        {
            CompetitionPanel.Visible = false;
            btnShow.Visible = false;
        }

        // Show eligibility check or x and message
        EligibilityDetails.DataBind();
        bool permission = (EligibilityDetails.Rows[1].Cells[1].Controls[0] as CheckBox).Checked;
        bool paid = (EligibilityDetails.Rows[2].Cells[1].Controls[0] as CheckBox).Checked;
        if (permission && paid)
        {
            EligiblePanel.Visible = true;
            NotEligiblePanel.Visible = false;
        }
        else
        {
            EligiblePanel.Visible = false;
            NotEligiblePanel.Visible = true;
        }
    }

    protected void TeamsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (TeamsGrid.SelectedIndex >= 0 && TeamsGrid.SelectedIndex < TeamsGrid.Rows.Count)
            TeamMembersPanel.Visible = true;
        else
            TeamMembersPanel.Visible = false;
    }

    protected void btnShow_Click(object sender, EventArgs e)
    {
        CompetitionDetailsPanel.Visible = true;
        btnShow.Visible = false;
    }

    protected void btnHide_Click(object sender, EventArgs e)
    {
        CompetitionDetailsPanel.Visible = false;
        btnShow.Visible = true;
    }
}