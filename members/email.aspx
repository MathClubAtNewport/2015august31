﻿<%@ Page Title="Newport Math Club &ndash; Send an E-mail" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="email.aspx.cs" Inherits="members_email" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <asp:AccessDataSource ID="MembersDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT [Name], [Email] FROM [Members]" />
    <h2>
        Send an E-mail</h2>
    <p>
        This page allows you to send an e-mail to club officers or club members.</p>
    <table style="width: 100%;">
        <tr>
            <td valign="top">
                <h4>
                    From</h4>
                <asp:RadioButtonList ID="rdoFrom" runat="server" Visible="false">
                    <asp:ListItem Selected="True">Newport Math Club</asp:ListItem>
                    <asp:ListItem>Kevin Liu</asp:ListItem>
                    <asp:ListItem>Austin Davis</asp:ListItem>
                    <asp:ListItem>Andrew Wang</asp:ListItem>
                    <asp:ListItem>Pei Tao</asp:ListItem>
                    <asp:ListItem>Mr. Nonis</asp:ListItem>
                </asp:RadioButtonList>
                <asp:RadioButtonList ID="rdoFromNew" runat="server">
                    <asp:ListItem>Newport Math Club</asp:ListItem>
                    <asp:ListItem Selected="True">Me</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td valign="top">
                <h4>
                    To</h4>
                <p>
                    <asp:RadioButtonList ID="rdoTo" runat="server">
                        <asp:ListItem Selected="True">Newport Math Club</asp:ListItem>
                        <asp:ListItem Value="All Officers">All Officers &amp; Mr. Nonis</asp:ListItem>
                        <asp:ListItem Value="All Officers + Vu">All Officers &amp; Mr. Nonis &amp; Mr. Vu</asp:ListItem>
                        <asp:ListItem>All Members</asp:ListItem>
                        <asp:ListItem>Austin Davis (President)</asp:ListItem>
                        <asp:ListItem>Andrew Wang (Vice President)</asp:ListItem>
                        <asp:ListItem>Sophia Wang (Treasurer)</asp:ListItem>
                        <asp:ListItem>Emily Zhang (Secretary)</asp:ListItem>
                        <asp:ListItem>Brian Liou (Webmaster)</asp:ListItem>
                        <asp:ListItem>Mr. Nonis (Advisor)</asp:ListItem>
                        <asp:ListItem>Mr. Vu (Advisor)</asp:ListItem>
                        <asp:ListItem>Mr. Bennett Haselton (Coach)</asp:ListItem>
                    </asp:RadioButtonList>
                </p>
            </td>
        </tr>
    </table>
    <h4>
        Subject</h4>
    <p>
        <asp:TextBox ID="SubjectBox" runat="server" Width="100%"></asp:TextBox>
    </p>
    <h4>
        Message</h4>
    <p>
        <asp:TextBox ID="MessageBox" runat="server" Width="100%" TextMode="MultiLine" Height="187px"></asp:TextBox>
    </p>
    <p>
        <asp:Button ID="btnSend" runat="server" Text="Send" OnClick="btnSend_Click" />
    </p>
</asp:Content>
