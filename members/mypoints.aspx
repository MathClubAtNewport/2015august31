﻿<%@ Page Title="Newport Math Club &ndash; My Pi Points" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="mypoints.aspx.cs" Inherits="members_mypoints" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <asp:Label runat="server" ID="nameLabel" Visible="false" />
    <asp:AccessDataSource ID="PointsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT P.[ID], P.[Date], V.[Item], V.[Points] FROM [Points] P, [Point_Values] V WHERE V.[Code]=P.[Item] AND P.[Name]=? ORDER BY P.[Date] DESC">
        <SelectParameters>
            <asp:ControlParameter ControlID="nameLabel" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:AccessDataSource>
    <h2>
        My Pi Points</h2>
    <p>
        This page lists the Pi Points you have earned. Pi Points can be earned for participating
        in club activities, and members with high numbers of pi points may be eligible to
        earn a varsity letter. Click <a href="../pages/lettering.aspx">here</a> for more
        information on lettering criteria and a detailed list of how to earn pi points.</p>
    <p>
        <b>Total Pi Points:
            <asp:Label ID="TotalPointsLabel" runat="server"></asp:Label>
        </b>
    </p>
    <p>
        <asp:Label ID="PiLabel" runat="server"></asp:Label>
    </p>
    <h4>
        Yearly Summary</h4>
    <br />
    <asp:AccessDataSource ID="YearlySummaryDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT * FROM [Yearly Pi Points Crosstab Query] WHERE ([Name] = ?)">
        <SelectParameters>
            <asp:ControlParameter Name="Name" ControlID="nameLabel" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:GridView ID="YearlySummaryGrid" runat="server" CellPadding="4" DataSourceID="YearlySummaryDataSource"
        ForeColor="#333333" GridLines="None" OnDataBound="YearlySummaryGrid_DataBound">
        <AlternatingRowStyle BackColor="White" />
        <EmptyDataTemplate>
            No data to display.
        </EmptyDataTemplate>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
    </asp:GridView>
    <h4>
        Detailed Record</h4>
    <p>
        <asp:GridView ID="DetailedPointsGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            CellPadding="4" DataSourceID="PointsData" ForeColor="#333333" GridLines="None">
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" SortExpression="ID"
                    Visible="False" />
                <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" DataFormatString="{0:ddd. M/d/yy}" />
                <asp:BoundField DataField="Item" HeaderText="Item" SortExpression="Item" />
                <asp:BoundField DataField="Points" HeaderText="Points" SortExpression="Points" />
            </Columns>
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </p>
    <br />
</asp:Content>
