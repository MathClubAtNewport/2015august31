﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class officers_members : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    
    protected void MemberDetailsForm_DataBound(object sender, EventArgs e)
    {
        MembersGrid.DataBind();
    }

    protected void DataBind(object sender, EventArgs e)
    {
        MemberDetailsForm.DataBind();
    }
}