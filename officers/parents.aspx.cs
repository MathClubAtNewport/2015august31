﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class officers_parents : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void InsertButton_Click(object sender, EventArgs e)
    {
        try
        {
            ParentsForm.InsertItem(true);
            ErrorLabel.Visible = false;
        }
        catch (Exception ex)
        {
            if (ex.Message.Contains("related record"))
            {
                try
                {
                    ErrorLabel.Text = "* Error: Member \"" + (ParentsForm.FindControl("MemberTextBox") as TextBox).Text + "\" not found.";
                }
                catch (Exception)
                {
                    ErrorLabel.Text = "* Error: Member not found.";
                }
                ErrorLabel.Visible = true;
            }
            else
                ShowAlert("An error has occured. " + ex.Message + " Contact an administrator for more help.");
        }
    }

    public static void ShowAlert(string message)
    {
        Page page = HttpContext.Current.CurrentHandler as Page;
        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
            page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "<script type=\"text/javascript\">alert('" + message.Replace("'", "\\'") + "');<" + "/script>");
    }

    protected void ParentsForm_DataBound(object sender, EventArgs e)
    {
        ParentsGrid.DataBind();
    }
}