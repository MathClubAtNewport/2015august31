﻿<%@ Page Title="Competitors &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="competitors.aspx.cs" Inherits="officers_competitors" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <asp:AccessDataSource ID="CompetitionsDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT [ID], [Name], [Date], [StartTime] FROM [Competitions] ORDER BY [Date] DESC">
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="CompetitorsDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT [ID], [Member], [Permission], [Paid] FROM [Competitors] WHERE ([Competition] = ?) ORDER BY [Member]"
        DeleteCommand="DELETE FROM [Competitors] WHERE [ID] = ?" UpdateCommand="UPDATE [Competitors] SET [Permission]=?, [Paid]=? WHERE [ID] = ?"
        InsertCommand="INSERT INTO [Competitors]([Competition], [Member], [Permission], [Paid]) Values(?,?,?,?)">
        <SelectParameters>
            <asp:ControlParameter ControlID="CompetitionsGrid" Name="Competition" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="CompetitionsGrid" Name="Competition" PropertyName="SelectedValue"
                Type="Int32" />
            <asp:ControlParameter ControlID="AddCompetitorForm$MemberDropDownList" Name="Member"
                PropertyName="SelectedValue" Type="String" />
        </InsertParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="MembersDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT [Name], [Grade], [Class] FROM [Members] ORDER BY [Name]">
    </asp:AccessDataSource>
    <h2>
        Manage Competitors</h2>
    <p>
        This page allows you to manage which members are competing at each competition.</p>
    <h3>
        Competitions</h3>
        <br />
    <asp:GridView ID="CompetitionsGrid" runat="server" AllowPaging="True" AllowSorting="True"
        CellPadding="4" DataSourceID="CompetitionsDataSource" ForeColor="#333333" GridLines="None"
        AutoGenerateColumns="False" DataKeyNames="ID" ToolTip="Select a Competition"
        OnSelectedIndexChanged="CompetitionsGrid_SelectedIndexChanged">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                SortExpression="ID" Visible="False" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:BoundField DataField="Date" DataFormatString="{0:M/d/yy}" HeaderText="Date"
                SortExpression="Date" />
            <asp:BoundField DataField="StartTime" DataFormatString="{0:h:mm tt}" HeaderText="Start Time"
                SortExpression="StartTime" />
        </Columns>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <br />
    <asp:Panel ID="CompetitorsPanel" runat="server" Visible="False">
        <table id="CompetitorsTable" style="width: 100%;">
            <tr>
                <td valign="top">
                    <h4>
                        Competitors</h4>
                    <asp:GridView ID="CompetitorsGrid" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="CompetitorsDataSource"
                        ForeColor="#333333" GridLines="None">
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <Columns>
                            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                SortExpression="ID" Visible="False" />
                            <asp:BoundField DataField="Member" HeaderText="Member" ReadOnly="true" SortExpression="Member" />
                            <asp:CheckBoxField DataField="Permission" HeaderText="Permission" SortExpression="Permission" />
                            <asp:CheckBoxField DataField="Paid" HeaderText="Paid" SortExpression="Paid" />
                        </Columns>
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </td>
                <td valign="top">
                    <h4>
                        Add A Member</h4>
                    <p>
                        <asp:FormView ID="AddCompetitorForm" runat="server" CellPadding="4" DataKeyNames="ID"
                            DataSourceID="CompetitorsDataSource" DefaultMode="Insert" ForeColor="#333333"
                            OnDataBound="AddCompetitorForm_DataBound">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <InsertItemTemplate>
                                Member:
                                <asp:DropDownList ID="MemberDropDownList" runat="server" DataSourceID="MembersDataSource"
                                    DataTextField="Name" />
                                <br />
                                Permission:
                                <asp:CheckBox ID="PermissionCheckBox" runat="server" Checked='<%# Bind("Permission") %>' />
                                <br />
                                Paid:
                                <asp:CheckBox ID="PaidCheckBox" runat="server" Checked='<%# Bind("Paid") %>' />
                                <br />
                                <br />
                                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                                    Text="Add Member to Competition" />
                            </InsertItemTemplate>
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        </asp:FormView>
                    </p>
                </td>
            </tr>
        </table>
        <hr />
    </asp:Panel>
</asp:Content>
