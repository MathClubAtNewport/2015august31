﻿<%@ Page Title="Newport Math Challenge &ndash; Administration" Language="C#" MasterPageFile="~/Site.master" %>

<script runat="server">

</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Newport Math Challenge - Administration</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <h2>
        <asp:Image ID="ChallengeImage" runat="server" Height="100px" ImageAlign="Right" ImageUrl="~/images/Challenge2.png" />
        Newport Math Challenge - Administration</h2>
    <p>
        This page allows you to edit the questions for the Newport Math Challenge.</p>
    <p>
        <asp:AccessDataSource ID="QuestionsData" runat="server" DataFile="~/App_Data/MathChallenge.mdb"
            DeleteCommand="DELETE FROM [Questions] WHERE [ID] = ?" InsertCommand="INSERT INTO [Questions] ([ID], [Category], [CorrectAnswer], [PointValue]) VALUES (?, ?, ?, ?)"
            SelectCommand="SELECT * FROM [Questions] ORDER BY [ID]" UpdateCommand="UPDATE [Questions] SET [Category] = ?, [CorrectAnswer] = ?, [PointValue] = ? WHERE [ID] = ?">
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Category" Type="String" />
                <asp:Parameter Name="CorrectAnswer" Type="Int32" />
                <asp:Parameter Name="PointValue" Type="Int32" />
                <asp:Parameter Name="ID" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="ID" Type="Int32" />
                <asp:Parameter Name="Category" Type="String" />
                <asp:Parameter Name="CorrectAnswer" Type="Int32" />
                <asp:Parameter Name="PointValue" Type="Int32" />
            </InsertParameters>
        </asp:AccessDataSource>
    </p>
    <h3>
        Questions</h3>
    <asp:GridView ID="QuestionsGrid" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataSourceID="QuestionsData" ForeColor="#333333"
        GridLines="None" PageSize="25" DataKeyNames="ID">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:CommandField ShowEditButton="True" />
            <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
            <asp:BoundField DataField="Category" HeaderText="Category" SortExpression="Category" />
            <asp:BoundField DataField="CorrectAnswer" HeaderText="CorrectAnswer" SortExpression="CorrectAnswer" />
            <asp:BoundField DataField="PointValue" HeaderText="PointValue" SortExpression="PointValue" />
        </Columns>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>
