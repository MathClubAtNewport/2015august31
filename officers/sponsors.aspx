<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void SponsorsForm_DataBound(object sender, EventArgs e)
    {
        SponsorsGrid.DataBind();
    }
</script>

<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml">

<!-- #BeginTemplate "../master.dwt" -->

<head>
<script type="text/javascript">
window.google_analytics_uacct = "UA-4106925-2";
</script>

<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<!-- #BeginEditable "doctitle" -->
<title>Newport Math Club - Sponsors</title>
    <style type="text/css">






































































































































        .style5
        {
            width: 100%;
        }
    </style>
<!-- #EndEditable -->
<link href="../styles/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form runat="server" name="theForm">

<!-- Begin Container -->
<div id="container">
	<!-- Begin Masthead -->
	<div id="masthead">
		<a href="../default.aspx">
		<img alt="Newport Math Club" src="../images/MathClubBanner.png" height="160" width="1000" /></a>
	</div>
	<!-- End Masthead -->
	<!-- Begin Navigation -->
	<div class="dropdown">
		<ul>
			<li><a href="../default.aspx">Home</a></li>
			<li><a href="../pages/about.aspx">About</a>
				<ul>
					<li><a href="../pages/about.aspx">About the Club</a></li>
					<li><a href="../pages/achievements.aspx">Achievements</a></li>
					<li><a href="../pages/officers.aspx">Officers</a></li>
					<li><a href="../pages/sponsors.aspx">Sponsors</a></li>
				</ul>
			</li>
			<li><p>Events</p>
				<ul>
					<li><a href="../pages/meetings.aspx">Meetings</a></li>
					<li><a href="../pages/competitions.aspx">Upcoming Competitions</a></li>
				</ul>
			</li>
			<li><a href="../pages/contests.aspx">Contests We Host</a>
				<ul>
					<li><a href="../pages/contests.aspx">List of Contests</a></li>
					<li><a href="../kpmt/default.aspx">KPMT</a></li>
				</ul>
			</li>
			<li><p>Resources</p>
				<ul>
					<li><a href="../pages/articles.aspx">Articles</a></li>
					<li><a href="../pages/practice.aspx">Practice</a></li>
					<li><a href="../pages/links.aspx">Links</a></li>
				</ul>
			</li>
			<li><p>Members</p>
				<ul>
					<asp:loginview id="MembersMenuLoginView" runat="server">
						<RoleGroups>
							<asp:RoleGroup Roles="Members">
								<ContentTemplate>
									<li><a href="../members/mymeetings.aspx">My Meetings</a></li>
									<li>
									<a href="../members/mycompetitions.aspx">My Competitions</a></li>
									<li><a href="../members/mypoints.aspx">My Pi Points</a></li>
									<li><a href="../members/myawards.aspx">My Awards</a></li>
									<li><a href="../members/email.aspx">Send E-mail</a></li>
									<li>
									<a href="../members/changepassword.aspx">Change Password</a></li>
								</ContentTemplate>
							</asp:RoleGroup>
						</RoleGroups>
					</asp:loginview>
					<li><asp:LoginStatus runat="server"></asp:LoginStatus></li>					
				</ul>
			</li>
			<asp:loginview id="AdminsMenuLoginView" runat="server">
				<RoleGroups>
					<asp:rolegroup Roles="Officers, Admins">
						<ContentTemplate>
							<li><p>Administration</p>
								<ul>
									<li><a href="meetings.aspx">Meetings</a></li>
									<li><a href="attendance.aspx">Attendance</a></li>
									<li><a href="competitions.aspx">Contests</a></li>
									<li><a href="competitors.aspx">Competitors</a></li>
									<li><a href="teams.aspx">Teams</a></li>
									<li><a href="transportation.aspx">Transportation</a></li>
									<li><a href="awards.aspx">Awards</a></li>
									<li><a href="points.aspx">Pi Points</a></li>
									<li><a href="members.aspx">Members</a></li>
									<li><a href="parents.aspx">Parents</a></li>
									<li><a href="sponsors.aspx">Sponsors</a></li>
									<asp:loginview id="LoginViewAdmins_Users" runat="server">
										<RoleGroups>
											<asp:rolegroup Roles="Admins">
												<ContentTemplate>
													<li>
													<a href="../admin/users.aspx">Users</a></li>
													<li>
													<a href="../admin/classes_teachers.aspx">Classes &amp; Teachers</a></li>
												</ContentTemplate>
											</asp:rolegroup>						
										</RoleGroups>
									</asp:loginview>
									<li><a href="reports/reports.aspx">Reports</a>
										<ul>
											<li>
											<a href="reports/report-points.aspx">Pi Points</a></li>
										</ul>
									</li>
								</ul>
							</li>	
						</ContentTemplate>
					</asp:rolegroup>
				</RoleGroups>
			</asp:loginview>
		</ul>
		
		<div class="searchbox">
			<script type="text/javascript" src="http://www.google.com/cse/brand?form=cse-search-box&amp;lang=en">
			</script>
			<script language="javascript" type="text/javascript">
		        function GoogleSiteSerach() {
		            document.location.href = 'http://www.newportmathclub.org/pages/search.aspx?cx=partner-pub-8846177013257426:k2c5vu-fqre&cof=FORID:9&ie=ISO-8859-1&q=' + document.getElementById('q').value + '&sa=Search';
		        }
			</script>
			<script language="javascript" type="text/javascript">
			function clickButton(e, buttonid){ 
	      		var evt = e ? e : window.event;
	     		var bt = document.getElementById(buttonid);
			    if (bt){ 
			        if (evt.keyCode == 13){ 
			              bt.click(); 
			              return false; 
			        } 
		    	} 
			}
			</script>	
			<input type="text" name="q" size="30" id="q" onkeypress="return clickButton(event,'sa')" />
			<input type="button" onclick="JavaScript:GoogleSiteSerach();" name="sa" value="Search" id="sa" />
		</div>
		
	</div>
	<!-- End Navigation -->
	<!-- Begin Page Content -->
	<div id="page_content">
		<!-- Begin Content -->
		<div id="content">
			<!-- #BeginEditable "content" -->
			<h2>
                <asp:AccessDataSource ID="SponsorsData" runat="server" 
                    DataFile="~/App_Data/MathClub.mdb" 
                    SelectCommand="SELECT [ID], [Sponsor], [Level] FROM [Sponsors] ORDER BY [Donation] DESC">
                </asp:AccessDataSource>
                <asp:AccessDataSource ID="SponsorDetailsData" runat="server" 
                    DataFile="~/App_Data/MathClub.mdb" 
                    
                    SelectCommand="SELECT * FROM [Sponsors] WHERE ([ID] = ?) ORDER BY [Donation] DESC" 
                    DeleteCommand="DELETE FROM [Sponsors] WHERE [ID] = ?" 
                    InsertCommand="INSERT INTO [Sponsors] ([Sponsor], [Contact], [Phone], [Email], [Donation], [Level]) VALUES (?, ?, ?, ?, ?, ?)" 
                    UpdateCommand="UPDATE [Sponsors] SET [Sponsor] = ?, [Contact] = ?, [Phone] = ?, [Email] = ?, [Donation] = ?, [Level] = ? WHERE [ID] = ?">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="SponsorsGrid" Name="ID" 
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="ID" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Sponsor" Type="String" />
                        <asp:Parameter Name="Contact" Type="String" />
                        <asp:Parameter Name="Phone" Type="String" />
                        <asp:Parameter Name="Email" Type="String" />
                        <asp:Parameter Name="Donation" Type="Decimal" />
                        <asp:Parameter Name="Level" Type="String" />
                        <asp:Parameter Name="ID" Type="Int32" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Sponsor" Type="String" />
                        <asp:Parameter Name="Contact" Type="String" />
                        <asp:Parameter Name="Phone" Type="String" />
                        <asp:Parameter Name="Email" Type="String" />
                        <asp:Parameter Name="Donation" Type="Decimal" />
                        <asp:Parameter Name="Level" Type="String" />
                    </InsertParameters>
                </asp:AccessDataSource>
                Sponsors</h2>
			<p>This page allows you to view and manage our club sponsors.</p>
            <table class="style5">
                <tr>
                    <td valign="top">
                        <asp:GridView ID="SponsorsGrid" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                            CellPadding="4" DataKeyNames="ID" DataSourceID="SponsorsData" ForeColor="#333333" GridLines="None">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                                    ReadOnly="True" SortExpression="ID" Visible="False" />
                                <asp:BoundField DataField="Sponsor" HeaderText="Sponsor" 
                                    SortExpression="Sponsor" />
                                <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
                            </Columns>
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
                    <td valign="top">
                        <asp:FormView ID="SponsorsForm" runat="server" CellPadding="4" DataKeyNames="ID" 
                            DataSourceID="SponsorDetailsData" ForeColor="#333333" OnDataBound="SponsorsForm_DataBound">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <EditItemTemplate>
                                <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' Visible="false"/>
                                Sponsor:
                                <asp:TextBox ID="SponsorTextBox" runat="server" Text='<%# Bind("Sponsor") %>' />
                                <br />
                                Contact:
                                <asp:TextBox ID="ContactTextBox" runat="server" Text='<%# Bind("Contact") %>' />
                                <br />
                                Phone:
                                <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' />
                                <br />
                                Email:
                                <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                                <br />
                                Donation: $
                                <asp:TextBox ID="DonationTextBox" runat="server" 
                                    Text='<%# Bind("Donation") %>' />
                                <br />
                                Level:
                                <asp:TextBox ID="LevelTextBox" runat="server" Text='<%# Bind("Level") %>' />
                                <br />
                                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                                    CommandName="Update" Text="Update" />
                                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                Sponsor:
                                <asp:TextBox ID="SponsorTextBox0" runat="server" Text='<%# Bind("Sponsor") %>' />
                                <br />
                                Contact:
                                <asp:TextBox ID="ContactTextBox0" runat="server" Text='<%# Bind("Contact") %>' />
                                <br />
                                Phone:
                                <asp:TextBox ID="PhoneTextBox0" runat="server" Text='<%# Bind("Phone") %>' />
                                <br />
                                Email:
                                <asp:TextBox ID="EmailTextBox0" runat="server" Text='<%# Bind("Email") %>' />
                                <br />
                                Donation:
                                <asp:TextBox ID="DonationTextBox0" runat="server" 
                                    Text='<%# Bind("Donation", "{0:c}") %>' />
                                <br />
                                Level:
                                <asp:TextBox ID="LevelTextBox0" runat="server" Text='<%# Bind("Level") %>' />
                                <br />
                                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                                    CommandName="Insert" Text="Insert" />
                                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' Visible="false"/>
                                Sponsor:
                                <asp:Label ID="SponsorLabel" runat="server" Text='<%# Bind("Sponsor") %>' />
                                <br />
                                Contact:
                                <asp:Label ID="ContactLabel" runat="server" Text='<%# Bind("Contact") %>' />
                                <br />
                                Phone:
                                <asp:Label ID="PhoneLabel" runat="server" Text='<%# Bind("Phone") %>' />
                                <br />
                                Email:
                                <asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' />
                                <br />
                                Donation:
                                <asp:Label ID="DonationLabel" runat="server" Text='<%# Bind("Donation", "{0:c}") %>' />
                                <br />
                                Level:
                                <asp:Label ID="LevelLabel" runat="server" Text='<%# Bind("Level") %>' />
                                <br />
                                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                                    CommandName="Edit" Text="Edit" />
                                &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                                    CommandName="Delete" Text="Delete" />
                                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                                    CommandName="New" Text="New" />
                            </ItemTemplate>
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        </asp:FormView>
                    </td>
                </tr>
            </table>
			<!-- #EndEditable -->
			
			<br/>
			<script type="text/javascript"><!--
				google_ad_client = "pub-8846177013257426";
				/* Newport Math Club Bottom Leaderboard */
				google_ad_slot = "0637970847";
				google_ad_width = 728;
				google_ad_height = 90;
			//-->
			</script>
			<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script>
		</div>
		<!-- End Content -->
		<!-- Begin Sidebar -->
		<div id="sidebar">
			<asp:LoginView id="LoginView" runat="server">
				<AnonymousTemplate>
					<!--<asp:LoginStatus id="LoginStatus1" runat="server" />-->
				</AnonymousTemplate>
				<LoggedInTemplate>
					<div id="user">
					<p><asp:LoginName runat="server" id="LoginName1"></asp:LoginName>
					<span class="float_right"><asp:loginstatus id="LoginStatus1" runat="server" />&nbsp;</span></p>
					</div>
				</LoggedInTemplate>
			</asp:LoginView>
			<h3 style="margin-bottom:5px">2009-10 Sponsors</h3>
				<a href="http://www.tecplot.com/" target="_new_sponsors">
				<img alt="Tecplot" src="../images/sponsors/Tecplot.jpg" /></a>
				<a href="http://www.knowledgepoints.com/seattle" target="_new_sponsors">
				<img alt="KnowledgePoints" src="../images/sponsors/KnowledgePoints.jpg" /></a>
				<br/>
			<asp:LoginView ID="LoginViewAds" runat="server">
				<AnonymousTemplate>
					Place your company<br/>logo here! 
					<a href="../pages/sponsors.aspx">Learn more &raquo;</a>
					<h3 style="margin-bottom:5px">Sponsored Links</h3>
					<script type="text/javascript">
					google_ad_client = "pub-8846177013257426";
					// large - Newport Math Club Right Wide Skyscraper
					google_ad_slot = "9208928404";
					google_ad_width = 160;
					google_ad_height = 600;
					</script>
					<script type="text/javascript"
					src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
					</script>
				</AnonymousTemplate>
				<LoggedInTemplate>
					<h3>Sponsored Links</h3>
					<script type="text/javascript">
						// small
						google_ad_client = "pub-8846177013257426";
						google_ad_slot = "0880078497";
						google_ad_width = 120;
						google_ad_height = 240;
					</script>
					<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
					</script>
				</LoggedInTemplate>
			</asp:LoginView>
			</div>
			<!-- End Sidebar -->
		</div>
		<!-- End Page Content -->
	</div>
	<!-- End Container -->

	<!-- Begin Footer -->
	<div id="footer">
		<span class="footer-left">
			Copyright &copy; 2008-2009 by Newport Math Club
		</span>
		<span class="footer-right">
			<a href="../pages/about.aspx">About Us</a> |
			<a href="../pages/contests.aspx">Our Contests</a> |
			<a href="../pages/officers.aspx">Contact Us</a>
		</span>
	</div>
	<!-- End Footer -->
</form>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-4106925-2");
pageTracker._trackPageview();
} catch(err) {}</script>

</body>

<!-- #EndTemplate -->

</html>
