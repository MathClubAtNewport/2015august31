﻿<%@ Page Title="Members &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="members.aspx.cs" Inherits="officers_members" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Member List</h2>
    <p>
        <asp:AccessDataSource ID="Classes" runat="server" DataFile="~/App_Data/MathClub.mdb"
            SelectCommand="SELECT [Class Name] AS Class_Name FROM [Classes]"></asp:AccessDataSource>
        <asp:AccessDataSource ID="Teachers" runat="server" DataFile="~/App_Data/MathClub.mdb"
            SelectCommand="SELECT [Teacher Name] AS Teacher_Name FROM [Teachers]"></asp:AccessDataSource>
        <asp:AccessDataSource ID="MembersDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
            SelectCommand="SELECT [Name], [Grade], [Email] FROM [Members] WHERE (([Active] = true OR ? = true)
			    AND ([Member] = true OR ? = true) AND (NOT [Grade] = 13)) OR ([Grade] = 13 AND ? = true) ORDER BY [Name]">
            <SelectParameters>
                <asp:ControlParameter Name="ShowInactive" Type="Boolean" ControlID="ShowInactiveCheckBox"
                    PropertyName="Checked" />
                <asp:ControlParameter Name="ShowNonMember" Type="Boolean" ControlID="ShowNonMemberCheckBox"
                    PropertyName="Checked" />
                <asp:ControlParameter Name="ShowGraduated" Type="Boolean" ControlID="ShowGraduatedCheckBox"
                    PropertyName="Checked" />
            </SelectParameters>
        </asp:AccessDataSource>
        <asp:AccessDataSource ID="MemberDetailsDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
            SelectCommand="SELECT * FROM [Members] WHERE ([Name] = ?)" InsertCommand="INSERT INTO [Members]([Name], [Email], [Grade], [PastYears], [Active], [Member], [Class], [Teacher], [Period], [HomePhone],
			[CellPhone], [SchedulingConcerns], [WebPhotoRelease], [Strengths], [Weaknesses], [PresentationTopics])
			Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" DeleteCommand="DELETE FROM [Members] WHERE [Name] = ?"
            UpdateCommand="UPDATE [Members] SET [Email]=?, [Grade]=?, [PastYears]=?, [Active]=?, [Member]=?, [Class]=?, [Teacher]=?, [Period]=?, [HomePhone]=?,
			[CellPhone]=?, [SchedulingConcerns]=?, [WebPhotoRelease]=?, [Strengths]=?, [Weaknesses]=?, [PresentationTopics]=?, [LastActiveYear]=? WHERE [Name] = ?">
            <InsertParameters>
                <asp:Parameter Name="[Name]" Type="String" />
                <asp:Parameter Name="[Email]" Type="String" />
                <asp:Parameter Name="[Grade]" Type="Int32" />
                <asp:Parameter Name="[PastYears]" Type="Int32" />
                <asp:Parameter Name="[Active]" Type="Boolean" />
                <asp:Parameter Name="[Member]" Type="Boolean" />
                <asp:ControlParameter Name="[Class]" ControlID="MemberDetailsForm$ClassList" PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter Name="[Teacher]" ControlID="MemberDetailsForm$TeacherList"
                    PropertyName="SelectedValue" Type="String" />
                <asp:Parameter Name="[Period]" Type="Int32" />
                <asp:Parameter Name="[HomePhone]" Type="String" />
                <asp:Parameter Name="[CellPhone]" Type="String" />
                <asp:Parameter Name="[SchedulingConcerns]" Type="String" />
                <asp:Parameter Name="[WebPhotoRelease]" Type="Boolean" />
                <asp:Parameter Name="[Strengths]" Type="String" />
                <asp:Parameter Name="[Weaknesses]" Type="String" />
                <asp:Parameter Name="[PresentationTopics]" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="MembersGrid" PropertyName="SelectedValue" />
            </SelectParameters>
            <DeleteParameters>
                <asp:ControlParameter ControlID="MembersGrid" PropertyName="SelectedValue" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="[Email]" Type="String" />
                <asp:Parameter Name="[Grade]" Type="Int32" />
                <asp:Parameter Name="[PastYears]" Type="Int32" />
                <asp:Parameter Name="[Active]" Type="Boolean" />
                <asp:Parameter Name="[Member]" Type="Boolean" />
                <asp:Parameter Name="[Class]" Type="String" ConvertEmptyStringToNull="true" />
                <asp:Parameter Name="[Teacher]" Type="String" ConvertEmptyStringToNull="true" />
                <asp:Parameter Name="[Period]" Type="Int32" />
                <asp:Parameter Name="[HomePhone]" Type="String" />
                <asp:Parameter Name="[CellPhone]" Type="String" />
                <asp:Parameter Name="[SchedulingConcerns]" Type="String" />
                <asp:Parameter Name="[WebPhotoRelease]" Type="Boolean" />
                <asp:Parameter Name="[Strengths]" Type="String" />
                <asp:Parameter Name="[Weaknesses]" Type="String" />
                <asp:Parameter Name="[PresentationTopics]" Type="String" />
                <asp:Parameter Name="[LastActiveYear]" Type="String" />
                <asp:ControlParameter ControlID="MembersGrid" PropertyName="SelectedValue" />
            </UpdateParameters>
        </asp:AccessDataSource>
        &nbsp;<asp:CheckBox ID="ShowInactiveCheckBox" runat="server" Text="Show Inactive"
            AutoPostBack="true" />
        &nbsp;<asp:CheckBox ID="ShowNonMemberCheckBox" runat="server" Text="Show Non-Members"
            AutoPostBack="true" />
        &nbsp;<asp:CheckBox ID="ShowGraduatedCheckBox" runat="server" Text="Show Graduated"
            AutoPostBack="true" />
        <asp:FormView ID="MemberDetailsForm" runat="server" OnItemUpdated="DataBind" OnDataBound="MemberDetailsForm_DataBound"
            DefaultMode="ReadOnly" CellPadding="4" DataKeyNames="Name" HeaderText="Member Details"
            DataSourceID="MemberDetailsDataSource" ForeColor="#333333">
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <EditItemTemplate>
                Name:
                <asp:Label ID="NameLabel1" runat="server" Text='<%# Eval("[Name]") %>' />
                <br />
                Email:
                <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("[Email]") %>' />
                <br />
                Grade:
                <asp:TextBox ID="GradeTextBox" runat="server" Text='<%# Bind("[Grade]") %>' />
                <br />
                Past Years:
                <asp:TextBox ID="PastYearsTextBox" runat="server" Text='<%# Bind("[PastYears]") %>' />
                <br />
                Active:
                <asp:CheckBox ID="ActiveCheckBox" runat="server" Checked='<%# Bind("[Active]") %>' />
                <br />
                Member:
                <asp:CheckBox ID="MemberCheckBox" runat="server" Checked='<%# Bind("[Member]") %>' />
                <br />
                Class:
                <asp:DropDownList ID="ClassList0" runat="server" DataSourceID="Classes" DataTextField="Class_Name"
                    DataValueField="Class_Name" SelectedValue='<%# Bind("[Class]") %>' />
                <br />
                Teacher:
                <asp:DropDownList ID="TeacherList0" runat="server" DataSourceID="Teachers" DataTextField="Teacher_Name"
                    DataValueField="Teacher_Name" SelectedValue='<%# Bind("[Teacher]") %>' />
                <br />
                Period:
                <asp:TextBox ID="PeriodTextBox" runat="server" Text='<%# Bind("[Period]") %>' />
                <br />
                Home Phone:
                <asp:TextBox ID="HomePhoneTextBox" runat="server" Text='<%# Bind("[HomePhone]") %>' />
                <br />
                Cell Phone:
                <asp:TextBox ID="CellPhoneTextBox" runat="server" Text='<%# Bind("[CellPhone]") %>' />
                <br />
                Scheduling Concerns:
                <asp:TextBox ID="SchedulingConcernsTextBox" runat="server" Text='<%# Bind("[SchedulingConcerns]") %>' />
                <br />
                Web Photo Release:
                <asp:CheckBox ID="WebPhotoReleaseCheckBox" runat="server" Checked='<%# Bind("[WebPhotoRelease]") %>' />
                <br />
                Areas of Strength:
                <asp:TextBox ID="StrengthsTextBox" runat="server" Text='<%# Bind("[Strengths]") %>' />
                <br />
                Areas Desiring to Improve:
                <asp:TextBox ID="WeaknessesTextBox" runat="server" Text='<%# Bind("[Weaknesses]") %>' />
                <br />
                Presentation Topics:
                <asp:TextBox ID="PresentationTopicsTextBox" runat="server" Text='<%# Bind("[PresentationTopics]") %>' />
                <br />
                Last Active Year:
                <asp:TextBox ID="LastActiveYearTextBox" runat="server" Text='<%# Bind("[LastActiveYear]") %>' />
                <br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                    Text="Update" />
                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False"
                    CommandName="Cancel" Text="Cancel" />
            </EditItemTemplate>
            <InsertItemTemplate>
                Name:
                <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("[Name]") %>' />
                <br />
                Email:
                <asp:TextBox ID="EmailTextBox0" runat="server" Text='<%# Bind("[Email]") %>' />
                <br />
                Grade:
                <asp:TextBox ID="GradeTextBox0" runat="server" Text='<%# Bind("[Grade]") %>' />
                <br />
                Past Years:
                <asp:TextBox ID="PastYearsTextBox0" runat="server" Text='<%# Bind("[PastYears]") %>' />
                <br />
                Active:
                <asp:CheckBox ID="ActiveCheckBox0" runat="server" Checked='<%# Bind("[Active]") %>' />
                <br />
                Member:
                <asp:CheckBox ID="MemberCheckBox0" runat="server" Checked='<%# Bind("[Member]") %>' />
                <br />
                Class:
                <asp:DropDownList ID="ClassList" runat="server" DataSourceID="Classes" DataTextField="Class_Name" />
                <br />
                Teacher:
                <asp:DropDownList ID="TeacherList" runat="server" DataSourceID="Teachers" DataTextField="Teacher_Name" />
                <br />
                Period:
                <asp:TextBox ID="PeriodTextBox0" runat="server" Text='<%# Bind("[Period]") %>' />
                <br />
                Home Phone:
                <asp:TextBox ID="HomePhoneTextBox0" runat="server" Text='<%# Bind("[HomePhone]") %>' />
                <br />
                Cell Phone:
                <asp:TextBox ID="CellPhoneTextBox0" runat="server" Text='<%# Bind("[CellPhone]") %>' />
                <br />
                Scheduling Concerns:
                <asp:TextBox ID="SchedulingConcernsTextBox0" runat="server" Text='<%# Bind("[SchedulingConcerns]") %>' />
                <br />
                Web Photo Release:
                <asp:CheckBox ID="WebPhotoReleaseCheckBox0" runat="server" Checked='<%# Bind("[WebPhotoRelease]") %>' />
                <br />
                Areas of Strength:
                <asp:TextBox ID="StrengthsTextBox0" runat="server" Text='<%# Bind("[Strengths]") %>' />
                <br />
                Areas Desiring to Improve:
                <asp:TextBox ID="WeaknessesTextBox0" runat="server" Text='<%# Bind("[Weaknesses]") %>' />
                <br />
                Presentation Topics:
                <asp:TextBox ID="PresentationTopicsTextBox0" runat="server" Text='<%# Bind("[PresentationTopics]") %>' />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                    Text="Insert" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False"
                    CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>
            <ItemTemplate>
                Name:
                <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("[Name]") %>' />
                <br />
                Email:
                <asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("[Email]") %>' />
                <br />
                Grade:
                <asp:Label ID="GradeLabel" runat="server" Text='<%# Bind("[Grade]") %>' />
                <br />
                Past Years:
                <asp:Label ID="PastYearsLabel" runat="server" Text='<%# Bind("[PastYears]") %>' />
                <br />
                Active:
                <asp:CheckBox ID="ActiveCheckBox1" runat="server" Enabled="false" Checked='<%# Bind("[Active]") %>' />
                <br />
                Member:
                <asp:CheckBox ID="MemberCheckBox1" runat="server" Enabled="false" Checked='<%# Bind("[Member]") %>' />
                <br />
                Class:
                <asp:Label ID="ClassLabel" runat="server" Text='<%# Bind("[Class]") %>' />
                <br />
                Teacher:
                <asp:Label ID="TeacherLabel" runat="server" Text='<%# Bind("[Teacher]") %>' />
                <br />
                Period:
                <asp:Label ID="PeriodLabel" runat="server" Text='<%# Bind("[Period]") %>' />
                <br />
                Home Phone:
                <asp:Label ID="HomePhoneLabel" runat="server" Text='<%# Bind("[HomePhone]") %>' />
                <br />
                Cell Phone:
                <asp:Label ID="CellPhoneLabel" runat="server" Text='<%# Bind("[CellPhone]") %>' />
                <br />
                Scheduling Concerns:
                <asp:Label ID="SchedulingConcernsLabel" runat="server" Text='<%# Bind("[SchedulingConcerns]") %>' />
                <br />
                Web Photo Release:
                <asp:CheckBox ID="WebPhotoReleaseCheckBox1" runat="server" Enabled="false" Checked='<%# Bind("[WebPhotoRelease]") %>' />
                <br />
                Areas of Strength:
                <asp:Label ID="StrengthsLabel" runat="server" Text='<%# Bind("[Strengths]") %>' />
                <br />
                Areas Desiring to Improve:
                <asp:Label ID="WeaknessesLabel" runat="server" Text='<%# Bind("[Weaknesses]") %>' />
                <br />
                Presentation Topics:
                <asp:Label ID="PresentationTopicsLabel" runat="server" Text='<%# Bind("[PresentationTopics]") %>' />
                <br />
                Last Active Year:
                <asp:Label ID="LastActiveYearLabel" runat="server" Text='<%# Bind("[LastActiveYear]") %>' />
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit"
                    Text="Edit" />
                &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete"
                    Text="Delete" />
                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New"
                    Text="New Member" />
            </ItemTemplate>
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        </asp:FormView>
    </p>
    <p>
        <asp:GridView ID="MembersGrid" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Name" DataSourceID="MembersDataSource"
            ForeColor="#333333" GridLines="None" PageSize="20">
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:CommandField ShowSelectButton="True" SelectText="Select"></asp:CommandField>
                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" SortExpression="Name">
                </asp:BoundField>
                <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email" NullDisplayText="[No Email Listed]"
                    SortExpression="Email"></asp:BoundField>
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </p>
</asp:Content>
