﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class officers_awards : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ShowHide();
    }

    private void ShowHide()
    {
        if (CompetitionsGrid.SelectedIndex < 0 || CompetitionsGrid.SelectedIndex >= CompetitionsGrid.Rows.Count)
        {
            TeamPanel.Visible = false;
            IndividualPanel.Visible = false;
            OrPanel.Visible = false;
        }
        else
        {
            TeamPanel.Visible = true;
            IndividualPanel.Visible = true;
            OrPanel.Visible = true;
        }
        TeamsGrid.DataBind();
        IndividualsGrid.DataBind();
        TeamAwardsGrid.DataBind();
        IndividualAwardsGrid.DataBind();

        if (TeamsGrid.Rows.Count > 0)
            NoTeamsLabel.Visible = false;
        else
            NoTeamsLabel.Visible = true;
        if (IndividualsGrid.Rows.Count > 0)
            NoIndividualsLabel.Visible = false;
        else
            NoIndividualsLabel.Visible = true;

        if (TeamsGrid.SelectedIndex < 0 || TeamsGrid.SelectedIndex >= TeamsGrid.Rows.Count)
            AddTeamAwardPanel.Visible = false;
        else
            AddTeamAwardPanel.Visible = true;
        if (IndividualsGrid.SelectedIndex < 0 || IndividualsGrid.SelectedIndex >= IndividualsGrid.Rows.Count)
            AddIndividualAwardPanel.Visible = false;
        else
            AddIndividualAwardPanel.Visible = true;

        if (TeamAwardsGrid.Rows.Count > 0)
            TeamAwardsPanel.Visible = true;
        else
            TeamAwardsPanel.Visible = false;
        if (IndividualAwardsGrid.Rows.Count > 0)
            IndividualAwardsPanel.Visible = true;
        else
            IndividualAwardsPanel.Visible = false;
    }

    protected void CompetitionsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        TeamsGrid.DataBind();
        IndividualsGrid.DataBind();
        TeamAwardsGrid.DataBind();
        IndividualAwardsGrid.DataBind();
        ShowHide();
    }

    protected void TeamsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (TeamsGrid.SelectedIndex >= 0)
            IndividualsGrid.SelectedIndex = -1;
        TeamAwardsGrid.DataBind();
        ShowHide();
    }

    protected void IndividualsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (IndividualsGrid.SelectedIndex >= 0)
            TeamsGrid.SelectedIndex = -1;
        IndividualAwardsGrid.DataBind();
        ShowHide();
    }

    protected void TeamAwardsData_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        TeamAwardsGrid.DataBind();
        ShowHide();
    }

    protected void IndividualAwardsData_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        IndividualAwardsGrid.DataBind();
        ShowHide();
    }

    protected void TeamAwardsData_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        ShowHide();
    }

    protected void IndividualAwardsData_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        ShowHide();
    }
}