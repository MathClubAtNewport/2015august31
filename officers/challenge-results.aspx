﻿<%@ Page Title="Results &ndash; Newport Math Challenge" Language="C#" MasterPageFile="~/Site.master" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
<h2>
                <asp:Image ID="ChallengeImage" runat="server" Height="125px" ImageAlign="Right" 
                    ImageUrl="~/images/Challenge2.png" />
                Newport Math Challenge - Results</h2>
			<p>This page lets you view the current results for the Newport Math Challenge.</p>
            <p>
                <asp:AccessDataSource ID="ResultsData" runat="server" 
                    DataFile="~/App_Data/MathChallenge.mdb" 
                    SelectCommand="SELECT [Name], [Grade], [Class], [Answered], [Score] FROM [Results] ORDER BY [Score] DESC, [Grade], [Answered]">
                </asp:AccessDataSource>
            </p>

                                                                <h3>Current Results            </h3>
                <asp:GridView ID="ResultsGrid" runat="server" AllowPaging="True" 
                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                    DataSourceID="ResultsData" ForeColor="#333333" GridLines="None" 
                    PageSize="25">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                        <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade" />
                        <asp:BoundField DataField="Class" HeaderText="Class" SortExpression="Class" />
                        <asp:BoundField DataField="Answered" HeaderText="Answered" 
                            SortExpression="Answered" />
                        <asp:BoundField DataField="Score" HeaderText="Score" SortExpression="Score" />
                    </Columns>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
</asp:Content>

