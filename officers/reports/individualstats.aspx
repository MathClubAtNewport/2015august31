﻿<%@ Page Title="Individual Stats &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Individual Stats</h2>
    <p>
        This reports shows summary stats for each member&#39;s participation in Math Club
        activities. Only members who have attended at least one meeting and one competition
        will display.</p>
    <asp:AccessDataSource ID="IndivStatsDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT * FROM [Individual Stats Query] WHERE (([Active] = true OR ? = true)
			    AND ([Member] = true OR ? = true) AND (NOT [Grade] = 13)) OR ([Grade] = 13 AND ? = true) ORDER BY [Sum Of Pi Points] DESC, [Competitions] DESC, [Meetings] DESC">
        <SelectParameters>
            <asp:ControlParameter Name="ShowInactive" Type="Boolean" ControlID="ShowInactiveCheckBox"
                PropertyName="Checked" />
            <asp:ControlParameter Name="ShowNonMember" Type="Boolean" ControlID="ShowNonMemberCheckBox"
                PropertyName="Checked" />
            <asp:ControlParameter Name="ShowGraduated" Type="Boolean" ControlID="ShowGraduatedCheckBox"
                PropertyName="Checked" />
        </SelectParameters>
    </asp:AccessDataSource>
    <p>
        &nbsp;<asp:CheckBox ID="ShowInactiveCheckBox" runat="server" Text="Show Inactive"
            AutoPostBack="true" />
        &nbsp;<asp:CheckBox ID="ShowNonMemberCheckBox" runat="server" Text="Show Non-Members"
            AutoPostBack="true" />
        &nbsp;<asp:CheckBox ID="ShowGraduatedCheckBox" runat="server" Text="Show Graduated"
            AutoPostBack="true" />
    </p>
    <asp:GridView runat="server" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
        DataKeyNames="Name" DataSourceID="IndivStatsDataSource" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" SortExpression="Name" />
            <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade" />
            <asp:BoundField DataField="PastYears" HeaderText="Past Years" SortExpression="PastYears"
                Visible="false" />
            <asp:BoundField DataField="Class" HeaderText="Class" SortExpression="Class" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" Visible="False" />
            <asp:BoundField DataField="Meetings" HeaderText="Meetings" SortExpression="Meetings" />
            <asp:BoundField DataField="Competitions" HeaderText="Competitions" SortExpression="Competitions" />
            <asp:BoundField DataField="Sum Of Pi Points" HeaderText="Pi Points" SortExpression="Sum Of Pi Points" />
            <asp:CheckBoxField DataField="Active" HeaderText="Active" SortExpression="Active"
                Visible="False" />
            <asp:CheckBoxField DataField="Member" HeaderText="Member" SortExpression="Member"
                Visible="False" />
        </Columns>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
    </asp:GridView>
</asp:Content>
