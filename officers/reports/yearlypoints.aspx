﻿<%@ Page Title="Yearly Pi Points Report &ndash; Newport Math Club" Language="C#"
    MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="yearlypoints.aspx.cs"
    Inherits="officers_reports_yearlypoints" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Yearly Pi Points Report</h2>
    <p>
        This report lists the total pi points each member earned in each academic year.</p>
    <asp:AccessDataSource ID="ReportDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT * FROM [Yearly Pi Points Crosstab Query] ORDER BY [11-12] DESC, [10-11] DESC, [09-10] DESC, [08-09] DESC">
    </asp:AccessDataSource>
    <asp:GridView ID="ReportGrid" runat="server" AllowSorting="True" CellPadding="4"
        DataSourceID="ReportDataSource" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
    </asp:GridView>
</asp:Content>
