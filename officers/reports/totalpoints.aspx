﻿<%@ Page Title="Total Pi Points Report &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <asp:AccessDataSource ID="Data" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT M.[Name], M.[Grade], SUM(V.Points) as PiPoints FROM [Members] M, [Points] P, [Point_Values] V WHERE P.[Name]=M.[Name] AND V.[Code]=P.[Item] GROUP BY M.[Name], M.[Grade]">
    </asp:AccessDataSource>
    <h2>
        Total Pi Points Report</h2>
    <p>
        This report shows the total pi points each member has earned.</p>
    <asp:GridView ID="ReportGrid" runat="server" CellPadding="4" DataSourceID="Data"
        ForeColor="#333333" GridLines="None" AllowSorting="True" AutoGenerateColumns="False"
        DataKeyNames="Name">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" SortExpression="Name" />
            <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade" />
            <asp:BoundField DataField="PiPoints" HeaderText="Pi Points" SortExpression="PiPoints" />
        </Columns>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>
