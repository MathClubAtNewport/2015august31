﻿<%@ Page Title="Reports &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Reports</h2>
    <p>
        This page lets you access various reports with information about the club.</p>
    <ul>
        <li><a href="memberlist.aspx">Member List</a></li>
        <li><a href="yearlypoints.aspx">Pi Points (Yearly)</a></li>
        <li><a href="totalpoints.aspx">Pi Points (Total)</a></li>
        <li><a href="individualstats.aspx">Individual Stats</a></li>
    </ul>
</asp:Content>
