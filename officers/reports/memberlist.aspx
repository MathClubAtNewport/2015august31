<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Newport Math Club - Member List</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Image ID="Title" runat="server" ImageUrl="~/images/title.png" />
        <br />
        <br />
    </div>
    <asp:GridView ID="Grid" runat="server" AllowSorting="True" 
        AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" 
        BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="Name" 
        DataSourceID="Data" Font-Names="Georgia">
        <FooterStyle BackColor="#FFFFCC" ForeColor="#000000" />
        <RowStyle BackColor="White" ForeColor="#000000" />
        <Columns>
            <asp:TemplateField HeaderText="Item 1"></asp:TemplateField>
            <asp:TemplateField HeaderText="Item 2"></asp:TemplateField>
            <asp:TemplateField HeaderText="Item 3"></asp:TemplateField>
            <asp:TemplateField HeaderText="Item 4"></asp:TemplateField>
            <asp:TemplateField HeaderText="Item 5"></asp:TemplateField>
            <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" 
                SortExpression="Name" />
            <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade" />
            <asp:BoundField DataField="Class" HeaderText="Class" SortExpression="Class" />
        </Columns>
        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
    </asp:GridView>
        <asp:AccessDataSource ID="Data" runat="server" DataFile="~/App_Data/MathClub.mdb" 
        SelectCommand="SELECT [Name], [Grade], [Class] FROM [Members] ORDER BY [Name]"></asp:AccessDataSource>
    </form>
</body>
</html>
