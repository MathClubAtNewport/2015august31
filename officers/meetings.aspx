﻿<%@ Page Title="Newport Math Club &ndash; Meeting Management" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Meeting Management</h2>
    <p>
        This page controls the list of math club meetings used for attendance purposes.</p>
    <asp:AccessDataSource runat="server" ID="MeetingsDataSource" DataFile="../App_Data/MathClub.mdb"
        SelectCommand="SELECT [ID], [Date], [Type], [Notes], (SELECT COUNT(*) FROM [Attendance] WHERE [Meeting] = [Meetings].[ID]) as membersCount FROM [Meetings] WHERE [Year] = ? ORDER BY [Date] DESC"
        InsertCommand="INSERT INTO [Meetings]([Date], [Type], [Notes]) Values(?,?,?)"
        DeleteCommand="DELETE FROM [Meetings] WHERE [ID] = ?" UpdateCommand="UPDATE [Meetings] Set [Date]=?, [Type]=?, [Notes]=? WHERE [ID]=?">
        <SelectParameters>
            <asp:ControlParameter ControlID="YearDropDownList" PropertyName="SelectedYear" Type="String"
                Name="Year" />
        </SelectParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="MeetingsForm$DateTextBox" Name="Date" />
            <asp:ControlParameter ControlID="MeetingsForm$TypeDropDownList" Name="Type" />
        </InsertParameters>
        <DeleteParameters>
            <asp:ControlParameter ControlID="MeetingsGrid" PropertyName="SelectedValue" Name="ID" />
        </DeleteParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="MeetingTypeDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
        SelectCommand="SELECT [Type] FROM [Meeting Types] ORDER BY [Type]"></asp:AccessDataSource>
    <h3>
        Add a Meeting</h3>
    <br />
    <asp:FormView ID="MeetingsForm" runat="server" CellPadding="4" DataKeyNames="ID"
        DataSourceID="MeetingsDataSource" DefaultMode="Insert" ForeColor="#333333" Width="100%">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <InsertItemTemplate>
            Date: &nbsp;
            <asp:TextBox ID="DateTextBox" runat="server" Text='<%# Bind("Date") %>' />
            &nbsp; Type: &nbsp;
            <asp:DropDownList ID="TypeDropDownList" runat="server" DataSourceID="MeetingTypeDataSource"
                DataTextField="Type" />
            <br />
            Notes: &nbsp;
            <asp:TextBox ID="NotesTextBox" runat="server" Width="60%" Text='<%# Bind("Notes") %>' />
            &nbsp;
            <asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                Text="Add Meeting" />
        </InsertItemTemplate>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
    </asp:FormView>
    <h3>
        View Meetings</h3>
    <br />
    <nmc:YearDropDownList ID="YearDropDownList" runat="server" />
    <br />
    <br />
    <asp:GridView ID="MeetingsGrid" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="MeetingsDataSource"
        ForeColor="#333333" GridLines="None" PageSize="20">
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True"></asp:CommandField>
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                SortExpression="ID" Visible="False"></asp:BoundField>
            <asp:BoundField DataField="Date" DataFormatString="{0:M/d/yy h:mm tt}" HeaderText="Date"
                SortExpression="Date" ApplyFormatInEditMode="True">
                <ControlStyle Width="100px" />
            </asp:BoundField>
            <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type">
                <ControlStyle Width="110px" />
            </asp:BoundField>
            <asp:BoundField DataField="membersCount" HeaderText="Members" SortExpression="membersCount"
                ReadOnly="true">
                <ControlStyle Width="35px" />
            </asp:BoundField>
            <asp:BoundField DataField="Notes" HeaderText="Notes" SortExpression="Notes">
                <ControlStyle Width="130px" />
            </asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>
