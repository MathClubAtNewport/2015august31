﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class officers_teams : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void CreateTeamForm_DataBound(object sender, EventArgs e)
    {
        TeamsGrid.DataBind();
    }

    protected void ShowHidePanels()
    {
        if (CompetitionsGrid.SelectedIndex < 0)
            TeamsPanel.Visible = false;
        else
            TeamsPanel.Visible = true;

        if (TeamsGrid.SelectedIndex < 0 || TeamsGrid.Rows.Count <= 0)
            CompetitorsPanel.Visible = false;
        else
            CompetitorsPanel.Visible = true;
    }

    protected void TeamsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHidePanels();
        AddTeamMemberForm.DataBind();
    }

    protected void CompetitionsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHidePanels();
    }

    protected void TeamsDataSource_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        TeamsGrid.DataBind();
        ShowHidePanels();
    }

    protected void TeamsGrid_DataBound(object sender, EventArgs e)
    {
        ShowHidePanels();
    }

    protected void TeamMembersGrid_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        AddTeamMemberForm.DataBind();
    }

    protected void AddTeamMemberForm_DataBound(object sender, EventArgs e)
    {
        TeamMembersGrid.DataBind();
        DropDownList MemberDropDownList = (DropDownList)AddTeamMemberForm.FindControl("MemberDropDownList");
        if (MemberDropDownList.Items.Count > 0)
        {
            AddTeamMemberForm.Visible = true;
            ErrorLabel.Visible = false;
        }
        else
        {
            AddTeamMemberForm.Visible = false; // no competitors not in team already
            ErrorLabel.Visible = true;
        }
    }
}