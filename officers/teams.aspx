﻿<%@ Page Title="Teams &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="teams.aspx.cs" Inherits="officers_teams" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
<h2>Team Management</h2>
			<hr />
			<asp:AccessDataSource ID="CompetitionsDataSource" runat="server" DataFile="~/App_Data/MathClub.mdb" 
                SelectCommand="SELECT [ID], [Name], [Date], [StartTime] FROM [Competitions] ORDER BY [Date] DESC">
            </asp:AccessDataSource>
		    <asp:AccessDataSource ID="TeamsDataSource" runat="server" 
                DataFile="~/App_Data/MathClub.mdb" 
                SelectCommand="SELECT [ID], [Team Description] AS Team_Description FROM [Teams] WHERE ([Competition] = ?) ORDER BY [Team Description]"
                UpdateCommand="UPDATE [Teams] SET [Team Description] = ? WHERE [ID] = ?"
                DeleteCommand="DELETE FROM [Teams] WHERE [ID] = ?"
                InsertCommand="INSERT INTO [Teams]([Competition], [Team Description]) Values(?, ?)" OnDeleted="TeamsDataSource_Deleted">
                <SelectParameters>
                    <asp:ControlParameter ControlID="CompetitionsGrid" Name="Competition" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="CompetitionsGrid" Name="Competition" PropertyName="SelectedValue" Type="Int32" />
                </InsertParameters>
            </asp:AccessDataSource>
            <asp:AccessDataSource ID="CompetitorsDataSource" runat="server" 
                DataFile="~/App_Data/MathClub.mdb" 
                SelectCommand="SELECT C.[ID], C.[Member] FROM [Competitors] C, [Teams] T WHERE (C.[Competition]=? AND T.[ID] = ?
                AND (SELECT COUNT(*) FROM [Team_Members] WHERE [Team]=T.[ID] AND [Competitor]=C.[ID]) = 0) ORDER BY C.[Member]" >
                <SelectParameters>
                    <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="TeamsGrid" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:AccessDataSource>
            <asp:AccessDataSource ID="TeamMembersDataSource" runat="server" 
                DataFile="~/App_Data/MathClub.mdb" 
                SelectCommand="SELECT M.[ID], C.[Member] FROM [Team_Members] M, [Competitors] C WHERE (M.[Team] = ? AND C.[ID] = M.[Competitor]) ORDER BY C.[Member]"
                DeleteCommand="DELETE FROM [Team_Members] WHERE [ID] = ?"
                InsertCommand="INSERT INTO [Team_Members]([Team], [Competitor]) Values(?,?)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="TeamsGrid" Name="Team" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="TeamsGrid" Name="Team" PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="AddTeamMemberForm$MemberDropDownList" Name="Competitor" PropertyName="SelectedValue" Type="Int32" />
                </InsertParameters>
            </asp:AccessDataSource>
            
            <asp:Panel ID="CompetitorsPanel" runat="server" Visible="False">
                <table id="CompetitorsTable" style="width:100%;">
                    <tr>
                        <td valign="top">
                            <h3>
                                Team Members</h3>
                            <asp:GridView ID="TeamMembersGrid" runat="server" AllowPaging="True" 
                                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                                DataKeyNames="ID" DataSourceID="TeamMembersDataSource" ForeColor="#333333" 
                                GridLines="None" OnRowDeleted="TeamMembersGrid_RowDeleted" PageSize="15">
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:CommandField ShowDeleteButton="True" />
                                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                                        ReadOnly="True" SortExpression="ID" Visible="False" />
                                    <asp:BoundField DataField="Member" HeaderText="Team Member" ReadOnly="true" 
                                        SortExpression="Member" />
                                </Columns>
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </td>
                        <td valign="top"> 
                            <h3>
                                Add A Member</h3>
                            <p>
                                <asp:FormView ID="AddTeamMemberForm" runat="server" CellPadding="4" DataKeyNames="ID" 
                            DataSourceID="TeamMembersDataSource" ForeColor="#333333" DefaultMode="Insert" OnDataBound="AddTeamMemberForm_DataBound">
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <InsertItemTemplate>
                                        Member:
                                        <asp:DropDownList ID="MemberDropDownList" DataSourceID="CompetitorsDataSource" 
                                    DataTextField="Member" DataValueField="ID" runat="server" />
                                        <br />
                                        <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                                    CommandName="Insert" Text="Add Member to Team" />
                                    </InsertItemTemplate>
                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                </asp:FormView>
                            </p>
                            <p>
                                <asp:Label ID="ErrorLabel" runat="server" 
                                    Text="* All competitors for this competition are on this team." Visible="False"></asp:Label>
                            </p>
                        </td>
                    </tr>
                </table>
                <hr />
            </asp:Panel>

            <asp:Panel ID="TeamsPanel" runat="server" Visible="False">
                <table id="TeamsTable" style="width: 100%;">
                    <tr>
                        <td>
                            <h3>
                                Teams</h3>
                            <asp:GridView ID="TeamsGrid" runat="server" AllowPaging="True" 
                                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                                DataKeyNames="ID" DataSourceID="TeamsDataSource" ForeColor="#333333" 
                                GridLines="None" OnDataBound="TeamsGrid_DataBound" 
                                OnSelectedIndexChanged="TeamsGrid_SelectedIndexChanged" PageSize="15">
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:CommandField EditText="Edit Name" SelectText="Edit Members" 
                                        ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
                                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                                        ReadOnly="True" SortExpression="ID" Visible="False" />
                                    <asp:BoundField DataField="Team_Description" HeaderText="Team_Description" 
                                        SortExpression="Team_Description" />
                                </Columns>
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </td>
                        <td valign="top">
                            <h4>
                                Create A Team</h4>
                            <asp:FormView ID="CreateTeamForm" runat="server" CellPadding="4" 
                            DataKeyNames="ID" DataSourceID="TeamsDataSource" DefaultMode="Insert" 
                            ForeColor="#333333" OnDataBound="CreateTeamForm_DataBound">
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <InsertItemTemplate>
                                    Team Description:
                                    <asp:TextBox ID="Team_DescriptionTextBox" runat="server" 
                                    Text='<%# Bind("Team_Description") %>' />
                                    <br />
                                    <asp:LinkButton ID="InsertButton0" runat="server" CausesValidation="True" 
                                    CommandName="Insert" Text="Create Team" />
                                </InsertItemTemplate>
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            </asp:FormView>
                            <br />
                        </td>
                    </tr>
                </table>
                <hr />
            </asp:Panel>

		    <h3>Competitions</h3>

		    <asp:GridView ID="CompetitionsGrid" runat="server" AllowPaging="True" AllowSorting="True" CellPadding="4" DataSourceID="CompetitionsDataSource" 
                ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" DataKeyNames="ID" ToolTip="Select a Competition" OnSelectedIndexChanged="CompetitionsGrid_SelectedIndexChanged">
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                        ReadOnly="True" SortExpression="ID" Visible="False" />
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:BoundField DataField="Date" DataFormatString="{0:M/d/yy}" 
                        HeaderText="Date" SortExpression="Date" />
                    <asp:BoundField DataField="StartTime" DataFormatString="{0:h:mm tt}" 
                        HeaderText="Start Time" SortExpression="StartTime" />
                </Columns>
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
</asp:Content>

