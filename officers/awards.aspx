﻿<%@ Page Title="Newport Math Club &ndash; Manage Awards" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="awards.aspx.cs" Inherits="officers_awards" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Newport Math Club - Manage Awards</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <asp:AccessDataSource ID="CompetitionsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT [ID], [Name], [Date] FROM [Competitions] ORDER BY [Date] DESC">
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="TeamsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT [ID], [Team Description] FROM [Teams] WHERE [Competition]=? ORDER BY [Team Description]">
        <SelectParameters>
            <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="IndividualsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT [ID], [Member] FROM [Competitors] WHERE [Competition]=? ORDER BY [Member]">
        <SelectParameters>
            <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="TeamAwardsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT A.[ID], A.[Place], A.[Division], A.[Award Description] as description
                FROM [Team_Awards] A, [Teams] T, [Competitions] C
                WHERE T.[ID]=A.[Team] AND C.[ID]=T.[Competition] AND T.[ID]=?
                ORDER BY C.[Date] DESC" InsertCommand="INSERT INTO [Team_Awards]([Team], [Place], [Division], [Award Description]) Values(?,?,?,?)"
        DeleteCommand="DELETE FROM [Team_Awards] WHERE [ID]=?" UpdateCommand="UPDATE [Team_Awards] SET [Place]=?, [Division]=?, [Award Description]=? WHERE [ID]=?"
        OnInserted="TeamAwardsData_Inserted" OnDeleted="TeamAwardsData_Deleted">
        <SelectParameters>
            <asp:ControlParameter ControlID="TeamsGrid" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="TeamsGrid" PropertyName="SelectedValue" Type="Int32" />
        </InsertParameters>
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="IndividualAwardsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT A.[ID], C.[Name], P.[Member], A.[Place], A.[Division], A.[Award Description] as description
                FROM [Individual_Awards] A, [Competitors] P, [Competitions] C
                WHERE P.[ID]=A.[Competitor] AND C.[ID]=P.[Competition] AND P.[ID]=?
                ORDER BY C.[Date] DESC" InsertCommand="INSERT INTO [Individual_Awards]([Competitor], [Place], [Division], [Award Description]) Values(?,?,?,?)"
        DeleteCommand="DELETE FROM [Individual_Awards] WHERE [ID]=?" UpdateCommand="UPDATE [Individual_Awards] SET [Place]=?, [Division]=?, [Award Description]=? WHERE [ID]=?"
        OnInserted="IndividualAwardsData_Inserted" OnDeleted="IndividualAwardsData_Deleted">
        <SelectParameters>
            <asp:ControlParameter ControlID="IndividualsGrid" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="IndividualsGrid" PropertyName="SelectedValue" Type="Int32" />
        </InsertParameters>
    </asp:AccessDataSource>
    <h2>
        Manage Awards</h2>
    <p>
        This page allows you to update the awards that teams and individuals have won.</p>
    <h4>
        Select a Competition:</h4>
    <asp:GridView ID="CompetitionsGrid" runat="server" CellPadding="4" ForeColor="#333333"
        GridLines="None" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
        DataSourceID="CompetitionsData" DataKeyNames="ID" OnSelectedIndexChanged="CompetitionsGrid_SelectedIndexChanged">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                SortExpression="ID" Visible="False" />
            <asp:BoundField DataField="Name" HeaderText="Competition Name" SortExpression="Name" />
            <asp:BoundField DataField="Date" DataFormatString="{0:ddd M/d/yy}" HeaderText="Date"
                SortExpression="Date" />
        </Columns>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <br />
    <table style="width: 100%">
        <tr>
            <td valign="top">
                <asp:Panel ID="TeamPanel" runat="server">
                    <h4>
                        Select a Team:</h4>
                    <asp:GridView ID="TeamsGrid" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CellPadding="4" DataSourceID="TeamsData" ForeColor="#333333"
                        GridLines="None" DataKeyNames="ID" OnSelectedIndexChanged="TeamsGrid_SelectedIndexChanged">
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" />
                            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                SortExpression="ID" Visible="False" />
                            <asp:BoundField DataField="Team Description" HeaderText="Teams" SortExpression="Team Description" />
                        </Columns>
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                    <asp:Label ID="NoTeamsLabel" runat="server" Text="No teams are available for this competition."></asp:Label>
                </asp:Panel>
            </td>
            <td valign="top">
                <asp:Panel ID="OrPanel" runat="server">
                    <h4>
                        -or-</h4>
                </asp:Panel>
            </td>
            <td valign="top" style="padding-left: 5px">
                <asp:Panel ID="IndividualPanel" runat="server">
                    <h4>
                        Select an Individual:</h4>
                    <asp:GridView ID="IndividualsGrid" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CellPadding="4" DataSourceID="IndividualsData" ForeColor="#333333"
                        GridLines="None" DataKeyNames="ID" OnSelectedIndexChanged="IndividualsGrid_SelectedIndexChanged">
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" />
                            <asp:BoundField DataField="ID" Visible="false" />
                            <asp:BoundField DataField="Member" HeaderText="Individuals" SortExpression="Member" />
                        </Columns>
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                    <asp:Label ID="NoIndividualsLabel" runat="server" Text="No individuals are available for this competition."></asp:Label>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <br />
    <table style="width: 100%">
        <tr>
            <td valign="top">
                <asp:Panel ID="TeamAwardsPanel" runat="server">
                    <h4>
                        Team Awards</h4>
                    <asp:GridView ID="TeamAwardsGrid" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="TeamAwardsData"
                        ForeColor="#333333" GridLines="None">
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <Columns>
                            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                SortExpression="ID" Visible="False" />
                            <asp:BoundField DataField="Place" HeaderText="Place" SortExpression="Place" />
                            <asp:BoundField DataField="Division" HeaderText="Division" SortExpression="Division" />
                            <asp:BoundField DataField="description" HeaderText="Award Description" SortExpression="description" />
                        </Columns>
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </asp:Panel>
            </td>
            <td valign="top" style="padding-left: 5px">
                <asp:Panel ID="AddTeamAwardPanel" runat="server">
                    <h4>
                        Add a Team Award</h4>
                    <asp:FormView ID="TeamAwardsForm" runat="server" CellPadding="4" DataKeyNames="ID"
                        DataSourceID="TeamAwardsData" DefaultMode="Insert" ForeColor="#333333">
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <InsertItemTemplate>
                            Place:
                            <asp:TextBox ID="PlaceTextBox" runat="server" Text='<%# Bind("Place") %>' />
                            <br />
                            Division:
                            <asp:TextBox ID="DivisionTextBox" runat="server" Text='<%# Bind("Division") %>' />
                            <br />
                            Award Description:
                            <asp:TextBox ID="descriptionTextBox" runat="server" Text='<%# Bind("description") %>'
                                Width="95%" />
                            <br />
                            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                                Text="Add Award" />
                        </InsertItemTemplate>
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    </asp:FormView>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td valign="top">
                <asp:Panel ID="IndividualAwardsPanel" runat="server">
                    <h4>
                        Individual Awards</h4>
                    <asp:GridView ID="IndividualAwardsGrid" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="IndividualAwardsData"
                        ForeColor="#333333" GridLines="None">
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <Columns>
                            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" SortExpression="ID"
                                Visible="False" />
                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" Visible="False" />
                            <asp:BoundField DataField="Member" HeaderText="Member" SortExpression="Member" Visible="False" />
                            <asp:BoundField DataField="Place" HeaderText="Place" SortExpression="Place" />
                            <asp:BoundField DataField="Division" HeaderText="Division" SortExpression="Division" />
                            <asp:BoundField DataField="description" HeaderText="Award Description" SortExpression="description" />
                        </Columns>
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </asp:Panel>
            </td>
            <td valign="top" style="padding-left: 5px">
                <asp:Panel ID="AddIndividualAwardPanel" runat="server">
                    <h4>
                        Add an Individual Award</h4>
                    <asp:FormView ID="AddIndividualAwardForm" runat="server" CellPadding="4" DataKeyNames="ID"
                        DataSourceID="IndividualAwardsData" DefaultMode="Insert" ForeColor="#333333">
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <InsertItemTemplate>
                            Place:
                            <asp:TextBox ID="PlaceTextBox0" runat="server" Text='<%# Bind("Place") %>' />
                            <br />
                            Division:
                            <asp:TextBox ID="DivisionTextBox0" runat="server" Text='<%# Bind("Division") %>' />
                            <br />
                            Award Description:
                            <asp:TextBox ID="descriptionTextBox0" runat="server" Text='<%# Bind("description") %>'
                                Width="95%" />
                            <br />
                            <asp:LinkButton ID="InsertButton0" runat="server" CausesValidation="True" CommandName="Insert"
                                Text="Add Award" />
                        </InsertItemTemplate>
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    </asp:FormView>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <hr />
    <!-- #EndEditable -->
</asp:Content>
