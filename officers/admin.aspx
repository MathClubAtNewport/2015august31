﻿<%@ Page Title="Admin &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Officer Administration Page</h2>
    <p>
        <a href="attendance.aspx">Attendance</a></p>
    <p>
        <a href="meetings.aspx">Meeting Management</a></p>
    <p>
        <a href="competitions.aspx">Competition Management</a></p>
    <p>
        <a href="teams.aspx">Team Management</a></p>
    <p>
        <a href="members.aspx">Member Management</a></p>
    <p>
        <a href="../members/email.aspx">Send E-mail</a></p>
</asp:Content>
