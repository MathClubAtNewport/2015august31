﻿<%@ Page Title="Newport Math Club &ndash; Competition Management" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="competitions.aspx.cs" Inherits="officers_competitions" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Competition Management</h2>
    <p>
        This page controls the database section for the competitions that we are attending
        or have attended.</p>
    <nmc:YearDropDownList ID="YearDropDownList" runat="server" />
    <p>
        <asp:AccessDataSource ID="CompetitionsDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
            SelectCommand="SELECT * FROM [Competitions] WHERE [Year] = ? ORDER BY [Date]" DeleteCommand="DELETE FROM [Competitions] WHERE [ID] = ?">
            <SelectParameters>
                <asp:ControlParameter ControlID="YearDropDownList" PropertyName="SelectedYear" Type="String" Name="Year" />
            </SelectParameters>
            <DeleteParameters>
                <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Name="ID" />
            </DeleteParameters>
        </asp:AccessDataSource>
        <asp:AccessDataSource ID="CompetitionDetailsDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
            SelectCommand="SELECT * FROM [Competitions] WHERE ([ID] = ?) ORDER BY [Date]"
            InsertCommand="INSERT INTO [Competitions]([Name], [Location], [Date], [MeetTime], [StartTime], [EndTime],
					[Fees], [Pay], [Meals], [Transportation], [Registered], [Paid], [RegistrationInfo], [Description]) Values(@Name,@Location,
					@Date,@MeetTime,@StartTime,@EndTime,@Fees,@Pay,@Meals,@Transportation,@Registered,@Paid,@RegistrationInfo,@Description)"
            DeleteCommand="DELETE FROM [Competitions] WHERE [ID] = ?" UpdateCommand="UPDATE [Competitions] Set[Name]=?, [Location]=?, [Date]=?, [MeetTime]=?, [StartTime]=?, [EndTime]=?, [Fees]=?,
					[Pay]=?, [Meals]=?, [Transportation]=?, [Registered]=?, [Paid]=?, [RegistrationInfo]=?, [Description]=? WHERE [ID]=?">
            <SelectParameters>
                <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" />
            </SelectParameters>
            <DeleteParameters>
                <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Name="ID" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Name="ID" />
            </UpdateParameters>
        </asp:AccessDataSource>
        <asp:FormView ID="CompetitionDetailsForm" OnDataBound="CompetitionDetailsForm_DataBound"
            runat="server" CellPadding="4" DataKeyNames="ID" DataSourceID="CompetitionDetailsDataSource"
            ForeColor="#333333">
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <EditItemTemplate>
                Name:
                <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' />
                <br />
                Location:
                <asp:TextBox ID="LocationTextBox" runat="server" Text='<%# Bind("Location") %>' />
                <br />
                Date:
                <asp:TextBox ID="DateTextBox" runat="server" Text='<%# Bind("Date", "{0:M/d/yyyy}") %>' />
                <br />
                Meet Time:
                <asp:TextBox ID="Meet_TimeTextBox" runat="server" Text='<%# Bind("[MeetTime]", "{0:h:mm tt}") %>' />
                <br />
                Start Time:
                <asp:TextBox ID="Start_TimeTextBox" runat="server" Text='<%# Bind("[StartTime]", "{0:h:mm tt}") %>' />
                <br />
                End Time:
                <asp:TextBox ID="End_TimeTextBox" runat="server" Text='<%# Bind("[EndTime]", "{0:h:mm tt}") %>' />
                <br />
                Fees Per Person:
                <asp:TextBox ID="FeesTextBox" runat="server" Text='<%# Bind("Fees", "{0:c}") %>' />
                <br />
                Each Member Pays:
                <asp:TextBox ID="PayTextBox" runat="server" Text='<%# Bind("Pay", "{0:c}") %>' />
                <br />
                Meals:
                <asp:TextBox ID="MealsTextBox" runat="server" Text='<%# Bind("Meals") %>' />
                <br />
                Transportation:
                <asp:TextBox ID="TransportationTextBox" runat="server" Text='<%# Bind("Transportation") %>' />
                <br />
                Registered:
                <asp:CheckBox ID="RegisteredCheckBox" runat="server" Checked='<%# Bind("Registered") %>' />
                <br />
                Paid:
                <asp:CheckBox ID="PaidCheckBox" runat="server" Checked='<%# Bind("Paid") %>' />
                <br />
                Registration Info:
                <asp:TextBox ID="Registration_InfoTextBox" runat="server" Text='<%# Bind("[RegistrationInfo]") %>' />
                <br />
                Description:<br />
                <asp:TextBox ID="DescriptionTextBox" TextMode="MultiLine" Wrap="true" Height="150px"
                    Width="700px" runat="server" Text='<%# Bind("[Description]") %>' />
                <br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                    Text="Update" />
                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False"
                    CommandName="Cancel" Text="Cancel" />
            </EditItemTemplate>
            <InsertItemTemplate>
                Name:
                <asp:TextBox ID="NameTextBox0" runat="server" Text='<%# Bind("Name") %>' />
                <br />
                Location:
                <asp:TextBox ID="LocationTextBox0" runat="server" Text='<%# Bind("Location") %>' />
                <br />
                Date:
                <asp:TextBox ID="DateTextBox0" runat="server" Text='<%# Bind("Date") %>' />
                <br />
                Meet Time:
                <asp:TextBox ID="Meet_TimeTextBox0" runat="server" Text='<%# Bind("[MeetTime]") %>' />
                <br />
                Start Time:
                <asp:TextBox ID="Start_TimeTextBox0" runat="server" Text='<%# Bind("[StartTime]") %>' />
                <br />
                End Time:
                <asp:TextBox ID="End_TimeTextBox0" runat="server" Text='<%# Bind("[EndTime]") %>' />
                <br />
                Fees Per Person:
                <asp:TextBox ID="FeesTextBox0" runat="server" Text='<%# Bind("Fees") %>' />
                <br />
                Each Member Pays:
                <asp:TextBox ID="PayTextBox0" runat="server" Text='<%# Bind("Pay") %>' />
                <br />
                Meals:
                <asp:TextBox ID="MealsTextBox0" runat="server" Text='<%# Bind("Meals") %>' />
                <br />
                Transportation:
                <asp:TextBox ID="TransportationTextBox0" runat="server" Text='<%# Bind("Transportation") %>' />
                <br />
                Registered:
                <asp:CheckBox ID="RegisteredCheckBox0" runat="server" Checked='<%# Bind("Registered") %>' />
                <br />
                Paid:
                <asp:CheckBox ID="PaidCheckBox0" runat="server" Checked='<%# Bind("Paid") %>' />
                <br />
                Registration Info:
                <asp:TextBox ID="Registration_InfoTextBox0" runat="server" Text='<%# Bind("[RegistrationInfo]") %>' />
                <br />
                Description:<br />
                <asp:TextBox ID="DescriptionTextBox0" TextMode="MultiLine" Wrap="true" Height="150px"
                    Width="700px" runat="server" Text='<%# Bind("[Description]") %>' />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                    Text="Insert" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False"
                    CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>
            <ItemTemplate>
                Name:
                <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>' />
                <br />
                Location:
                <asp:Label ID="LocationLabel" runat="server" Text='<%# Bind("Location") %>' />
                <br />
                Date:
                <asp:Label ID="DateLabel" runat="server" Text='<%# Bind("Date", "{0:ddd. MMMM d, yyyy}") %>' />
                <br />
                Meet Time:
                <asp:Label ID="Meet_TimeLabel" runat="server" Text='<%# Bind("[MeetTime]", "{0:h:mm tt}") %>' />
                <br />
                Start Time:
                <asp:Label ID="Start_TimeLabel" runat="server" Text='<%# Bind("[StartTime]", "{0:h:mm tt}") %>' />
                <br />
                End Time:
                <asp:Label ID="End_TimeLabel" runat="server" Text='<%# Bind("[EndTime]", "{0:h:mm tt}") %>' />
                <br />
                Fees Per Person:
                <asp:Label ID="FeesLabel" runat="server" Text='<%# Bind("Fees", "{0:c}") %>' />
                <br />
                Each Member Pays:
                <asp:Label ID="PayLabel" runat="server" Text='<%# Bind("Pay", "{0:c}") %>' />
                <br />
                Meals:
                <asp:Label ID="MealsLabel" runat="server" Text='<%# Bind("Meals") %>' />
                <br />
                Transportation:
                <asp:Label ID="TransportationLabel" runat="server" Text='<%# Bind("Transportation") %>' />
                <br />
                Registered:
                <asp:CheckBox ID="RegisteredCheckBox1" runat="server" Checked='<%# Bind("Registered") %>'
                    Enabled="false" />
                <br />
                Paid:
                <asp:CheckBox ID="PaidCheckBox1" runat="server" Checked='<%# Bind("Paid") %>' Enabled="false" />
                <br />
                Registration Info:
                <asp:Label ID="Registration_InfoLabel" runat="server" Text='<%# Bind("[RegistrationInfo]") %>' />
                <br />
                Description:
                <asp:Label ID="DescriptionTextBox1" runat="server" Text='<%# Bind("[Description]") %>' />
                <br />
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="false" CommandName="Edit"
                    Text="Edit" />
                &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete"
                    Text="Delete" />
                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New"
                    Text="New" />
            </ItemTemplate>
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        </asp:FormView>
        <br />
        <asp:GridView ID="CompetitionsGrid" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="CompetitionsDataSource"
            ForeColor="#333333" GridLines="None">
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:CommandField ShowSelectButton="True"></asp:CommandField>
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                    SortExpression="ID" Visible="False"></asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location">
                </asp:BoundField>
                <asp:BoundField DataField="Date" DataFormatString="{0:ddd. MMMM d, yyyy}" HeaderText="Date"
                    SortExpression="Date"></asp:BoundField>
                <asp:BoundField DataField="MeetTime" HeaderText="Meet Time" SortExpression="MeetTime"
                    Visible="False"></asp:BoundField>
                <asp:BoundField DataField="StartTime" DataFormatString="{0:h:mm tt}" HeaderText="Start Time"
                    SortExpression="StartTime"></asp:BoundField>
                <asp:BoundField DataField="EndTime" HeaderText="End Time" SortExpression="EndTime"
                    Visible="False"></asp:BoundField>
                <asp:BoundField DataField="Fees" HeaderText="Fees" SortExpression="Fees" Visible="False">
                </asp:BoundField>
                <asp:BoundField DataField="Pay" HeaderText="Pay" SortExpression="Pay" Visible="False">
                </asp:BoundField>
                <asp:BoundField DataField="Meals" HeaderText="Meals" SortExpression="Meals" Visible="False">
                </asp:BoundField>
                <asp:BoundField DataField="Transportation" HeaderText="Transportation" SortExpression="Transportation"
                    Visible="False"></asp:BoundField>
                <asp:CheckBoxField DataField="Registered" HeaderText="Registered" SortExpression="Registered">
                </asp:CheckBoxField>
                <asp:CheckBoxField DataField="Paid" HeaderText="Paid" SortExpression="Paid"></asp:CheckBoxField>
                <asp:BoundField DataField="RegistrationInfo" HeaderText="Registration Info" SortExpression="RegistrationInfo"
                    Visible="False"></asp:BoundField>
                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description"
                    Visible="False"></asp:BoundField>
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </p>
</asp:Content>
