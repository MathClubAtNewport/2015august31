﻿<%@ Page Title="Attendance &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="attendance.aspx.cs" Inherits="officers_attendance" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Attendance</h2>
    <p>
        This page keeps track of attendance records for math club meetings.</p>
    <table style="width: 86%">
        <tr>
            <td style="width: 440px" valign="top">
                <h4>
                    Select a Meeting</h4>
                <p>
                    <asp:AccessDataSource ID="YearsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
                        SelectCommand="SELECT [Year] FROM [Years] ORDER BY [Year] DESC"></asp:AccessDataSource>
                    Choose a year:
                    <asp:DropDownList ID="YearDropDownList" runat="server" AutoPostBack="True" DataSourceID="YearsData"
                        DataTextField="Year" DataValueField="Year" OnDataBound="YearDropDownList_DataBound">
                    </asp:DropDownList>
                    <br />
                </p>
            </td>
            <td style="width: 413px">
                <asp:Panel ID="AttendancePanel" runat="server">
                    <h3>
                        Add a Member</h3>
                    <!--<asp:RadioButtonList ID="ActiveMembersRadioButtonList" runat="server" 
                                    AutoPostBack="True" RepeatDirection="Horizontal" CssClass="style7" 
                                    ToolTip="Select an option to choose which members to show.">
                                    <asp:ListItem Selected="True" Value="true">Active Members</asp:ListItem>
                                    <asp:ListItem Value="false">All Members</asp:ListItem>
                                </asp:RadioButtonList>-->
                    <div style="margin: 10px 10px 0 20px">
                        <asp:CheckBox ID="ShowInactiveCheckBox" runat="server" Text="Show Inactive" AutoPostBack="true" />
                        &nbsp;<asp:CheckBox ID="ShowNonMemberCheckBox" runat="server" Text="Show Non-Members"
                            AutoPostBack="true" />
                        &nbsp;<asp:CheckBox ID="ShowGraduatedCheckBox" runat="server" Text="Show Graduated"
                            AutoPostBack="true" />
                        <asp:FormView ID="AttendanceForm" runat="server" DataSourceID="AttendanceDataSource"
                            DefaultMode="Insert" Width="400px">
                            <EditItemTemplate>
                                Name:
                                <asp:DropDownList ID="NameDropDownList" runat="server" DataSourceID="MembersDataSource"
                                    DataTextField="Name" Width="350px" />
                                <br />
                                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                                    Text="Update" />
                                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False"
                                    CommandName="Cancel" Text="Cancel" />
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                Name:
                                <asp:DropDownList ID="NameDropDownList0" runat="server" DataSourceID="MembersDataSource"
                                    DataTextField="Name" Width="200px" />
                                &nbsp;<asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                                    Text="Add Member" />
                            </InsertItemTemplate>
                            <ItemTemplate>
                                Name:
                                <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>' />
                                <br />
                            </ItemTemplate>
                        </asp:FormView>
                    </div>
                </asp:Panel>
                <asp:AccessDataSource ID="MeetingsDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
                    SelectCommand="SELECT [ID], [Date], [Type] FROM [Meetings] WHERE [Year] = ? ORDER BY [Date] DESC">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="YearDropDownList" Type="String" PropertyName="SelectedValue"
                            Name="Year" />
                    </SelectParameters>
                </asp:AccessDataSource>
                <!--<asp:AccessDataSource id="MembersDataSource" runat="server" DataFile="../App_Data/MathClub.mdb" SelectCommand="SELECT M.[Name], M.[Grade] FROM [Members] M, [Meetings] E
			    WHERE (E.[ID] = ? AND ((M.[Active] = true) OR (? = false)) AND (SELECT COUNT(*) FROM [Attendance] A WHERE A.[Name] = M.[Name] AND A.[Meeting] = E.[ID]) = 0) ORDER BY M.[Name]">
			    <SelectParameters>
			        <asp:ControlParameter ControlID="MeetingsGrid" PropertyName="SelectedValue" Type="Int32" />
			        <asp:ControlParameter ControlID="ActiveMembersRadioButtonList" PropertyName="SelectedValue" Type="Boolean" />
			    </SelectParameters>
			</asp:AccessDataSource>-->
                <!--<asp:AccessDataSource id="NewMembersDataSourceOLD" runat="server" DataFile="../App_Data/MathClub.mdb" SelectCommand="SELECT M.[Name], M.[Grade],
			    ((SELECT COUNT(*) FROM [Attendance] A WHERE A.[Name] = M.[Name] AND A.[Meeting] = E.[ID]) = 1) AS Present FROM [Members] M, [Meetings] E
			    WHERE (E.[ID] = ? AND ((M.[Active] = true) OR ((? = false) AND M.[Member] = true) OR ((SELECT COUNT(*) FROM [Attendance] A WHERE A.[Name] = M.[Name] AND A.[Meeting] = E.[ID]) = 1)))
			    ORDER BY M.[Name]">
			    <SelectParameters>
			        <asp:ControlParameter ControlID="MeetingsGrid" PropertyName="SelectedValue" Type="Int32" />
			        <asp:ControlParameter ControlID="ActiveMembersRadioButtonList" PropertyName="SelectedValue" Type="Boolean" />
			    </SelectParameters>
			</asp:AccessDataSource>-->
                <asp:AccessDataSource ID="NewMembersDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
                    SelectCommand="SELECT M.[Name], M.[Grade],
			    ((SELECT COUNT(*) FROM [Attendance] A WHERE A.[Name] = M.[Name] AND A.[Meeting] = E.[ID]) = 1) AS Present FROM [Members] M, [Meetings] E
			    WHERE (E.[ID] = ? AND ((([Active] = true OR ? = true)
			    AND ([Member] = true OR ? = true) AND (NOT [Grade] = 13)) OR ([Grade] = 13 AND ? = true)
			    OR ((SELECT COUNT(*) FROM [Attendance] A WHERE A.[Name] = M.[Name] AND A.[Meeting] = E.[ID]) = 1)))
			    ORDER BY M.[Name]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="MeetingsGrid" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter Name="ShowInactive" Type="Boolean" ControlID="ShowInactiveCheckBox"
                            PropertyName="Checked" />
                        <asp:ControlParameter Name="ShowNonMember" Type="Boolean" ControlID="ShowNonMemberCheckBox"
                            PropertyName="Checked" />
                        <asp:ControlParameter Name="ShowGraduated" Type="Boolean" ControlID="ShowGraduatedCheckBox"
                            PropertyName="Checked" />
                    </SelectParameters>
                </asp:AccessDataSource>
                <asp:AccessDataSource ID="AttendanceDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
                    SelectCommand="SELECT DISTINCT [Name] FROM [Attendance] WHERE ([Meeting] = ?) ORDER BY [Name] ASC"
                    InsertCommand="INSERT INTO [Attendance](Name, Meeting) Values(?, ?)" DeleteCommand="DELETE FROM [Attendance] WHERE [Meeting] = ? AND [Name] = ?"
                    OnInserting="AttendanceDataSource_Inserting" OnDeleting="AttendanceDataSource_Deleting">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="MeetingsGrid" Name="Meeting" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:ControlParameter ControlID="AttendanceForm$NameDropDownList0" Name="Name" />
                        <asp:ControlParameter ControlID="MeetingsGrid" Name="Meeting" />
                    </InsertParameters>
                    <DeleteParameters>
                        <asp:ControlParameter ControlID="MeetingsGrid" Name="Meeting" />
                    </DeleteParameters>
                </asp:AccessDataSource>
                <asp:AccessDataSource ID="MeetingDateDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
                    SelectCommand="SELECT [Date], [Type] FROM [Meetings] WHERE [ID]=?">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="meetingIDLabel" PropertyName="Text" Type="Int32" />
                    </SelectParameters>
                </asp:AccessDataSource>
                <asp:Label ID="meetingIDLabel" runat="server" Visible="false" />
                <asp:Label ID="attendanceIDLabel" runat="server" Visible="false" />
                <asp:Label ID="memberNameLabel" runat="server" Visible="false" />
                <asp:Label ID="dateLabel" runat="server" Visible="false" />
                <asp:Label ID="codeLabel" runat="server" Visible="false" />
                <asp:AccessDataSource ID="PointsDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
                    InsertCommand="INSERT INTO [Points]([Name], [Date], [Item]) Values(?, ?, ?)"
                    DeleteCommand="DELETE FROM [Points] WHERE [Name]=? AND [Date]=? AND [Item]=?">
                    <InsertParameters>
                        <asp:ControlParameter ControlID="memberNameLabel" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="dateLabel" PropertyName="Text" Type="DateTime" />
                        <asp:ControlParameter ControlID="codeLabel" PropertyName="Text" Type="String" />
                    </InsertParameters>
                    <DeleteParameters>
                        <asp:ControlParameter ControlID="memberNameLabel" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="dateLabel" PropertyName="Text" Type="DateTime" />
                        <asp:ControlParameter ControlID="codeLabel" PropertyName="Text" Type="String" />
                    </DeleteParameters>
                </asp:AccessDataSource>
                <asp:AccessDataSource ID="AttendanceInfoDataSource" runat="server" DataFile="../App_Data/MathClub.mdb"
                    InsertCommand="SELECT [Name] FROM [Attendance] WHERE [ID]=?">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="AttendanceIDLabel" PropertyName="Text" Type="Int32" />
                    </SelectParameters>
                </asp:AccessDataSource>
            </td>
        </tr>
        <tr>
            <td style="width: 440px" valign="top">
                <asp:GridView ID="MeetingsGrid" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="MeetingsDataSource"
                    ForeColor="#333333" GridLines="None" Width="307px" OnSelectedIndexChanged="MeetingsGrid_SelectedIndexChanged"
                    OnRowDeleted="MeetingsGrid_RowDeleted" PageSize="20">
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True"></asp:CommandField>
                        <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" InsertVisible="False"
                            ReadOnly="True" Visible="False"></asp:BoundField>
                        <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" DataFormatString="{0:M/dd/yy}">
                        </asp:BoundField>
                        <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type"></asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td class="style6" valign="top" style="width: 413px">
                <asp:GridView ID="AttendanceGridView" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" CellPadding="4" CssClass="style8" DataSourceID="AttendanceDataSource"
                    ForeColor="#333333" GridLines="None" HorizontalAlign="Justify" Width="185px"
                    DataKeyNames="Name" PageSize="20">
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" DeleteText="Not Present"></asp:CommandField>
                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <br />
                <asp:CheckBoxList ID="AttendanceCheckBoxList" runat="server" CssClass="style7" DataSourceID="NewMembersDataSource"
                    DataTextField="Name" DataValueField="Present" OnDataBound="AttendanceCheckBoxList_DataBound"
                    RepeatColumns="3">
                </asp:CheckBoxList>
                <br />
                <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" CssClass="float_right"
                    Text="Update Attendance" />
            </td>
        </tr>
    </table>
    <p>
        &nbsp;</p>
</asp:Content>
