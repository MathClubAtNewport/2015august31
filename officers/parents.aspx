﻿<%@ Page Title="Parents &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="parents.aspx.cs" Inherits="officers_parents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style5
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Parents</h2>
    <p>
        This page lets you view and edit the list of parents.</p>
    <p>
        <asp:AccessDataSource ID="ParentsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
            SelectCommand="SELECT [ID], [Name], [Email], [Driver], [Coach] FROM [Parents] ORDER BY [Name]">
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="Driver" Type="Boolean" />
                <asp:Parameter Name="Coach" Type="Boolean" />
                <asp:Parameter Name="ID" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="ID" Type="Int32" />
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="Driver" Type="Boolean" />
                <asp:Parameter Name="Coach" Type="Boolean" />
            </InsertParameters>
        </asp:AccessDataSource>
        <asp:AccessDataSource ID="ParentDetailsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
            SelectCommand="SELECT [ID], [Name], [Member], [Email], [Phone], [BestTimeToCall], [Driver], [Availability], [Coach], [Topics] FROM [Parents] WHERE ([ID] = ?) ORDER BY [Name]"
            DeleteCommand="DELETE FROM [Parents] WHERE [ID] = ?" InsertCommand="INSERT INTO [Parents] ([Name], [Member], [Email], [Phone], [BestTimeToCall], [Driver], [Availability], [Coach], [Topics]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
            UpdateCommand="UPDATE [Parents] SET [Name] = ?, [Member] = ?, [Email] = ?, [Phone] = ?, [BestTimeToCall] = ?, [Driver] = ?, [Availability] = ?, [Coach] = ?, [Topics] = ? WHERE [ID] = ?">
            <SelectParameters>
                <asp:ControlParameter ControlID="ParentsGrid" Name="ID" PropertyName="SelectedValue"
                    Type="Int32" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="Member" Type="String" />
                <asp:Parameter Name="Email" Type="String" />
                <asp:Parameter Name="Phone" Type="String" />
                <asp:Parameter Name="BestTimeToCall" Type="String" />
                <asp:Parameter Name="Driver" Type="Boolean" />
                <asp:Parameter Name="Availability" Type="String" />
                <asp:Parameter Name="Coach" Type="Boolean" />
                <asp:Parameter Name="Topics" Type="String" />
                <asp:Parameter Name="ID" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="Member" Type="String" />
                <asp:Parameter Name="Email" Type="String" />
                <asp:Parameter Name="Phone" Type="String" />
                <asp:Parameter Name="BestTimeToCall" Type="String" />
                <asp:Parameter Name="Driver" Type="Boolean" />
                <asp:Parameter Name="Availability" Type="String" />
                <asp:Parameter Name="Coach" Type="Boolean" />
                <asp:Parameter Name="Topics" Type="String" />
            </InsertParameters>
        </asp:AccessDataSource>
    </p>
    <table class="style5">
        <tr>
            <td valign="top">
                <asp:GridView ID="ParentsGrid" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    DataKeyNames="ID" DataSourceID="ParentsData" ForeColor="#333333" GridLines="None"
                    AllowPaging="True" AllowSorting="True">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                            SortExpression="ID" Visible="False" />
                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                        <asp:CheckBoxField DataField="Driver" HeaderText="Driver" SortExpression="Driver" />
                        <asp:CheckBoxField DataField="Coach" HeaderText="Coach" SortExpression="Coach" />
                    </Columns>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:FormView ID="ParentsForm" runat="server" CellPadding="4" DataKeyNames="ID" DataSourceID="ParentDetailsData"
                    ForeColor="#333333" OnDataBound="ParentsForm_DataBound">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <EditItemTemplate>
                        <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' Visible="false" />
                        Name:
                        <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' />
                        <br />
                        Member:
                        <asp:TextBox ID="MemberTextBox" runat="server" Text='<%# Bind("Member") %>' />
                        <br />
                        Email:
                        <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                        <br />
                        Phone:
                        <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' />
                        <br />
                        Best Time To Call:
                        <asp:TextBox ID="BestTimeToCallTextBox" runat="server" Text='<%# Bind("BestTimeToCall") %>' />
                        <br />
                        Driver:
                        <asp:CheckBox ID="DriverCheckBox" runat="server" Checked='<%# Bind("Driver") %>' />
                        <br />
                        Availability:
                        <asp:TextBox ID="AvailabilityTextBox" runat="server" Text='<%# Bind("Availability") %>' />
                        <br />
                        Coach:
                        <asp:CheckBox ID="CoachCheckBox" runat="server" Checked='<%# Bind("Coach") %>' />
                        <br />
                        Topics:
                        <asp:TextBox ID="TopicsTextBox" runat="server" Text='<%# Bind("Topics") %>' />
                        <br />
                        <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                            Text="Update" />
                        &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False"
                            CommandName="Cancel" Text="Cancel" />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        Name:
                        <asp:TextBox ID="NameTextBox0" runat="server" Text='<%# Bind("Name") %>' />
                        <br />
                        Member:
                        <asp:TextBox ID="MemberTextBox0" runat="server" Text='<%# Bind("Member") %>' />
                        <br />
                        Email:
                        <asp:TextBox ID="EmailTextBox0" runat="server" Text='<%# Bind("Email") %>' />
                        <br />
                        Phone:
                        <asp:TextBox ID="PhoneTextBox0" runat="server" Text='<%# Bind("Phone") %>' />
                        <br />
                        Best Time To Call:
                        <asp:TextBox ID="BestTimeToCallTextBox0" runat="server" Text='<%# Bind("BestTimeToCall") %>' />
                        <br />
                        Driver:
                        <asp:CheckBox ID="DriverCheckBox0" runat="server" Checked='<%# Bind("Driver") %>' />
                        <br />
                        Availability:
                        <asp:TextBox ID="AvailabilityTextBox0" runat="server" Text='<%# Bind("Availability") %>' />
                        <br />
                        Coach:
                        <asp:CheckBox ID="CoachCheckBox0" runat="server" Checked='<%# Bind("Coach") %>' />
                        <br />
                        Topics:
                        <asp:TextBox ID="TopicsTextBox0" runat="server" Text='<%# Bind("Topics") %>' />
                        <br />
                        <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" Text="Insert"
                            OnClick="InsertButton_Click" />
                        &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False"
                            CommandName="Cancel" Text="Cancel" />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' Visible="false" />
                        Name:
                        <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>' />
                        <br />
                        Member:
                        <asp:Label ID="MemberLabel" runat="server" Text='<%# Bind("Member") %>' />
                        <br />
                        Email:
                        <asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' />
                        <br />
                        Phone:
                        <asp:Label ID="PhoneLabel" runat="server" Text='<%# Bind("Phone") %>' />
                        <br />
                        Best Time To Call:
                        <asp:Label ID="BestTimeToCallLabel" runat="server" Text='<%# Bind("BestTimeToCall") %>' />
                        <br />
                        Driver:
                        <asp:CheckBox ID="DriverCheckBox1" runat="server" Checked='<%# Bind("Driver") %>'
                            Enabled="false" />
                        <br />
                        Availability:
                        <asp:Label ID="AvailabilityLabel" runat="server" Text='<%# Bind("Availability") %>' />
                        <br />
                        Coach:
                        <asp:CheckBox ID="CoachCheckBox1" runat="server" Checked='<%# Bind("Coach") %>' Enabled="false" />
                        <br />
                        Topics:
                        <asp:Label ID="TopicsLabel" runat="server" Text='<%# Bind("Topics") %>' />
                        <br />
                        <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit"
                            Text="Edit" />
                        &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete"
                            Text="Delete" />
                        &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New"
                            Text="New" />
                    </ItemTemplate>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                </asp:FormView>
                <br />
                <asp:Label ID="ErrorLabel" runat="server" ForeColor="Red" Text="* Error: Member not found. "
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
