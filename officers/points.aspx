﻿<%@ Page Title="My Pi Points &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="points.aspx.cs" Inherits="officers_points" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        Manage Pi Points</h2>
    <p>
        This page allows you to award pi points and manage them.</p>
    <asp:AccessDataSource ID="PointItemsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT [Code], [Item], [Points] FROM [Point_Values] ORDER BY [Item]">
    </asp:AccessDataSource>
    <asp:AccessDataSource ID="MembersData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT [Name] FROM [Members] ORDER BY [Name]"></asp:AccessDataSource>
    <asp:AccessDataSource ID="PointsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
        SelectCommand="SELECT P.[ID], P.[Name], P.[Date], V.[Item], V.[Points] FROM [Points] P, [Point_Values] V
                    WHERE V.[Code]=P.[Item] ORDER BY [Date] DESC" DeleteCommand="DELETE FROM [Points] WHERE [ID]=?"
        InsertCommand="INSERT INTO [Points]([Name], [Date], [Item]) Values(?,?,?)" OnDeleted="PointsData_Deleted">
        <InsertParameters>
            <asp:ControlParameter ControlID="MembersGrid" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="Calendar" PropertyName="SelectedDate" Type="DateTime" />
            <asp:ControlParameter ControlID="PointItemsGrid" PropertyName="SelectedValue" Type="String" />
        </InsertParameters>
    </asp:AccessDataSource>
    <table style="width: 100%;">
        <tr>
            <td valign="top">
                <asp:Calendar ID="Calendar" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66"
                    BorderWidth="1px" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt"
                    ForeColor="#663399" Height="200px" ShowGridLines="True" Width="220px" OnSelectionChanged="Calendar_SelectionChanged">
                    <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                    <SelectorStyle BackColor="#FFCC66" />
                    <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                    <OtherMonthDayStyle ForeColor="#CC9966" />
                    <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                    <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                    <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />
                </asp:Calendar>
            </td>
            <td valign="top">
                <asp:GridView ID="PointItemsGrid" runat="server" AllowPaging="True" DataKeyNames="Code"
                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataSourceID="PointItemsData"
                    ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="PointItemsGrid_SelectedIndexChanged"
                    PageSize="20">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="Code" HeaderText="Code" ReadOnly="True" SortExpression="Code"
                            Visible="False" />
                        <asp:BoundField DataField="Item" HeaderText="Item" SortExpression="Item" />
                        <asp:BoundField DataField="Points" HeaderText="Points" SortExpression="Points" />
                    </Columns>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="MembersGrid" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Name" DataSourceID="MembersData"
                    ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="MembersGrid_SelectedIndexChanged"
                    PageSize="20">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" SortExpression="Name" />
                    </Columns>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td valign="top">
                &nbsp;
            </td>
            <td valign="top">
                &nbsp;
            </td>
            <td valign="top">
                <asp:Button ID="Button" runat="server" Text="Award Points" OnClick="Button_Click" />
            </td>
        </tr>
    </table>
    <p>
        <asp:GridView ID="PointsGrid" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" CellPadding="4" DataSourceID="PointsData" ForeColor="#333333"
            GridLines="None" Width="100%" DataKeyNames="ID" PageSize="30">
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" />
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" SortExpression="ID"
                    Visible="False" />
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" DataFormatString="{0:M/d/yy}" />
                <asp:BoundField DataField="Item" HeaderText="Item" SortExpression="Item" />
                <asp:BoundField DataField="Points" HeaderText="Points" SortExpression="Points" />
            </Columns>
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </p>
</asp:Content>
