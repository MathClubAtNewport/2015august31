﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

public partial class officers_attendance : System.Web.UI.Page
{
    AccessDataSource data;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (data == null)
        {
            data = new AccessDataSource("../App_Data/MathClub.mdb", "SELECT [ID] FROM [Attendance] WHERE [Name] = ? AND [Meeting] = ?");
            data.DeleteCommand = "DELETE FROM [Attendance] WHERE [Name] = ? AND [Meeting] = ?";
            data.InsertCommand = "INSERT INTO [Attendance]([Name], [Meeting]) Values(?, ?)";
            this.Page.Controls.Add(data);
            data.SelectParameters.Clear();
            data.SelectParameters.Add(new Parameter("Name", TypeCode.String, ""));
            data.SelectParameters.Add(new ControlParameter("Meeting", TypeCode.Int32, "ctl00$body$" + MeetingsGrid.ID, "SelectedValue"));
            data.DeleteParameters.Clear();
            data.DeleteParameters.Add(new Parameter("Name", TypeCode.String, ""));
            data.DeleteParameters.Add(new ControlParameter("Meeting", TypeCode.Int32, "ctl00$body$" + MeetingsGrid.ID, "SelectedValue"));
            data.InsertParameters.Clear();
            data.InsertParameters.Add(new Parameter("Name", TypeCode.String, ""));
            data.InsertParameters.Add(new ControlParameter("Meeting", TypeCode.Int32, "ctl00$body$" + MeetingsGrid.ID, "SelectedValue"));
        }
        ShowHide();
    }

    protected void MeetingsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHide();
    }

    private void ShowHide()
    {
        bool b = !(MeetingsGrid.SelectedIndex < 0 || MeetingsGrid.Rows.Count <= 0);
        AttendanceGridView.Visible = false;
        AttendancePanel.Visible = b;
        btnUpdate.Visible = b;
    }

    protected void MeetingsGrid_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        ShowHide();
    }

    protected void AttendanceDataSource_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        object o = e.Command.Parameters;
        string name = e.Command.Parameters[0].Value as string;
        int meetingID = (int)(e.Command.Parameters[1].Value);
        addPiPoints(name, meetingID);
    }

    protected void addPiPoints(String name, int meetingID)
    {
        meetingIDLabel.Text = meetingID.ToString();
        System.Data.DataTable data = (MeetingDateDataSource.Select(DataSourceSelectArguments.Empty) as System.Data.DataView).ToTable();
        DateTime meetingDate = (DateTime)(data.Rows[0].ItemArray[0]);
        string type = (string)(data.Rows[0].ItemArray[1]);
        memberNameLabel.Text = name;
        dateLabel.Text = meetingDate.ToString();
        if (type.Equals("Officer Meeting"))
            return; // don't award points for officer meetings.
        if (type.Equals("Competition"))
            codeLabel.Text = "AC";
        else
            codeLabel.Text = "AM";
        PointsDataSource.Insert();
    }

    protected void removePiPoints(String name, int meetingID)
    {
        meetingIDLabel.Text = meetingID.ToString();
        System.Data.DataTable data2 = (MeetingDateDataSource.Select(DataSourceSelectArguments.Empty) as System.Data.DataView).ToTable();
        DateTime meetingDate = (DateTime)(data2.Rows[0].ItemArray[0]);
        string type = (string)(data2.Rows[0].ItemArray[1]);
        memberNameLabel.Text = name;
        dateLabel.Text = meetingDate.ToString();
        if (type.Equals("Officer Meeting"))
            return; // don't have points records for officer meetings.
        if (type.Equals("Competition"))
            codeLabel.Text = "AC";
        else
            codeLabel.Text = "AM";
        PointsDataSource.Delete();
    }

    protected void AttendanceDataSource_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        object o = e.Command.Parameters;
        string name = e.Command.Parameters[1].Value as string; // 0 meeting ID, 1 name
        /*int attendanceID = (int)(e.Command.Parameters[0].Value);
        attendanceIDLabel.Text = attendanceID.ToString();
        System.Data.DataTable data1 = (AttendanceInfoDataSource.Select(DataSourceSelectArguments.Empty) as System.Data.DataView).ToTable();
        if (!(data1.Rows.Count > 0 && data1.Columns.Count > 0))
            return; // error - can't find record being deleted.
        string name = data1.Rows[0].ItemArray[0] as string; */
        int meetingID = (int)(e.Command.Parameters[0].Value); // 0 meeting ID, 1 name
        removePiPoints(name, meetingID);
    }

    protected void AttendanceCheckBoxList_DataBound(object sender, EventArgs e)
    {
        foreach (ListItem item in AttendanceCheckBoxList.Items)
        {
            String name = item.Text;
            data.SelectParameters[0].DefaultValue = name;
            IEnumerable stuff1 = data.Select(DataSourceSelectArguments.Empty);
            IEnumerator stuff2 = stuff1.GetEnumerator();
            try
            {
                stuff2.MoveNext();
                if (stuff2.Current != null)
                    item.Selected = true;
            }
            catch (System.InvalidOperationException ex)
            {
                item.Selected = false;
                continue;
            }
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        int updated = 0;
        foreach (ListItem item in AttendanceCheckBoxList.Items)
        {
            String name = item.Text;
            bool present = item.Selected;
            if (!present)
            {
                data.DeleteParameters[0].DefaultValue = name;
                updated += data.Delete();
                removePiPoints(name, (int)MeetingsGrid.SelectedValue);
            }
            else
            {
                data.SelectParameters[0].DefaultValue = name;
                IEnumerable stuff1 = data.Select(DataSourceSelectArguments.Empty);
                IEnumerator stuff2 = stuff1.GetEnumerator();
                try
                {
                    stuff2.MoveNext();
                    if (stuff2.Current != null)
                        ; // do nothing - record already exists!
                }
                catch (System.InvalidOperationException) // add attendance record
                {
                    data.InsertParameters[0].DefaultValue = name;
                    addPiPoints(name, (int)MeetingsGrid.SelectedValue);
                    updated += data.Insert();
                    continue;
                }
            }
        }
        AttendanceForm.DataBind();
    }

    protected void YearDropDownList_DataBound(object sender, EventArgs e)
    {
        //MeetingsDataSource.DataBind();
        //MeetingsGrid.DataBind();
    }
}