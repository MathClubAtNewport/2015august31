﻿<%@ Page Title="Newport Math Club - KPMT Data" Language="C#" MasterPageFile="~/kpmt/kpmt.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="body" runat="Server">
    <h2>
        <asp:AccessDataSource ID="SchoolsData" runat="server" DataFile="~/App_Data/KPMT.mdb"
            SelectCommand="SELECT [ID], [School Name], [Email] FROM [Schools] ORDER BY [ID]">
        </asp:AccessDataSource>
        <asp:AccessDataSource ID="TeamsData" runat="server" DataFile="~/App_Data/KPMT.mdb"
            SelectCommand="SELECT * FROM [Teams] WHERE [School] = ?">
            <SelectParameters>
                <asp:ControlParameter ControlID="SchoolsGrid" PropertyName="SelectedValue" Type="Int32"
                    Name="School" />
            </SelectParameters>
        </asp:AccessDataSource>
        <asp:AccessDataSource ID="TeamMembersData" runat="server" DataFile="~/App_Data/KPMT.mdb"
            SelectCommand="SELECT [Student Name], [Grade] FROM [Competitors With Teams] WHERE [TeamID] = ?">
            <SelectParameters>
                <asp:ControlParameter ControlID="TeamsGrid" PropertyName="SelectedValue" Type="Int32"
                    Name="TeamID" />
            </SelectParameters>
        </asp:AccessDataSource>
        <asp:AccessDataSource ID="CompetitorsWithoutTeamsData" runat="server" DataFile="~/App_Data/KPMT.mdb"
            SelectCommand="SELECT [Student Name], [Grade] FROM [Competitors Without Teams] WHERE [School ID] = ?">
            <SelectParameters>
                <asp:ControlParameter ControlID="SchoolsGrid" PropertyName="SelectedValue" Type="Int32"
                    Name="School" />
            </SelectParameters>
        </asp:AccessDataSource>
        KPMT Registrations</h2>
    <p>
        This page allows you to view KPMT registrations.</p>
    <table style="width: 100%">
        <tr>
            <td valign="top">
                <h4>
                    Schools<br />
                </h4>
                <asp:GridView ID="SchoolsGrid" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="SchoolsData"
                    ForeColor="#333333" GridLines="None" PageSize="25">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                            SortExpression="ID" Visible="False" />
                        <asp:BoundField DataField="School Name" HeaderText="School" SortExpression="School Name" />
                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                    </Columns>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <br />
                <table style="width: 100%">
                    <tr>
                        <td>
                            <h4>
                                Teams</h4>
                            <asp:GridView ID="TeamsGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                CellPadding="4" DataKeyNames="ID" DataSourceID="TeamsData" ForeColor="#333333"
                                GridLines="None">
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                        SortExpression="ID" Visible="False" />
                                    <asp:BoundField DataField="Team Number" HeaderText="Team Number" SortExpression="Team Number" />
                                    <asp:BoundField DataField="School" HeaderText="School" SortExpression="School" Visible="False" />
                                    <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade" />
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </td>
                        <td>
                            <h4>
                                Team Members</h4>
                            <asp:GridView ID="TeamMembersGrid" runat="server" AutoGenerateColumns="False" DataSourceID="TeamMembersData"
                                AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None">
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="Student Name" HeaderText="Student Name" SortExpression="Student Name">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade"></asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <h4>
                    Competitors Without Teams</h4>
                <br />
                <asp:GridView ID="CompetitorsWithoutTeamsGrid" runat="server" AllowSorting="True"
                    AutoGenerateColumns="False" CellPadding="4" DataSourceID="CompetitorsWithoutTeamsData"
                    ForeColor="#333333" GridLines="None">
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <Columns>
                        <asp:BoundField DataField="Student Name" HeaderText="Student Name" SortExpression="Student Name">
                        </asp:BoundField>
                        <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade"></asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td valign="top">
            </td>
        </tr>
    </table>
</asp:Content>
