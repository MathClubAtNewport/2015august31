﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class officers_competitors : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void AddCompetitorForm_DataBound(object sender, EventArgs e)
    {
        CompetitorsGrid.DataBind();
    }

    protected void ShowHidePanels()
    {
        if (CompetitionsGrid.SelectedIndex < 0 || CompetitionsGrid.Rows.Count <= 0)
            CompetitorsPanel.Visible = false;
        else
            CompetitorsPanel.Visible = true;
    }

    protected void CompetitionsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHidePanels();
    }
}