﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class officers_points : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PointsGrid.DataBind();
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        PointsData.Insert();
        PointsGrid.DataBind();
    }

    protected void PointsData_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {

    }

    private void RefreshButton()
    {
        if (Calendar.SelectedDate == null
                || PointItemsGrid.SelectedIndex < 0 || PointItemsGrid.SelectedIndex >= PointItemsGrid.Rows.Count
                || MembersGrid.SelectedIndex < 0 || MembersGrid.SelectedIndex >= MembersGrid.Rows.Count)
            Button.Enabled = false;
        else
            Button.Enabled = true;
    }

    protected void Calendar_SelectionChanged(object sender, EventArgs e)
    {
        RefreshButton();
    }

    protected void PointItemsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        RefreshButton();
    }

    protected void MembersGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        RefreshButton();
    }
}