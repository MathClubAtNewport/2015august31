﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class officers_tranportation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void TransportationSourcesForm_DataBound(object sender, EventArgs e)
    {
        TransportationSourcesGrid.DataBind();
    }

    protected void CompetitorsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CompetitorsGrid.SelectedIndex < 0)
            return;
        TransportationData.Insert();
        CompetitorsGrid.DataBind();
        TransportationGrid.DataBind();
        CompetitorsGrid.SelectedIndex = -1; // clear selection
    }

    protected void TransportationGrid_DataBound(object sender, EventArgs e)
    {
        CompetitorsGrid.DataBind();
        TransportationSourcesGrid.DataBind();
    }

    protected void CompetitionsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CompetitionsGrid.SelectedIndex < 0 || CompetitionsGrid.SelectedIndex >= CompetitionsGrid.Rows.Count)
            TransportationSourcesForm.Visible = false;
        else
            TransportationSourcesForm.Visible = true;
    }
}