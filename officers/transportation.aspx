﻿<%@ Page Title="Transportation &ndash; Newport Math Club" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="transportation.aspx.cs" Inherits="officers_tranportation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
        .style5
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
<asp:AccessDataSource ID="CompetitionsData" runat="server" 
                DataFile="~/App_Data/MathClub.mdb"
                SelectCommand="SELECT [ID], [Name], [Location], [Date], [Transportation] FROM [Competitions] ORDER BY [Date] DESC">
            </asp:AccessDataSource>
            <asp:AccessDataSource ID="TransportationSourcesData" runat="server" 
                DataFile="~/App_Data/MathClub.mdb" 
                SelectCommand="SELECT [ID], [Provider], [Method], [Capacity], ([Capacity] - (SELECT COUNT(*) FROM [Transportation] WHERE [Source]=[Transportation_Sources].[ID])) as Available
                FROM [Transportation_Sources] WHERE ([Competition] = ?) ORDER BY [Method]"
                UpdateCommand="UPDATE [Transportation_Sources] SET [Provider] = ?, [Method] = ?, [Capacity] = ? WHERE [ID] = ?"
                DeleteCommand="DELETE FROM [Transportation_Sources] WHERE [ID] = ?" >
                <DeleteParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Provider" Type="String" />
                    <asp:Parameter Name="Method" Type="String" />
                    <asp:Parameter Name="Capacity" Type="Int32" />
                    <asp:Parameter Name="ID" Type="Int32" />
                </UpdateParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Name="Competition" Type="Int32" />
                </SelectParameters>
            </asp:AccessDataSource>
            <asp:AccessDataSource ID="TransportationSourcesFormData" runat="server" 
                DataFile="~/App_Data/MathClub.mdb" 
                InsertCommand="INSERT INTO [Transportation_Sources] ([Provider], [Method], [Capacity], [Competition]) VALUES (?, ?, ?, ?)"  
                SelectCommand="SELECT [ID], [Provider], [Method], [Capacity], ([Capacity] - (SELECT COUNT(*) FROM [Transportation] WHERE [Source]=[Transportation_Sources].[ID])) as Available
                FROM [Transportation_Sources] WHERE ([ID] = ?) ORDER BY [Method]" >
                <SelectParameters>
                    <asp:ControlParameter ControlID="TransportationSourcesGrid" PropertyName="SelectedValue" Name="ID" Type="Int32" />
                </SelectParameters>
                <InsertParameters>
                    <asp:Parameter Name="Provider" Type="String" />
                    <asp:Parameter Name="Method" Type="String" />
                    <asp:Parameter Name="Capacity" Type="Int32" />
                    <asp:ControlParameter ControlID="CompetitionsGrid" PropertyName="SelectedValue" Type="Int32" />
                </InsertParameters>
            </asp:AccessDataSource>
            <asp:AccessDataSource ID="TransportationData" runat="server" 
                DataFile="~/App_Data/MathClub.mdb" 
                DeleteCommand="DELETE FROM [Transportation] WHERE [ID] = ?"
                InsertCommand="INSERT INTO [Transportation]([Competitor], [Source]) Values(?, ?)"
                SelectCommand="SELECT T.[ID], C.[Member] AS Competitor FROM [Transportation] T, [Competitors] C
                WHERE (T.[Source] = ? AND C.[ID]=T.[Competitor]) ORDER BY C.[Member]" >
                <SelectParameters>
                    <asp:ControlParameter ControlID="TransportationSourcesGrid" PropertyName="SelectedValue" Name="Source" Type="Int32" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="CompetitorsGrid" PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="TransportationSourcesGrid" PropertyName="SelectedValue" Type="Int32" />
                </InsertParameters>
            </asp:AccessDataSource>
            <asp:AccessDataSource ID="CompetitorsData" runat="server" DataFile="~/App_Data/MathClub.mdb"
                SelectCommand="SELECT P.[ID], P.[Member] FROM [Competitors] P, [Competitions] C, [Transportation_Sources] S
                    WHERE S.[ID]=? AND C.[ID]=S.[Competition] AND P.[Competition]=C.[ID]
                    AND (SELECT COUNT(*) FROM [Transportation] WHERE [Competitor]=P.[ID] AND [Source]=S.[ID]) = 0 ">
                <SelectParameters>
                    <asp:ControlParameter ControlID="TransportationSourcesGrid" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:AccessDataSource>
            <!-- AND S.[ID]=R.[Source] AND  -->
            <h2>Manage Transportation</h2>
			<p>This page allows you to manage sources of transportation.</p>
            <table class="style5">
                <tr>
                    <td valign="top">
            <asp:GridView ID="CompetitionsGrid" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataKeyNames="ID" DataSourceID="CompetitionsData" ForeColor="#333333" 
                GridLines="None" OnSelectedIndexChanged="CompetitionsGrid_SelectedIndexChanged">
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                        ReadOnly="True" SortExpression="ID" Visible="False" />
                    <asp:BoundField DataField="Name" HeaderText="Competition" 
                        SortExpression="Name" />
                    <asp:BoundField DataField="Location" HeaderText="Location" 
                        SortExpression="Location" />
                    <asp:BoundField DataField="Date" DataFormatString="{0:M/d/yy}" 
                        HeaderText="Date" SortExpression="Date" />
                    <asp:BoundField DataField="Transportation" HeaderText="Transportation" 
                        SortExpression="Transportation" />
                </Columns>
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
                    </td>
                </tr>
            </table>
            <br />
            <table class="style5">
                <tr>
                    <td valign="top">
                        <asp:FormView ID="TransportationSourcesForm" runat="server" CellPadding="4" DataKeyNames="ID" 
                            DataSourceID="TransportationSourcesFormData" ForeColor="#333333" 
                            OnDataBound="TransportationSourcesForm_DataBound" DefaultMode="Insert" 
                            Visible="False">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <EditItemTemplate>
                                <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' Visible="false"/>
                                Provider:
                                <asp:TextBox ID="ProviderTextBox" runat="server" 
                                    Text='<%# Bind("Provider") %>' />
                                <br />
                                Method:
                                <asp:TextBox ID="MethodTextBox" runat="server" Text='<%# Bind("Method") %>' />
                                <br />
                                Capacity:
                                <asp:TextBox ID="CapacityTextBox" runat="server" 
                                    Text='<%# Bind("Capacity") %>' />
                                <br />
                                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                                    CommandName="Update" Text="Update" />
                                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                Provider:
                                <asp:TextBox ID="ProviderTextBox0" runat="server" 
                                    Text='<%# Bind("Provider") %>' />
                                <br />
                                Method:
                                <asp:TextBox ID="MethodTextBox0" runat="server" Text='<%# Bind("Method") %>' />
                                <br />
                                Capacity:
                                <asp:TextBox ID="CapacityTextBox0" width="50px" runat="server" 
                                    Text='<%# Bind("Capacity") %>' />
                                <br />
                                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                                    CommandName="Insert" Text="Add Transportation Source" />                                
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' Visible="false"/>
                                Provider:
                                <asp:Label ID="ProviderLabel" runat="server" Text='<%# Bind("Provider") %>' />
                                <br />
                                Method:
                                <asp:Label ID="MethodLabel" runat="server" Text='<%# Bind("Method") %>' />
                                <br />
                                Capacity:
                                <asp:Label ID="CapacityLabel" runat="server" Text='<%# Bind("Capacity") %>' />
                                <br />
                                Available:
                                <asp:Label ID="AvailableLabel" runat="server" Text='<%# Bind("Available") %>' />
                                <br />
                                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                                    CommandName="Edit" Text="Edit" />
                                &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                                    CommandName="Delete" Text="Delete" />
                                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                                    CommandName="New" Text="New" />
                            </ItemTemplate>
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        </asp:FormView>
                    </td>
                </tr>
            </table>
			<br />
            <table class="style5">
                <tr>
                    <td valign="top">
                        <asp:GridView ID="TransportationSourcesGrid" runat="server" AllowPaging="True" 
                            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                            DataKeyNames="ID" DataSourceID="TransportationSourcesData" ForeColor="#333333" 
                            GridLines="None">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" ShowDeleteButton="True" 
                                    ShowEditButton="True" />
                                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                                    ReadOnly="True" SortExpression="ID" Visible="False" />
                                <asp:BoundField DataField="Provider" HeaderText="Transportation Provider" 
                                    SortExpression="Provider" />
                                <asp:BoundField DataField="Method" HeaderText="Method" 
                                    SortExpression="Method" />
                                <asp:BoundField DataField="Capacity" HeaderText="Capacity" 
                                    SortExpression="Capacity" />
                                <asp:BoundField DataField="Available" HeaderText="Available" ReadOnly="True" 
                                    SortExpression="Available" />
                            </Columns>
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
			<br />
			<table class="style5">
                <tr>
                    <td valign="top">
                        <asp:GridView ID="TransportationGrid" runat="server" AutoGenerateColumns="False" 
                            CellPadding="4" DataKeyNames="ID" DataSourceID="TransportationData" 
                            ForeColor="#333333" GridLines="None" AllowPaging="True" 
                            AllowSorting="True" OnDataBound="TransportationGrid_DataBound">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <Columns>
                                <asp:CommandField ShowDeleteButton="True" />
                                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                                    ReadOnly="True" SortExpression="ID" Visible="False" />
                                <asp:BoundField DataField="Competitor" HeaderText="Currently Transporting" 
                                    SortExpression="Competitor" />
                            </Columns>
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
                    <td valign="top">
                        <asp:GridView ID="CompetitorsGrid" runat="server" AllowPaging="True" 
                            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                            DataKeyNames="ID" DataSourceID="CompetitorsData" ForeColor="#333333" 
                            GridLines="None" OnSelectedIndexChanged="CompetitorsGrid_SelectedIndexChanged">
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" SelectText="&laquo;&nbsp;Add to Transportation" />
                                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                                    ReadOnly="True" SortExpression="ID" Visible="False" />
                                <asp:BoundField DataField="Member" HeaderText="Members Attending" 
                                    SortExpression="Member" />
                            </Columns>
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
</asp:Content>

