﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Globalization" %>
<script RunAt="server">
   
    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup

    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_BeginRequest(object sender, EventArgs e)
    {

    }

    void Application_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        HttpException hex = ((HttpException)ex);
        int httpCode = hex.GetHttpCode();
        
        string err = httpCode + " Error in " + Request.Url.ToString() + ". " + ex.Message;
        Application["LastError"] = err;
        WriteError(Request, hex, User.Identity);
        if (Context.IsCustomErrorEnabled)
        {
            Server.ClearError();
            switch (httpCode)
            {
                case 404:
                    Server.Transfer("~/pages/404.aspx");
                    break;
                default:
                    Server.Transfer("~/pages/error.aspx");
                    break;
            }
        }
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

    public static void WriteError(HttpRequest request, HttpException ex, System.Security.Principal.IIdentity userIdentity)
    {
        string timestamp = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        string statusCode = ex.GetHttpCode().ToString();
        string user = userIdentity.Name;
        string userHostAddress = request.UserHostAddress;
        string userHostName = request.UserHostName;
        string url = request.Url.ToString();
        string requestType = request.RequestType;
        string urlReferrer = request.UrlReferrer != null ? request.UrlReferrer.ToString() : String.Empty;
        string errorMessage = ex.Message;

        string dir = "~/error_log/";
        string path = dir + DateTime.Today.ToString("yyyy-MM") + ".csv";
        if (!Directory.Exists(HttpContext.Current.Server.MapPath(dir)))
            Directory.CreateDirectory(HttpContext.Current.Server.MapPath(dir));
        if (!File.Exists(HttpContext.Current.Server.MapPath(path)))
        {
            using (StreamWriter w = File.CreateText(HttpContext.Current.Server.MapPath(path)))
            {
                w.WriteLine("Timestamp,Status Code,URL,Error Message,User,User Host Address,User Host Name,Request Type,URL Referrer");
                w.Flush();
                w.Close();
            }
        }
        using (StreamWriter w = File.AppendText(HttpContext.Current.Server.MapPath(path)))
        {
            w.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8}", timestamp, statusCode, url, errorMessage, user, userHostAddress, userHostName, requestType, urlReferrer);
            w.Flush();
            w.Close();
        }
    }
       
</script>
